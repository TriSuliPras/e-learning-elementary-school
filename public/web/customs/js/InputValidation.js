function InputValidation ( ...input ) {

	var error = {
		prop: 'is-invalid',
		style: "border-color: red;"
	};

	var validateMany = function ( inputs ) {

		for ( var comp in inputs ) {

			var input = $(`#${inputs[comp]}`);

			if ( input.attr('type') === "file" ) {
				
				if ( input.hasClass(error.prop) ) {
					input.change(function() {
				        var filename = $(this).val().split('\\')[2];
				        
				        $(this).removeClass(error.prop);
				        $(this).parent().find('label').text(filename);
				        $(this).parent().find('div').hide();
				    });
				}
				else {
					input.change(function() {
				        var filename = $(this).val().split('\\')[2];
				        $(this).parent().find('label').text(filename);
				    });
				}
			}
			else if ( input.hasClass('datetimepicker-input') ) {

				$('#picker').click(function () {
					
					if ( input.hasClass(error.prop) ) {

						$('#datetime').removeClass(error.prop);
						$('.datetime').hide();
					}
				})
			}
			else {

				if ( input.hasClass(error.prop) ) {

					input.keyup(function () {
						var isInputed = $(this).val().length;

						if (isInputed) {
							$(this).removeClass(error.prop);
							$(this).next().hide();
						}
						else {
							$(this).addClass(error.prop);
							$(this).next().show();
						}
					})
				}
			}
		}
	};

	this.input = input;

	this.validated = function () {

		validateMany(this.input);
	};
}