function clock( comp ) {
    const SUNDAY = 0
          MONDAY = 1,
          TUESDAY = 2,
          WEDNESDAY = 3,
          THURSDAY = 4,
          FRIDAY = 5,
          SATURDAY = 6;
    
    return setInterval(function () {
        var date = new Date();
        var H = date.getHours(),
            M = date.getMinutes(),
            S = date.getSeconds(),
            statusH = H >= 12 ? 'PM' : 'AM',
            seconds = S < 10 ? `0${S}` : S,
            hours = H < 10 ? `0${H}` : H,
            minutes = M < 10 ? `0${M}` : M,
            getDay = function () {
                switch (date.getDay()) {
                    case SUNDAY:
                        return "Ahad";
                    case MONDAY:
                        return "Senin";
                    case TUESDAY:
                        return "Selasa";
                    case WEDNESDAY:
                        return "Rabu";
                    case THURSDAY:
                        return "Kamis";
                    case FRIDAY:
                        return "Jum'at";
                    case SATURDAY:
                        return "Sabtu";
                }
            };

        $(comp).html(`<i class="fa fa-calendar mr-1"></i>${getDay()}, ${hours}:${minutes}:${seconds} ${statusH}`);
    }, 1000);
}