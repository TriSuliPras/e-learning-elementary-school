$(document).ready(function () { 
        
    let error = "is-invalid";

    //handle select option validation
    function handlSelectValidation(dataID) {

        let btnSelect = $(`button[data-id=${dataID}]`);

        if (btnSelect.parent().hasClass(error)) {
            
            btnSelect.css('borderColor', "red");

            btnSelect.parent().find('select').change(function() {
                
                btnSelect.removeAttr('style');
                btnSelect.parent().removeClass(error);
                $(this).parent().next().hide();
            });
        }
    }

    function handleInputTypeText(inputname) {
        
        let input = $(`[name = ${inputname}]`);
        
        if (input.hasClass(error)) {
            
            input.keyup(function() {
                
                let message = $(this).next();
                let letterIsExist = $(this).val().length === 0 ? true : false;
                
                if (letterIsExist) {
                    
                    $(this).addClass(error);
                    message.show();
                }
                else {

                    $(this).removeClass(error);
                    message.hide();
                }
            });
        }
    }
    
    $(function() {

        handlSelectValidation("lesson_id");
        handlSelectValidation("class_room");
        handleInputTypeText('title');
        

        var inputDateTime = $('input[name = datetime]'),
            inputFile = $('input[name = file_path]')
            textArea = $('textarea');

        if (textArea.hasClass(error)) {
            textArea.keyup(function () {
                var letterIsExist = $(this).val().length === 0 ? true : false,
                    message = $(this).next();

                if (letterIsExist) {
                    
                    $(this).addClass(error);
                    message.show();
                }
                else {

                    $(this).removeClass(error);
                    message.hide();
                }
            })
        }

        if ( inputDateTime.hasClass(error) || inputFile.hasClass(error)) {
            
            $("#datetimepicker").on('change.datetimepicker', function ( event ) {
                inputDateTime.removeClass(error);
                $("#datetime").hide();
            })

            inputFile.change(function() {

                $(this).removeClass(error);
                $(this).next().hide();
            });
        }
    });
});