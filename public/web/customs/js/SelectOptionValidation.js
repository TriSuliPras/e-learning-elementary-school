function SelectOptionValidation( ...name ) {
	var error = {
		prop: 'is-invalid',
		style: "border-color: red;"
	};

	var validate = function ( name ) {

		var select = $(`select[name=${name}]`),
			button = $(`button[data-id=${name}]`);

		if ( select.hasClass(error.prop) ) {

			button.attr({style: error.style});
			select.change(function () {
				button.removeAttr('style')
				$(this).parent().next().hide();
			});
		}
	};

	var validateMany = function ( names ) {

		for (var comp in names) {

			var select = $(`select[name=${names[comp]}]`);

			if ( select.hasClass(error.prop) ) {
				$(`button[data-id=${names[comp]}]`).attr({style: error.style});
			}
		}	
	};

	this.name = name;

	this.validated = function () {

		if ( this.name.length > 1 ) {
			validateMany(this.name);
		}
		else {
			validate(this.name)
		}
	};
}