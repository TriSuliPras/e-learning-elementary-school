<?php 

Route::get('/', function () { 
	return view("welcome.index"); 
})->name('guest')->middleware(['guest']);

Route::group(['middleware' => ["web"]], function () {
	
	Route::group(['prefix' => "e-learning"], function () {

		Route::post('login-management', [
			'uses'	=> "Auth\LoginController@authentication",
			'as'	=> "management.auth"
		]);

		Route::group(['middleware' => ["auth:web"]], function () {
			
			Route::group(['prefix' => "management"], function () {
				Route::get('/', [
					'uses'	=> "Dashboard\UserController@home",
					'as'	=> "management.home"
				]);
			});

			Route::get("logout", [
				'uses' => "Auth\LoginController@logout",
				'as'   => "management.logout"
			]);

			/**
			 * Route for controlling Management Users
			 * 
			 */
			Route::group(['prefix' => "management-pegawai"], function() {

				Route::get('profil/{id}', [
					'uses' => "Profile\EmployeeController@show",
					'as'   => "management.employee.show"
				]);

				Route::get('edit-profil/{id}', [
					'uses' => "Profile\EmployeeController@edit",
					'as'   => "management.employee.edit"
				]);

				Route::patch('update-pegawai/{id}', [
					'uses' => "Profile\EmployeeController@update",
					'as'   => "management.employee.update"
				]);
			});

			Route::group(['prefix' => "management-siswa"], function() {

				Route::get('profil-siswa/{id}', [
					'uses' => "Profile\StudentController@show",
					'as'   => "management.student.show"
				]);
			});


			/**
			 * Route for controlling all News
			 * 
			 */
			Route::group(['prefix' => "berita"], function () {
				Route::get('/', [
					'uses' => "Data\NewsController@index",
					'as'   => "news"
				]);

				Route::get('tambah-berita', [
					'uses' => "Data\NewsController@createCategory",
					'as'   => "news.add.category"
				]);

				Route::post('tambah-berita/{title}/kategori/{category}', [
					'uses' => "Data\NewsController@createNews",
					'as'   => "news.add.news"
				]);

				Route::get('detail-berita/{id}', [
					'uses' => "Data\NewsController@detail",
					'as'   => "news.detail"
				]);
			
				Route::post('simpan-berita', [
					'uses' => "Data\NewsController@store",
					'as'   => "news.store"
				]);
				
				Route::get('edit-berita/{id}', [
					'uses' => "Data\NewsController@editCategory",
					'as'   => "news.edit.category"
				]);

				Route::post('edit-berita/{id}/{title}/kategori/{category}', [
					'uses' => "Data\NewsController@editNews",
					'as'   => "news.edit.news"
				]);

				Route::patch("perbarui-berita/{id}", [
					'uses' => "Data\NewsController@update",
					'as'   => "news.update"
				]);

				Route::patch('/', [
					'uses' => "Data\NewsController@updateNewsStatus",
					'as'   => "news.update.status"
				]);

				Route::delete("hapus-berita/{id}", [
					'uses' => "Data\NewsController@delete",
					'as'   => "news.delete"
				]);
			});


			/**
			 * Route for controlling Materials
			 * 
			 */
			Route::group(['prefix' => "materi-pelajaran"], function () {
				Route::match(["get", "delete"],'/', [
					'uses' => "Data\MaterialController@index",
					'as'   => "material"
				]);

				Route::get('tambah-materi', [
					'uses' => "Data\MaterialController@create",
					'as'   => "material.add"
				]);

				Route::post('save-materi', [
					'uses' => "Data\MaterialController@store",
					'as'   => "material.save"
				]);

				Route::get('mode_baca-materi/{id}', [
					'uses' => "Data\MaterialController@show",
					'as'   => "material.show"
				]);

				Route::get('detail-materi/{id}', [
					'uses' => "Data\MaterialController@detail",
					'as'   => "material.detail"
				]);

				Route::get('edit-materi/{id}', [
					'uses' => "Data\MaterialController@edit",
					'as'   => "material.edit"
				]);

				Route::patch('perbarui-materi/{id}', [
					'uses' => "Data\MaterialController@update",
					'as'   => "material.update"
				]);

				Route::delete('hapus-materi/{id}', [
					'uses' => "Data\MaterialController@delete",
					'as'   => "material.delete"
				]);
			});


			//////////////////////////////////////////////
			// Kontroller untuk handel fitur kuisioner  //
			// untuk user guru                          //
			//////////////////////////////////////////////
			Route::group(['namespace' => 'Questionnaire', 'prefix' => "kuis"], function ($route) {
				$route->get('/', 'QuizController@index')->name('quiz');
				$route->get('tambah-kuis', 'QuizController@create')->name('quiz.create');
				$route->post('simpan-kuis', 'QuizController@store')->name('quiz.store');
				$route->get('detail-kuis/{id}', 'QuizController@show')->name('quiz.detail');
				$route->get('edit-kuis/{id}', 'QuizController@edit')->name('quiz.edit');
				$route->patch('perbarui-kuis/{id}', 'QuizController@update')->name('quiz.update');
				$route->delete('hapus-kuis/{id}', 'QuizController@delete')->name("quiz.delete");
				$route->get('soal-kuis/{id}', 'TeacherController@show')->name('quiz.show');
				$route->get('koreksi-kuis/{id}/siswa/{studentId}', 'CorrectionController@show')->name('quiz.essay');
				$route->patch('perbarui-nilai-esai/{id}', 'CorrectionController@update')->name('quiz.essay.update');
				$route->patch('kunci-nilai/{quizId}/{stdId}', 'StatusScoreController@update')->name('quiz.lock');
			});


			Route::group(['prefix' => "tugas"], function () {

				$unique = uniqid() . uniqid();

				Route::match(["get", "delete"], '/', [
					'uses' => "Data\TaskController@index",
					'as'   => "task"
				]);

				Route::get('tambah-tugas', [
					'uses' => "Data\TaskController@create",
					'as'   => "task.add"
				]);

				Route::get('detail-tugas/{id}', [
					'uses' => "Data\TaskController@detail",
					'as'   => "task.detail"
				]);

				Route::post('save-tugas', [
					'uses' => "Data\TaskController@store",
					'as'   => "task.save"
				]);

				Route::get('edit-tugas/{id}', [
					'uses' => "Data\TaskController@edit",
					'as'   => "task.edit"
				]);

				Route::patch('perbarui-tugas/{id}', [
					'uses' => "Data\TaskController@update",
					'as'   => "task.update"
				]);

				Route::patch('simpan/nilai-siswa/{id}', [
					'uses' => "Data\TaskController@updateStudentScore",
					'as'   => "task.update.student.score"
				]);

				Route::patch('kunci/nilai/{taskId}/{stdId}', [
					'uses' => "Data\TaskController@updateStudentScoreStatus",
					'as'   => "task.lock"
				]);

				Route::delete('hapus-tugas/{id}', [
					'uses' => "Data\TaskController@delete",
					'as'   => "task.delete"
				]);
			});

		});

	});

});