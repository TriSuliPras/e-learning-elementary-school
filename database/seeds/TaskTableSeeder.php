<?php

use Carbon\Carbon as Date;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ([
        	[
        		'per_year_id' => 2,
                'semester_id' => 1,
        		'user_id' => 6,
        		'lesson_id' => 1,
        		'class_room' => 3,
        		'pararel' => 'A',
        		'title' => 'Seed Tugas 1',
        		'description' => null,
        		'deadline' => Date::tomorrow('Asia/Jakarta'),
        		'created_at' => Date::now(),
        		'updated_at' => Date::now()
        	],
        	[
        		'per_year_id' => 1,
                'semester_id' => 1,
        		'user_id' => 1,
        		'lesson_id' => 1,
        		'class_room' => 3,
        		'pararel' => 'A',
        		'title' => 'Seed Tugas 2',
        		'description' => null,
        		'deadline' => Date::tomorrow('Asia/Jakarta'),
        		'created_at' => Date::now(),
        		'updated_at' => Date::now()
        	],
        	[
        		'per_year_id' => 1,
                'semester_id' => 1,
        		'user_id' => 7,
        		'lesson_id' => 1,
        		'class_room' => 2,
        		'pararel' => 'A',
        		'title' => 'Seed Tugas 3',
        		'description' => null,
        		'deadline' => Date::tomorrow('Asia/Jakarta'),
        		'created_at' => Date::now(),
        		'updated_at' => Date::now()
        	],
        	[
        		'per_year_id' => 3,
                'semester_id' => 2,
        		'user_id' => 1,
        		'lesson_id' => 1,
        		'class_room' => 3,
        		'pararel' => 'A',
        		'title' => 'Seed Tugas 4',
        		'description' => 'Deskripsi tugas 4',
        		'deadline' => Date::tomorrow('Asia/Jakarta'),
        		'created_at' => Date::now(),
        		'updated_at' => Date::now()
        	],
        	[
        		'per_year_id' => 2,
                'semester_id' => 1,
        		'user_id' => 8,
        		'lesson_id' => 1,
        		'class_room' => 1,
        		'pararel' => 'A',
        		'title' => 'Seed Tugas 5',
        		'description' => null,
        		'deadline' => Date::tomorrow('Asia/Jakarta'),
        		'created_at' => Date::now(),
        		'updated_at' => Date::now()
        	],
        	[
        		'per_year_id' => 3,
                'semester_id' => 2,
        		'user_id' => 1,
        		'lesson_id' => 1,
        		'class_room' => 3,
        		'pararel' => 'A',
        		'title' => 'Seed Tugas 6',
        		'description' => null,
        		'deadline' => Date::tomorrow('Asia/Jakarta'),
        		'created_at' => Date::now(),
        		'updated_at' => Date::now()
        	],
        	[
        		'per_year_id' => 3,
                'semester_id' => 2,
        		'user_id' => 6,
        		'lesson_id' => 1,
        		'class_room' => 3,
        		'pararel' => 'A',
        		'title' => 'Seed Tugas 7',
        		'description' => 'Deskripsi tugas 7',
        		'deadline' => Date::tomorrow('Asia/Jakarta'),
        		'created_at' => Date::now(),
        		'updated_at' => Date::now()
        	],
        	[
        		'per_year_id' => 3,
                'semester_id' => 2,
        		'user_id' => 7,
        		'lesson_id' => 1,
        		'class_room' => 3,
        		'pararel' => 'A',
        		'title' => 'Seed Tugas 8',
        		'description' => 'Deskripsi tugas 8',
        		'deadline' => Date::tomorrow('Asia/Jakarta'),
        		'created_at' => Date::now(),
        		'updated_at' => Date::now()
        	],
        	[
        		'per_year_id' => 3,
                'semester_id' => 2,
        		'user_id' => 1,
        		'lesson_id' => 1,
        		'class_room' => 3,
        		'pararel' => 'A',
        		'title' => 'Seed Tugas 9',
        		'description' => 'Deskripsi tugas 9',
        		'deadline' => Date::tomorrow('Asia/Jakarta'),
        		'created_at' => Date::now(),
        		'updated_at' => Date::now()
        	],
        	[
        		'per_year_id' => 3,
                'semester_id' => 2,
        		'user_id' => 9,
        		'lesson_id' => 1,
        		'class_room' => 3,
        		'pararel' => 'A',
        		'title' => 'Seed Tugas 10',
        		'description' => 'Deskripsi tugas 10',
        		'deadline' => Date::tomorrow('Asia/Jakarta'),
        		'created_at' => Date::now(),
        		'updated_at' => Date::now()
        	],
        ] as $key => $value) {
        	DB::table('el_tasks')->insert($value);
        }
    }
}
