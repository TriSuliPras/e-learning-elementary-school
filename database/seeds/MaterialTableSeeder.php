<?php

use Carbon\Carbon as DateTime;
use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile as File;
use App\Repositories\Eloquent\ORM_MaterialRepository;

class MaterialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $repository = new ORM_MaterialRepository();

        foreach ($this->materials() as $key => $value) {
        	$repository->createWithFile(
        		$value, $this->pathToFile("{$value['class_room']}{$value['pararel']}", $value['lesson_id'])
        	);
        }
    }

    /**
     * Path file
     * 
     * @param  String|Int $classname
     * @param  String|Int $lid      
     * @return String
     */
    public function pathToFile($classname, $lid) : String 
    {
    	return "public/management/material/$classname/lid_$lid";
    }

    public function file(String $path_to_file, $name)
    {
    	return new File(base_path("../../../../$path_to_file"), $name);
    }

    public function materials()
    {
    	return [
    		[
    			'per_year_id'	=> 1,
    			'semester_id'	=> 1,
    			'user_id'		=> 1,
    			'lesson_id'		=> 1,
    			'class_room'	=> 1,
    			'pararel'		=> 'A',
    			'title'			=> 'Tematik Bab 1',
    			'description'	=> 'Teknik Senam Ria Indonesia',
    			'file_path'		=> $this->file('Documents/uas_audit.docx', 'tematik_pjok'),
    		],
    		[
    			'per_year_id'	=> 1,
    			'semester_id'	=> 2,
    			'user_id'		=> 1,
    			'lesson_id'		=> 1,
    			'class_room'	=> 1,
    			'pararel'		=> 'A',
    			'title'			=> 'Tematik Bab 2',
    			'description'	=> 'Teknik Bola Voly',
    			'file_path'		=> $this->file('Documents/uas_audit.docx', 'tematik_pjok'),
    		],
    		[
    			'per_year_id'	=> 2,
    			'semester_id'	=> 1,
    			'user_id'		=> 6,
    			'lesson_id'		=> 33,
    			'class_room'	=> 2,
    			'pararel'		=> 'A',
    			'title'			=> 'Belajar membaca',
    			'description'	=> 'Mempelajari teknik membaca yang baik dan benar',
    			'file_path'		=> $this->file('Downloads/Persyaratan Judisium.pdf', 'Bahasa Indonesia'),
    		],
    		[
    			'per_year_id'	=> 2,
    			'semester_id'	=> 2,
    			'user_id'		=> 6,
    			'lesson_id'		=> 33,
    			'class_room'	=> 2,
    			'pararel'		=> 'A',
    			'title'			=> 'Persamaan Kata',
    			'description'	=> 'Belajara mencari persamaan kata',
    			'file_path'		=> $this->file('Downloads/Persyaratan Judisium.pdf', 'Bahasa Indonesia'),
    		],
    	];
    }
}
