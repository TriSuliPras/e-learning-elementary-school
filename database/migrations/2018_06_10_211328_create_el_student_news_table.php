<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElStudentNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('el_student_news', function (Blueprint $table) {
            $table->integer('news_id')->unsigned();
            $table->integer('student_id')->unsigned();

            $table->foreign("news_id")
                  ->references('id')
                  ->on("el_news")
                  ->onUpdate("NO ACTION")
                  ->onDelete("CASCADE");

            $table->foreign("student_id")
                  ->references('id')
                  ->on("siswas")
                  ->onDelete("NO ACTION");

            $table->primary(["news_id", "student_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('el_student_news');
    }
}
