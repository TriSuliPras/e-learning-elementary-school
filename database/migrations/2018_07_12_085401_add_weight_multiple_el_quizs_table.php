<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWeightMultipleElQuizsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('el_quizs', function (Blueprint $table) {
            $table->char('weight_multiple', 3)->nullable()->after("count_down");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('el_quizs', function (Blueprint $table) {
            $table->dropColumn('weight_multiple');
        });
    }
}
