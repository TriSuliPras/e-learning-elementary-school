<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElStudentQuizsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('el_student_quizs', function (Blueprint $table) {
            $table->integer("quiz_id")->unsigned();
            $table->integer("student_id")->unsigned();
            $table->char("multiple_scores", 4);
            $table->char("essay_scores", 4);
            $table->char("final_scores", 4);
            $table->string("file_path", 100);
            $table->string("status", 10)->default("unlock")->nullable();

            $table->foreign("quiz_id")
                  ->references('id')
                  ->on("el_quizs")
                  ->onUpdate("NO ACTION")
                  ->onDelete("CASCADE");

            $table->foreign("student_id")
                  ->references('id')
                  ->on("siswas")
                  ->onUpdate("NO ACTION")
                  ->onDelete("NO ACTION");

            $table->primary(["quiz_id", "student_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('el_student_quizs');
    }
}
