<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElClassNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('el_class_news', function (Blueprint $table) {
            $table->integer('news_id')->unsigned();
            $table->char('class_room', 2);
            $table->char("pararel", 2);
            
            $table->foreign("news_id")
                  ->references('id')
                  ->on("el_news")
                  ->onUpdate("NO ACTION")
                  ->onDelete("CASCADE");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('el_class_news');
    }
}
