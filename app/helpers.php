<?php declare(strict_types=1);

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

if (! function_exists('extract_class_room')) {
	/**
	 * Memecah grade kelas dengan tipe pararelnya.
	 * 
	 * @param  String $class_room
	 * @return Array       
	 */
	function extract_class_room(String $class_room) : Array {
		$class_room = explode(' ', $class_room);

		return [
			'grade'   => $class_room[0],
			'pararel' => $class_room[1]
		];
	}
}

if (! function_exists('periode')) {
	/**
	 * Extract query url untuk pencarian periode.
	 * 
	 * @param  String $periode
	 * @return Bool|String
	 */
	function periode(String $periode) {
		// hilangkan semua huruf.
		$periode = preg_replace('/[a-z]|[A-Z]/', null, $periode);

		// Hilangkan semua karakter simbol 
        $periode = preg_split('/-|\/|_| /', $periode);

        // Filter data periode yang tidak memiliki nilai.
        $periode = Collection::make($periode)->filter(function ($item) {
            return (int) $item;
        })->values();

        // Jika Hasil dari filter adalah [yyyy, yyyy] 
        if (count($periode) === 2) {
        	return $periode->implode('/');
        }

        // Query string tidak valid.
        return false;
	}
}

if ( ! function_exists('school_year') ) {
	/**
	 * Queri mengambil data tahun ajaran.
	 *  
	 * @param  Int    $start
	 * @param  Int    $end 
	 * @param  String $s
	 * @return \Illuminate\Database\Eloquent\Model|static|mixed
	 */
	function school_year(Int $start = 0, Int $end = 0, String $s = 'aktif') {
		$query = DB::table('tahun_ajaran')->select([
			'id',
			'tahun_ajaran AS year',
			'status'
		]);

		if ($start !== 0 && $end !== 0) {
			return $query->where('tahun_ajaran', "$start/$end")->where('status', $s)->first();
		}

		return $query->where('status', $s)->first();
	}
}

if (! function_exists('semester')) {
	/**
	 * Queri mengambil data semester yang aktif.
	 *  
	 * @param  Int 	$val
	 * @return \Illuminate\Database\Eloquent\Model|static|mixed
	 */
	function semester(Int $val = 0) {
		$query = DB::table('semester')->select([
			'semester', 'id', 'status', 'nama_semester AS name'
		]);

		if ($val === 0) {
			return $query->get();
		}

		return $query->where('semester', $val)->where('status', 'aktif')->first();
	}
}

if (! function_exists("comeback")) {
	
	/**
     * Return redirect back.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
	function comeback() {

		return redirect()->back();
	}
}

if (! function_exists("comeback_with_flag")) {
	
	/**
     * Return redirect back with message.
     * @param String $message
     * @param String $flag
     * @return \Illuminate\Http\RedirectResponse
     */
	function comeback_with_flag(string $flag, string $message) {
		
		return comeback()->with($flag, $message);
	}
}

if (! function_exists("to_route")) {
	
	/**
     * Return redirect route.
     * @param String $name
     * @return \Illuminate\Http\RedirectResponse
     */
	function to_route(string $name) {
		
		return redirect()->route($name);
	}
}

if (! function_exists("to_route_with_param")) {
	
	/**
     * Return redirect route with parameter.
     * @param String $name
     * @param $param
     * @return \Illuminate\Http\RedirectResponse
     */
	function to_route_with_param(string $name, $param) {
		
		return redirect()->route($name, $param);
	}
}

if (! function_exists("to_route_with_params")) {
	
	/**
     * Return redirect route with parameters.
     * @param String $name
     * @param Array $params
     * @return \Illuminate\Http\RedirectResponse
     */
	function to_route_with_params(string $name, array $params) {
		
		return to_route_with_param($name, $params);
	}
}

if (! function_exists("to_route_with_flag")) {
	
	/**
     * Return redirect route with parameter.
     * @param String $name
     * @param String $flag
     * @return \Illuminate\Http\RedirectResponse
     */
	function to_route_with_flag(string $name, string $flag, string $message) {

		return to_route($name)->with($flag, $message);
	}
}

if (! function_exists("to_route_with_param_and_flag")) {
	
	/**
     * Return redirect route with parameter.
     * @param String $name
     * @param $param
     * @param String $message
     * @param String $flag
     * @return \Illuminate\Http\RedirectResponse
     */
	function to_route_with_param_and_flag(
		string $name, 
		$param, 
		string $flag,
		string $message
	) {
		return to_route_with_param($name, $param)->with($flag, $message);
	}
}

if (! function_exists("to_route_with_params_and_flag")) {
	
	/**
     * Return redirect route with parameter.
     * @param String $name
     * @param Array $param
     * @param String $message
     * @param String $flag
     * @return \Illuminate\Http\RedirectResponse
     */
	function to_route_with_params_and_flag(
		string $name, 
		array $param, 
		string $flag, 
		string $message
	) {
		
		return to_route_with_params($name, $params)->with($flag, $message);
	}
}

if (! function_exists("dashboard")) {
	
	/**
     * get the View Path.
     * @param String $name
     * @param String $prefix
     * @return String 
     */
	function dashboard(string $prefix, string $name) {
		
		$isGuardWeb = \Illuminate\Support\Facades\Auth::guard('web')->check();

		if ($isGuardWeb) {

			return "dashboard.management.$prefix.$name";
		}

		return "dashboard.student.$prefix.$name"; 
	}
}

if (! function_exists("not_null")) {

	/**
     * check data isn't null.
     * @param Any $data
     * @return Boolean
     */
	function not_null($data) {

		return !is_null($data);
	}
}

if (! function_exists("null")) {

	/**
     * check data is null.
     * @param Any $data
     * @return Boolean
     */
	function null($data) {

		return !not_null($data);
	}
}

if (! function_exists("is_equal")) {

	/**
     * check data isn't same.
     * @param Any $data1
     * @param Any $data2
     * @return Boolean
     */
	function is_equal($data1, $data2) {

		return $data1 === $data2;
	}
}

if (! function_exists("student")) {
	
	/**
	 * @return \App\Models\Actors\Student
	 */
	function student()
	{
		return new  \App\Repositories\Eloquent\ORM_StudentRepository();
	}
}

if (! function_exists("management")) {
	
	/**
	 * @return \App\Models\Actors\User
	 */
	function management()
	{
		return new  \App\Repositories\Eloquent\ORM_UserRepository();
	}
}

if (! function_exists("datetimepicker")) {
	
	function datetimepicker($date) {

		return \Carbon\Carbon::parse($date)->format('M d, Y H:i A');
	}
}

if (! function_exists("to_json")) {

	function to_json($data) {

		return json_encode($data);
	}
}

if (! function_exists("to_array")) {

	function to_array(string $data) {

		return json_decode($data);
	}
}