<?php declare(strict_types=1);

namespace App\Libraries\AutomatedGrading;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use App\Libraries\Contracts\AutomatedGrading\Corrector\Correction;

abstract class QuestionCorrector implements Correction
{
	/**
	 * Jawaban yang akan dikoreksi.
	 * 
	 * @var array
	 */
	protected $answers;

	/**
	 * Kunci jawaban.
	 * 
	 * @var array
	 */
	protected $answersKey;

	/**
	 * Jawaban benar.
	 * 
	 * @var array
	 */
	protected $correctAnswer;

	/**
	 * Bobot soal.
	 * 
	 * @var integer
	 */
	protected $percentage = 0;

	/**
	 * Soal ujian.
	 * 
	 * @var array
	 */
	protected $questions;

	/**
	 * Nilai akhir.
	 * 
	 * @var float
	 */
	protected $score = 0.0;

	/**
	 * Jawaban salah.
	 * 
	 * @var array
	 */
	protected $wrongAnswer;

	/**
	 * Menyiapkan soal dan kunci jawaban.
	 * 
	 * @param  array $qna [Question And Answers]
	 * @return void
	 */
	public function __construct(array $qna, int $percentage)
	{ 
		// Untuk soal pilihan ganda.
		if (property_exists($this, 'answerChoice')) {
			$this->answerChoice = $this->resolveLesson($qna, 'option');
		}

		$this->percentage = $percentage;
		$this->answersKey = $this->resolveLesson($qna, 'key');
		$this->questions = $this->resolveLesson($qna, 'exercise');
	}

	/**
	 * Prosedur perhitungan nilai.
	 * 
	 * @return float
	 */
	// abstract protected function calculatedResult() : Float;

	/**
	 * Menjalankan prosedur koreksi soal.
	 *
	 * @return void
	 */
	abstract public function correction() : Void;

	/**
	 * Mendapatkan jawaban soal yang dikoreksi.
	 * 
	 * @return array
	 */
	public function getAnswers() : Array
	{
		return $this->answers;
	}

	/**
	 * Mendapatkan jumlah sebuah objek atau properti.
	 * 
	 * @return int
	 */
	public function count() : int
	{
		return count($this->getQuestions());
	}

	/**
	 * Mendapatkan kunci jawaban soal.
	 * 
	 * @return array
	 */
	public function getAnswersKey() : Array
	{
		return $this->answersKey;
	}

	/**
	 * Mendapatkan jawaban soal yang benar.
	 *
	 * @param  boolean $raw
	 * @return int|array
	 */
	public function getCorrectAnswers(bool $raw = false)
	{
		return $raw ? $this->correctAnswer : count($this->correctAnswer);
	}

	/**
	 * Mendapatkan persentase perhitungan nilai.
	 * 
	 * @return int
	 */
	public function getPercentage() : Int
	{
		return $this->percentage;
	}

	/**
	 * Mendapatkan soal ujian.
	 * 
	 * @return array
	 */
	public function getQuestions() : Array
	{
		return $this->questions;
	}

	/**
	 * Mendapatkan nilai akhir dari proses koreksi.
	 * 
	 * @return float
	 */
	public function getScore() : Float
	{
		return $this->score;
	}

	/**
	 * Mendapatkan jawaban soal yang salah.
	 *
	 * @param  boolean $raw
	 * @return int|array
	 */
	public function getWrongAnswers(bool $raw = false)
	{
		return $raw ? $this->wrongAnswer : count($this->wrongAnswer);
	}

	/**
	 * Mengambil beberapa item dari file soal.
	 * 
	 * @param  array  $lessons
	 * @param  string $type   
	 * @return array
	 */
	protected function resolveLesson(array $lessons, string $type) : Array
	{
		return Collection::make($lessons)->map(function ($item) use ($type) {
			return $item[$type];
		})->toArray();
	}

	/**
	 * Menyiapkan jawaban soal.
	 * 
	 * @param  array $values
	 * @return void
	 */
	public function setAnswers(...$values) : Void
	{
		$default = "Tidak diisi";
		$answers = $values[0];
		foreach ($this->getAnswersKey() as $key => $value) {
			$seq = "exam_" . ++$key;
			if (array_key_exists($seq, $answers)) {
				$this->answers[] = $answers[$seq] ?: $default;
			}
			else {
				$this->answers[] = $default;
			}
		}
	}

	/**
	 * Menyiapkan basis persentase nilai.
	 * 
	 * @param  int $value
	 * @return void
	 */
	public function setPercentage(int $value) : Void
	{
		$this->percentage = $value;
	}
}