<?php declare(strict_types=1);

namespace App\Libraries\Contracts\Semantic;

use App\Libraries\Contracts\Semantic\Document;

interface DocumentMatrixable
{
	/**
	 * Mendapatkan hasil inverse document frequency.
	 * 
	 * @return Array
	 */
	public function getIdf() : Array;

	/**
	 * Mendapatkan list term dokumen.
	 * 
	 * @return Array
	 */
	public function getTerms() : Array;

	/**
	 * Mendapatkan hasil dari proses TF-IDF.
	 * 
	 * @return Array
	 */
	public function getTfIdF() : Array;

	/**
	 * Mendapatkan hasil term frequency.
	 * 
	 * @return Array
	 */
	public function getTF() : Array;
}