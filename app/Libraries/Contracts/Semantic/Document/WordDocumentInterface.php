<?php declare(strict_types=1);

namespace App\Libraries\Contracts\Semantic\Document;

interface WordDocumentInterface 
{
	/**
	 * Menambah kata baru pada dokumen.
	 * 
	 * @param  String $word
	 * @return Void
	 */
	public function addWord(String $word) : Void;

	/**
	 * Menambah beberapa kata kedalam dokumen.
	 * 
	 * @param  Array $words
	 * @return Void
	 */
	public function addWords(Array $words) : Void;

	/**
	 * Mencari sebuah yang berdasarkan akhiran huruf
	 * dan mengembalikan nilai huruf tersebut jika
	 * berhasil ditemukan.
	 * 
	 * @param  String $char
	 * @return Array
	 */
	public function findEndWith(String $char) : Array;

	/**
	 * Mencari sebuah yang berdasarkan awalan huruf
	 * dan mengembalikan nilai huruf tersebut jika
	 * berhasil ditemukan.
	 * 
	 * @param  String $char
	 * @return Array
	 */
	public function findStartWith(String $char) : Array;

	/**
	 * Mendapatkan semua kata dokumen dan kata yang baru
	 * ditambahkan kedalam dokumen.
	 * 
	 * @return Array
	 */
	public function getAll() : Array;

	/**
	 * Mendapakan kata dokumen.
	 * 
	 * @return Array
	 */
	public function getAllWord() : Array;

	/**
	 * Mendapatkan semua kata yang baru ditambahkan
	 * kedalam dokumen.
	 * 
	 * @return Array
	 */
	public function getAllNewWord() : Array;
}