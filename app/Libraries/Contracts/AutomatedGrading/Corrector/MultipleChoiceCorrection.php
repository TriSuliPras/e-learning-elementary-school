<?php declare(strict_types=1);

namespace App\Libraries\Contracts\AutomatedGrading\Corrector;

interface MultipleChoiceCorrection
{
	/**
	 * Menyiapkan jawaban pilihan ganda yang akan dikoreksi.
	 * 
	 * @param  array $values
	 * @return void
	 */
	public function setMultipleChoiceAnswers(...$values) : Void;
}