<?php declare(strict_types=1);

namespace App\Libraries\Contracts\AutomatedGrading\Corrector;

use Countable;

interface Correction extends Countable
{
	/**
	 * Menjalankan prosedur koreksi soal.
	 *
	 * @return void
	 */
	public function correction() : Void;

	/**
	 * Mendapatkan jawaban soal yang dikoreksi.
	 * 
	 * @return array
	 */
	public function getAnswers() : Array;

	/**
	 * Mendapatkan kunci jawaban soal.
	 * 
	 * @return array
	 */
	public function getAnswersKey() : Array;

	/**
	 * Mendapatkan jawaban soal yang benar.
	 *
	 * @param  boolean $raw
	 * @return int|array
	 */
	public function getCorrectAnswers(bool $raw = false);

	/**
	 * Mendapatkan persentase perhitungan nilai.
	 * 
	 * @return int
	 */
	public function getPercentage() : Int;

	/**
	 * Mendapatkan soal ujian.
	 * 
	 * @return array
	 */
	public function getQuestions() : Array;

	/**
	 * Mendapatkan nilai akhir dari proses koreksi.
	 * 
	 * @return float
	 */
	public function getScore() : Float;

	/**
	 * Mendapatkan jawaban soal yang salah.
	 *
	 * @param  boolean $raw
	 * @return int|array
	 */
	public function getWrongAnswers(bool $raw = false);

	/**
	 * Menyiapkan jawaban soal.
	 * 
	 * @param  array $values
	 * @return void
	 */
	public function setAnswers(...$values) : Void;

	/**
	 * Menyiapkan basis persentase nilai.
	 * 
	 * @param  int $value
	 * @return void
	 */
	public function setPercentage(int $value) : Void;
}