<?php declare(strict_types=1);

namespace App\Libraries\Semantic;

use Sastrawi\Dictionary\ArrayDictionary;
use Sastrawi\StopWordRemover\StopWordRemover as SastrawiStopword;

class TextPreprocessing
{
	/**
	 * Text yang akan dinormalisasi.
	 * 
	 * @var string
	 */
	public $text;

	/**
	 * Text hasil normalisasi.
	 * 
	 * @var string
	 */
	protected $result;

	/**
	 * Membuat instance dari kelas ini dengan parameter
	 * optional text.
	 * 
	 * @param  string $text
	 * @return void
	 */
	public function __construct(string $text = '')
	{
		$this->text = $this->preprocessing($text);
	}

	/**
	 * Mendapatkan text hasil normalisasi.
	 * 
	 * @return string|null
	 */
	public function getResult()
	{
		return $this->result;
	}

	/**
	 * Mendapatkan kumpulan kata-kata yang tidak memiliki arti.
	 * 
	 * @param  integer $lenght
	 * @return array   
	 */
	public function getStopWords($lenght = 0) : Array
	{
		$stopwords = require base_path('/dictionary/stopwords/indonesian.php');

		if ($lenght > 0 && array_key_exists($lenght, $stopwords)) {
			return $stopwords[$lenght];
		}

		return $stopwords;
	}

	/**
	 * Mendapatkan text.
	 * 
	 * @return string
	 */
	public function getText() : String
	{
		return $this->text;
	}

	/**
	 * Karakter spesial yang akan dihilangkan
	 * dari text.
	 * 
	 * @return array
	 */
	public function charToRemove() : Array
	{
		return [
			'~', '`', '!', '#', '$', '%', '^', '&', '*',
			'(', ')', '[', ']', '{', '}', '/', '|', '\\',
			'-', '+', '=', ',', ':', "'", '"', '_', '.',
			'<', '>',
		];
	}

	/**
	 * Membuat instance dari kelas ini secara statis.
	 * 
	 * @param  string $text
	 * @return static   
	 */
	public static function normalize(string $text)
	{
		return new static($text);
	}	

	/**
	 * Proses normalisasi text.
	 *  
	 * @param  string $text
	 * @return string
	 */
	public function preprocessing(string $text) : String
	{
		$normalizeText = null;
		$text = strtolower($text);

		for ($i = 0; $i < strlen($text); $i++) { 
			if ( ! in_array($text[$i], $this->charToRemove()) ) {
				$normalizeText .= $text[$i];
			}
		}

		$text = array_filter(explode(' ', $normalizeText), function ($item) {
			return $item !== '';
		});

		return implode(' ', $text);
	}

	/**
	 * Menghapus kata-kata dari text yang tidak memiliki arti.
	 * 
	 * @return $this
	 */
	public function removeStopWord()
	{
		$afterStopWord = null;
		$words = $this->getResult() ?? $this->getText();
		$words = explode(' ', $words);

		foreach ($words as $key => $word) {
			if ( ! in_array($word, $this->getStopWords(strlen($word))) ) {
				$afterStopWord .= ($key === (count($words)-1) ) ? $word : str_finish($word, ' ');
			}
		}

		$this->result = $afterStopWord;

		return $this;
	}

	/**
	 * Proses merubah bentuk kata ke kata dasar.
	 * 
	 * @return $this
	 */
	public function stem()
	{
		$words = $this->getResult() ?? $this->getText();

		$stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
		$stemmer = $stemmerFactory->createStemmer();

		$this->result = $stemmer->stem($words);

		return $this;
	}
}