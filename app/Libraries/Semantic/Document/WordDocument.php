<?php declare(strict_types=1);

namespace App\Libraries\Semantic\Document;

use Countable;
use InvalidArgumentException;
use Illuminate\Support\Arr;
use NlpTools\Documents\WordDocument as WordDocumentContext;
use App\Libraries\Contracts\Semantic\Document\WordDocumentInterface;

class WordDocument implements WordDocumentInterface, Countable
{
	/**
	 * Kata baru yang ditambahkan.
	 * 
	 * @var array
	 */
	protected $newWords = [];

	/**
	 * Kumpulan kata.
	 * 
	 * @var array
	 */
	protected $words;

	/**
	 * @var WordDocumentContext
	 */
	private $wordDocumentContext;

	/**
	 * @param  array|string $word
	 * @return void
	 */
	public function __construct(...$word)
	{
		$this->setUpWords($word);
	}

	/**
	 * Menambah kata baru pada dokumen.
	 * 
	 * @param  String $word
	 * @return Void
	 */
	public function addWord(String $word) : Void
	{	
		$this->newWords[] = $word;
	}

	/**
	 * Menambah beberapa kata kedalam dokumen.
	 * 
	 * @param  Array $words
	 * @return Void
	 */
	public function addWords(Array $words) : Void
	{
		foreach ($words as $word) {
			$this->addWord($word);
		}
	}

	/**
	 * Menghitung jumlah kata.
	 * 
	 * @return Int
	 */
	public function count() : Int 
	{
		return count($this->getAll());
	}

	/**
	 * Mencari sebuah yang berdasarkan akhiran huruf
	 * dan mengembalikan nilai huruf tersebut jika
	 * berhasil ditemukan.
	 * 
	 * @param  String $char
	 * @return Array
	 */
	public function findEndWith(String $char) : Array
	{
		$matching = [];
		$words = $this->getAll();

		foreach ($words as $key => $word) {
			if ($word[strlen($word) - 1] === $char) {
				$matching[] = $word;
			}
		}

		return $matching;
	}

	/**
	 * Mencari sebuah yang berdasarkan awalan huruf
	 * dan mengembalikan nilai huruf tersebut jika
	 * berhasil ditemukan.
	 * 
	 * @param  String $char
	 * @return Array
	 */
	public function findStartWith(String $char) : Array
	{
		$matching = [];
		$words = $this->getAll();

		foreach ($words as $key => $word) {
			if ($word[0] === $char) {
				$matching[] = $word;
			}
		}

		return $matching;
	}

	/**
	 * Mendapatkan semua kata dokumen dan kata yang baru
	 * ditambahkan kedalam dokumen.
	 * 
	 * @return Array
	 */
	public function getAll() : Array
	{
		return Arr::collapse([$this->getAllWord(), $this->getAllNewWord()]);
	}

	/**
	 * Mendapakan kata dokumen.
	 * 
	 * @return Array
	 */
	public function getAllWord() : Array
	{
		return $this->words;
	}

	/**
	 * Mendapatkan semua kata yang baru ditambahkan
	 * kedalam dokumen.
	 * 
	 * @return Array
	 */
	public function getAllNewWord() : Array
	{
		return $this->newWords;
	}

	/**
	 * Inisialisasi nilai dari kumpulan kata 
	 * dokumen.
	 * 
	 * @param  array|string $word
	 * @return void
	 * @throws InvalidArgumentException
	 */
	private function setUpWords(...$word) : Void 
	{
		$words = Arr::flatten($word);

		foreach ($words as $word) {
			if (! is_string($word) ) {
				throw new InvalidArgumentException(
					"Word document required a string or array of some text"
				);
			}
		}

		$this->words = $words;
	}
}