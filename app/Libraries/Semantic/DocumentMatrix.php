<?php declare(strict_types=1);

namespace App\Libraries\Semantic;

use App\Libraries\Contracts\Semantic\Document;
use App\Libraries\Contracts\Semantic\DocumentMatrixable;
use App\Libraries\Semantic\Math\TfIdfFeatureFactory;
use Countable;
use Illuminate\Support\Collection;
use NlpTools\Documents\TokensDocument;
use NlpTools\Documents\TrainingSet;
use Phpml\Math\Matrix;

class DocumentMatrix implements Countable, DocumentMatrixable
{
	/**
	 * @var TrainingSet
	 */
	private $documentTrainer;

	/**
	 * Term document - Inverse Document Frequency.
	 * 
	 * @var TfIdfFeatureFactory
	 */
	private $tfIdfFactory;

	public function __construct(Document $keyDoc, Document $valDoc)
	{
		$terms = Collection::make([
			TextPreprocessing::normalize($keyDoc->getBody())->stem()->getResult(),
			TextPreprocessing::normalize($valDoc->getBody())->stem()->getResult(),
		])->map(function ($item) {
			return explode(' ', $item);
		})->collapse()->sort()->unique()->values();

		$this->createDocument([
			'terms' => $terms,
			$keyDoc->getTitle() => $keyDoc, 
			$valDoc->getTitle() => $valDoc
		]);

		$this->tfIdfFactory = new TfIdfFeatureFactory($this);
	}

	private function createDocument(Array $documents)
	{
		$this->documentTrainer = new TrainingSet();

		foreach ($documents as $key => $value) {
			if ($key === 'terms')
				$document = $value->toArray();
			/*else if ($key === 'training')
				$document = explode(' ', 
				   TextPreprocessing::normalize($value->getBody())->stem()->getResult()
				);*/
			else 
				$document = explode(' ', $value->getNormalizeBody());

			$this->documentTrainer->addDocument($key, new TokensDocument($document));
		}
	}

	public function count() : Int 
	{
		return $this->documentTrainer->count();
	}

	public function get() : Array 
	{
		$tfidf = $this->getTfIdF();

		$M = new Matrix(array_values($this->tfIdfFactory->getFeatureArray('', $this->documentTrainer[0])));
		$qN = new Matrix(array_values($tfidf['query']));
		$dN = new Matrix(array_values($tfidf['testing']));

		return ['Q' => $qN, 'D1' => $dN];
	}

	public function getData(String $type)
	{
		switch ($type) {
			case 'training':
				return $this->documentTrainer[1]->getDocumentData();
			case 'testing':
				return $this->documentTrainer[2]->getDocumentData();			
			default:
				dd('Exception');
		}
	}

	public function getDocuments()
	{
		return $this->documentTrainer;
	}

	/**
	 * Mendapatkan hasil inverse document frequency.
	 * 
	 * @return Array
	 */
	public function getIdf() : Array
	{
		$idfTerms = [];

		foreach ($this->getTerms() as $term) {
			$idfTerms[$term] = $this->tfIdfFactory->getInverseDocumentFrequency()[$term];
		}

		return $idfTerms; 
	}

	/**
	 * Mendapatkan list term dokumen.
	 * 
	 * @return Array
	 */
	public function getTerms() : Array
	{
		return $this->documentTrainer[0]->getDocumentData();
	}

	/**
	 * Mendapatkan hasil dari proses TF-IDF.
	 * 
	 * @return Array
	 */
	public function getTfIdF() : Array
	{
		return [
			'query' => $this->tfIdfFactory->getFeatureArray('', $this->documentTrainer[1]),
			'testing' => $this->tfIdfFactory->getFeatureArray('', $this->documentTrainer[2]),
		];
	}

	/**
	 * Mendapatkan hasil term frequency.
	 * 
	 * @return Array
	 */
	public function getTF() : Array
	{
		return $this->tfIdfFactory->getTermFrequency();
	}
}