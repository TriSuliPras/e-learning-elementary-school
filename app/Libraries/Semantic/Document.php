<?php declare(strict_types=1);

namespace App\Libraries\Semantic;

use App\Libraries\Contracts\Semantic\Document as DocumentContract;

class Document implements DocumentContract
{
	/**
	 * Default judul dokumen.
	 */
	private const DEFAULT_TITLE = 'Untitled Document';

	/**
	 * Isi dokumen.
	 * 
	 * @var string
	 */
	public $body;

	/**
	 * Judul dokumen.
	 * 
	 * @var string
	 */
	public $title;

	/**
	 * Membuat dokumen baru dengan judul dan isi dari
	 * dokumen.
	 * 
	 * @param  string|null $title
	 * @param  string|null $body 
	 * @return void
	 */
	public function __construct(string $title = null, string $body = null)
	{
		$this->title = $title;
		$this->body = $body;
	}

	/**
	 * Menyiapkan isi dokumen secara dinamis.
	 * 
	 * @param string $key  
	 * @param string $value
	 * @return void 
	 */
	public function __set($key, $value) : Void 
	{
		$this->{$key} = $value;
	}

	/**
	 * Mendapatkan isi dari document.
	 * 
	 * @return string|null
	 */
	public function getBody()
	{
		return $this->body;
	}

	/**
	 * Mendapatkan judul dokumen.
	 * 
	 * @return string
	 */
	public function getTitle() : String
	{
		if (is_null($this->title)) {
			return self::DEFAULT_TITLE;
		}

		return $this->title;
	}

	public function getNormalizeBody()
	{
		return TextPreprocessing::normalize(
			$this->getBody()
		)->removeStopWord()->stem()->getResult();
	}

	/**
	 * Menyiapkan isi dokumen.
	 * 
	 * @param  string $text
	 * @return void
	 */
	public function setBody(string $text) : Void
	{
		$this->__set('body', $text);
	}

	/**
	 * Menyiapkan judul dokumen.
	 * 
	 * @param  string $text
	 * @return void
	 */
	public function setTitle(string $text) : Void
	{
		$this->__set('title', title_case($text));
	}

	/**
     * Mendapatkan isi dokumen per kata.
     *
     * @return array
     */
    public function toArray()
    {
    	return explode(' ', $this->getBody());
    }
}