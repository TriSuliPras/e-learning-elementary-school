<?php declare(strict_types=1);

namespace App\Libraries\Semantic\Math;

use NlpTools\Analysis\Idf;
use Illuminate\Support\Collection;
use NlpTools\Documents\DocumentInterface;
use NlpTools\FeatureFactories\FunctionFeatures;
use App\Libraries\Contracts\Semantic\DocumentMatrixable;
 
class TfIdfFeatureFactory extends FunctionFeatures
{
	protected $document;

	private $tf;

    private $idf;
 
    public function __construct(DocumentMatrixable $document)
    {
        parent::__construct(array(
	        function ($cls, $doc) {
	            return $doc->getDocumentData();
	        }
	    ));
        
        $this->modelFrequency();

    	$this->tf = $this->createTermFrequency($document);
        $this->idf = new Idf($document->getDocuments());
    }

    protected function createTermFrequency(DocumentMatrixable $document)
    {
    	$termFrequency = [];

    	$termsProduct = Collection::make($document->getTerms())->mapToGroups(function ($item, $k) {
    		return [$item => ['q' => 0, 'd' => 0, 'tf' => 0]];
    	});

    	foreach ($document->getTerms() as $word) {
    		$termFrequency[$word] = [
    			'q' => $termsProduct[$word][0]['q'],
    			'd' => $termsProduct[$word][0]['d'],
    		];
    		foreach ($document->getData('training') as $try) {
    			if ($word === $try) {
    				$termFrequency[$word]['q'] += 1;
    			}
    		}

    		foreach ($document->getData('testing') as $try) {
    			if ($word === $try) {
    				$termFrequency[$word]['d'] += 1;
    			}
    		}

    		$termFrequency[$word]['tf'] = $termFrequency[$word]['q'] + $termFrequency[$word]['d'];
    	}

    	return $termFrequency;
    }
 
    public function getFeatureArray($class, DocumentInterface $doc)
    {
        $frequencies = parent::getFeatureArray($class, $doc);
        foreach ($frequencies as $term=>&$value) {
            $value = $value*$this->idf[$term];
        }
        return $frequencies;
    }

    public function getInverseDocumentFrequency()
    {
    	return $this->idf;
    }

    public function getTermFrequency() : Array 
    {
    	return $this->tf;
    }
}
 
/*$idf = new Idf($tset);
$ff = new TfIdfFeatureFactory(
    $idf,
    
);
 
print_r($ff->getFeatureArray("", $tset[0]));*/