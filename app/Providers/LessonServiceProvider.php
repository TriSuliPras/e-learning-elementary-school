<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class LessonServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // lesson share variable
        View::composer(
            [
                "dashboard.management.task.storeORupdate",
                "dashboard.management.quiz.storeORupdate",
                "dashboard.management.material.storeORupdate",
            ], 
            'App\Http\ViewComposers\LessonViewComposer'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
