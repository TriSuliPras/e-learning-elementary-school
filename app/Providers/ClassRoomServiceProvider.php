<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ClassRoomServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // classroom share variable
        View::composer(
            [
                "dashboard.management.quiz.storeORupdate",
                "dashboard.management.task.storeORupdate",
                "dashboard.management.material.storeORupdate",
            ], 
            'App\Http\ViewComposers\ClassRoomViewComposer'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
