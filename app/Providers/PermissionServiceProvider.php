<?php

namespace App\Providers;

use Gate;
use App\Models\Permissions\Permission;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class PermissionServiceProvider extends ServiceProvider
{

	public function boot()
	{
		// Permission::get()->map(function ($permission){
		// 	Gate::define($permission->name, function ($user) use ($permission){
		// 		return $user->hasPermissionTo($permission);
		// 	});
		// });
		
		Blade::directive('role', function ($role) {
			return $this->roleHelper($role)['start'];
		});

		Blade::directive('endrole', function ($role) {
			return $this->roleHelper()['end'];
		});
	}

	public function register() 
	{

	}

	public function roleHelper($role = "")
	{
		$START = "if (auth()->check() && auth()->user()->hasRole({$role})):";

		$END = "endif;";

		return [
			'start' 	=> '<?php '. $START .' ?'.'>',
			'end'		=> '<?php '. $END   .' ?'.'>'
		];
	}
}