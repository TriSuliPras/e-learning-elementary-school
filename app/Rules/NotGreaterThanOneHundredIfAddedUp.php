<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NotGreaterThanOneHundredIfAddedUp implements Rule
{
    public $otherValue;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($otherValue)
    {
        $this->otherValue = $otherValue;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $combineValue = (int) $value + (int) $this->otherValue;

        return  $combineValue == 100;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.greater_than_one_hundred');;
    }
}
