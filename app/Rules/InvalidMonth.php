<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class InvalidMonth implements Rule
{
    protected $maxMonth;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->maxMonth = 12;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        return (int) $value > 0 && $value <= $this->maxMonth;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.invalid_date', ['what' => "Bulan"]);
    }
}
