<?php 

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\Eloquent\ORM_QuizRepository;

final class UpdateQuizStatus
{
	/**
	 * 
	 * @property [#] $quiz <Quiz Repository Model>
	 */
	protected $quiz;

	/**
	 * @param ORM_QuizRepository $quiz <Type Hinting Quiz Repository model>
	 * @return void 
	 */
	public function __construct(ORM_QuizRepository $quiz)
	{
		// Dependencies automatically resolved by service container.
		$this->quiz = $quiz;
	}
	
	/**
	 * @param View $view
	 * @return void 
	 */
	public function compose(View $view)
	{
		$view->with('quizStatus', $this->quiz->updateQuizStatus("true"));
	}
}