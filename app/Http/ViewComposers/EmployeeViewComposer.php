<?php 

namespace App\Http\ViewComposers;

use Illuminate\View\View;	
use App\Repositories\Eloquent\ORM_EmployeeRepository;

final class EmployeeViewComposer
{
	/**
	 * 
	 * @property [#] $employee <ORM_EmployeeRepository
	 */
	protected $employee;

	/**
	 * @param ORM_EmployeeRepository $employee <ORM_EmployeeRepository>
	 * @return void 
	 */
	public function __construct(ORM_EmployeeRepository $employee)
	{
		// Dependencies automatically resolved by service container.
		$this->employee = $employee;
	}
	
	/**
	 * @param View $view
	 * @return void 
	 */
	public function compose(View $view)
	{
		$view->with('employees', $this->employee->selectNameAndId()->get());
	}
}