<?php 

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\Eloquent\ORM_LessonRepository;

final class LessonViewComposer
{
	/**
	 * 
	 * @property [#] $lesson <ORM_LessonRepository
	 */
	protected $lesson;

	/**
	 * @param ORM_LessonRepository $lesson <ORM_LessonRepository>
	 * @return void 
	 */
	public function __construct(ORM_LessonRepository $lesson)
	{
		// Dependencies automatically resolved by service container.
		$this->lesson = $lesson;
	}
	
	/**
	 * @param View $view
	 * @return void 
	 */
	public function compose(View $view)
	{
		// dd($this->lesson->getWithCurriculum()->toArray());

		$view->with('lessons', $this->lesson->getWithCurriculums());
	}
}