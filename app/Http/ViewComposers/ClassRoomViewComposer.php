<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\Eloquent\ORM_ClassRoomRepository;

final class ClassRoomViewComposer
{
	/**
	 * 
	 * @property [#] $classroom <ORM_ClassRoomRepository
	 */
	protected $classroom;

	/**
	 * @param ORM_ClassRoomRepository $classroom <ORM_ClassRoomRepository>
	 * @return void 
	 */
	public function __construct(ORM_ClassRoomRepository $classroom)
	{
		// Dependencies automatically resolved by service container.
		$this->classroom = $classroom;
	}
	
	/**
	 * @param View $view
	 * @return void 
	 */
	public function compose(View $view)
	{
		$view->with('classes', $this->classroom->all());
	}
}