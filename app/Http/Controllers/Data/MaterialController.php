<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\ORM_MaterialRepository;
use App\Http\Controllers\Data\Traits\ClassroomPararel;
use App\Http\Controllers\Data\Contracts\GetProperties;
use App\Repositories\Eloquent\Criteria\{EagerLoad, Teacher, Select, Join};

class MaterialController extends Controller implements GetProperties
{
    use ClassroomPararel;
    
    /**
     * @property [#] $material <Material Model>
     */
    protected $material;
    
    /**
     * 
     * @param \App\Repositories\Eloquent\ORM_MaterialRepository $material
     * @return void 
     */
    public function __construct(ORM_MaterialRepository $material)
    {
        $this->material = $material;
    }

    /**
     * Get request values
     * 
     * @param \Illuminate\Http\Request $request
     * @return Array 
     */
    public function getProperties(Request $request) : Array
    {
        return [
            'per_year_id'     => school_year()->id,
            'semester_id'     => \DB::table('semester')->where('status', 'aktif')->first()->id,
            'user_id'         => $request->user_id,
            'lesson_id'       => $request->lesson_id,
            'class_room'      => $this->getClassRoom($request->class_room),
            'pararel'         => $this->getPararel($request->class_room),
            'title'           => ucfirst($request->title),
            'description'     => $request->description,
            'file_path'       => $request->file_path
        ];
    }
    
    /**
     * Display list data materi berdasarkan tahun ajaran dan semester.
     *
     * @param  Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Request query string url.
        $p = $request->periode; $s = $request->semester;

        $materials = $this->material->withCriteria([
            new Teacher,
            new Join('tahun_ajaran.id', 'per_year_id'),
            new Join('semester.id', 'semester_id'),
            new EagerLoad([
                'lessons' => function ($l) {
                    $l->select(['id', 'nama_matapelajaran']);
                }
            ]),
            new Select([
                'el_materials.id', 'user_id', 'per_year_id', 'file_path',
                'lesson_id', 'title', 'tahun_ajaran as year', 
                'pararel', 'semester_id', 'tahun_ajaran.status as periode_status', 
                'class_room',
            ]),
        ]);

        if (auth()->guard('web')->check()) {
            $materials = $materials->wheres(['user_id = ' . management()->getId()]);
        }
        else {
            $materials = $materials->wheres([
                'class_room = ' . student()->getClass(),
                'pararel = ' . student()->getPararel()
            ]);
        }

        // Jika filter periode tahun dan semester
        if ($p && $s) {
            $materials = $materials->wheres([
                "semester_id = $s", 
                "tahun_ajaran.tahun_ajaran = " . periode($p),
            ]);
        }
        else if ($p || $s) {
            if ($p) // Jika filter hanya periode tahun 
                $materials = $materials->wheres(['tahun_ajaran.tahun_ajaran = ' . periode($p)]);

            if ($s) // Jika filter hanya periode tahun 
                $materials = $materials->wheres(["semester_id = $s", 'tahun_ajaran.status = aktif']);
        }
        else {
            $materials = $materials->wheres(['tahun_ajaran.status = aktif', 'semester.status = aktif']);
        }
        // dd($materials->all()->toArray());
        return view(dashboard("material", "index"), [
            'datas' => $materials->all()
        ]);
    }
    
    /**
     * Material create page
     * 
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view(dashboard("material", "storeORupdate"));
    }
    
    /**
     * Store Material
     *
     * @param \Illuminate\Http\Request $request 
     * @return \Illuminate\Http\RedirectResponse 
     */
    public function store(Request $request)
    {   
        $this->validate($request, $this->material->getRules());

        $properties = $this->getProperties($request);

        $class = $properties['class_room'];
        $pararel = $properties['pararel'];
        $lid = $properties['lesson_id'];

        $pathToFile = "public/management/material/{$class}{$pararel}/lid_$lid";

        $this->material->createWithFile($properties, $pathToFile);

        return to_route_with_flag(
            "material", 
            "store", 
            trans('confirmation.store', ['data' => "Materi"])
        );
    }
    
    /**
     * Material detail
     * 
     * @param \Illuminate\Http\Request $request
     * @param Int $id <Material id>
     * @return \Illuminate\View\View     
     */
    public function detail(Request $request, int $id)
    {
        $material = $this->material->find($id);

        return view(dashboard("material", "detail"), compact('material'));

    }
    
    /**
     * Material read mode page
     * 
     * @param  Int $id <Material id>
     * @return \Illuminate\Http\RedirectResponse 
     */
    public function show($id)
    {
		$material = $this->material->find($id);

        return response()->file(public_path($material->file_path));
    }
    
    /**
     * Material Edit Page
     *
     * @param Int $id <Material id>
     * @return \Illuminate\View\View
     */
    public function edit(int $id)
    {
		$material = $this->material->find($id);
		
		return view(dashboard("material", "storeORupdate"), compact('material'));
    }
    
    /**
     * Update Material
     *
     * @param \Illuminate\Http\Request $request 
     * @param Int $id <Material id>
     * @return \Illuminate\Http\RedirectResponse 
     */
    public function update(Request $request, int $id)
    {		   
        // Get rules of material model
        $rules = $this->material->getRules();

        // extract request
        $properties = $this->getProperties($request);

        // find material by id must to update
        $material = $this->material->find($id);

        $oldpath = $material->file_path;
        $classroom = $properties['class_room'];
        $cls = $classroom.$properties['pararel'];
        $lesson = $properties['lesson_id'];

        // make new path for incoming file
        $newpath = "public/management/material/$cls/lid_$lesson/";
        
        if ($request->hasFile('file_path')) {
            
            $this->validate($request, $rules);

            $this->material->updateWithCreateNewFile($id, $properties, $oldpath, $newpath);
        }
        else {
                
            $this->validate($request, array_except($rules, ['file_path']));
         
            $filename = explode('/', $oldpath)[5];
            $newpath = str_replace("public", "storage", $newpath) . "$filename";
            
            $this->material->updateWithMoveFile($id, $properties, $oldpath, $newpath);
        }

        return to_route_with_param_and_flag(
            "material.detail", 
            $id, 
            "update", 
            trans('confirmation.update', ['data' => "Materi"])
        );
    }
    
    /**
     * Delete Material
     * 
     * @param Int $id <Material id>
     * @return \Illuminate\Http\RedirectResponse 
     */
    public function delete($id)
    {
    	$this->material->deleteWithFile($id);

    	return to_route_with_flag(
            "material", 
            "delete", 
            trans('confirmation.delete', ['data' => "Materi"]));
    }
}
