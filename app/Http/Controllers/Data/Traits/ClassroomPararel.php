<?php 

namespace App\Http\Controllers\Data\Traits;

trait ClassroomPararel
{
	/**
     * Explode class_room request 
     * and get the index 0 value 
     * 
     * @param String $classroom <input class_room>
     * @return String
     */
    public function getClassRoom(string $classroom) : String
    {
        $class = explode(' ', $classroom);

        return $class[0];
    }
    
    /**
     * Explode class_room request 
     * and get the index 1 value 
     * 
     * @param String $pararel <input class_room>
     * @return String
     */
    public function getPararel(string $pararel) : String
    {
        $pararel = explode(' ', $pararel);

        return $pararel[1];
    }
}