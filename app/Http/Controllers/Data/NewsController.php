<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\ORM_NewsRepository;
use App\Repositories\Eloquent\ORM_StudentRepository;
use App\Repositories\Eloquent\Criteria\EagerLoad;
use App\Http\Controllers\Data\Contracts\GetProperties;

class NewsController extends Controller implements GetProperties
{
    /**
     * @property [#] $news <ORM_NewsRepository>
     */
	protected $news;
    
    /**
     * Instanciate News model Repository
     * 
     * @param \App\Repositories\Eloquent\ORM_NewsRepository
     * @return void 
     */
	public function __construct(ORM_NewsRepository $news)
	{
        $this->news = $news;
	}
    
    /**
     * Extract request data
     * 
     * @param \Illuminate\Http\Request $request 
     * @return Array 
     */
    public function getProperties(Request $request) : Array
    {
        return [
            'title'       => $request->title,
            'receiver'    => $this->news->setCategory($request->receiver),
            'student_id'  => $request->student_id,
            'class_room'  => $request->class_room,
            'description' => $request->description
        ];
    }
    
    /**
     * Index Page
     * 
     * @param \Illuminate\Http\Request $request 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $datas = $this->news->getAll();

        return view(dashboard("news", "index"), compact("datas"));
    }
    
    /**
     * Create News Category
     * 
     * @param \App\Repositories\Eloquent\ORM_StudentRepository $request 
     * @return \Illuminate\Http\Response
     */
    public function createCategory(ORM_StudentRepository $students)
    {
        $students = $students->select(['id', 'nama_siswa as name', 'nisn'])->all();
        
    	return view(dashboard("news.storeORupdate", "create_category"), compact('students'));
    }
    
    /**
     * Create News body
     * 
     * @param \Illuminate\Http\Request $request
     * @param $title 
     * @param $category 
     * @return \Illuminate\Http\Response
     */
    public function createNews(Request $request, $title, $category)
    {
        $rules = $this->news->getRules();
        $this->validate($request, array_except($rules, ['description']));
        
        $students = $request->student_id;
        $classes = $request->class_room;
        $request->session()->put('title', $title);
        $request->session()->put('receiver', $category);

        return view(dashboard("news.storeORupdate", "create_news"), [
            'datas' => [
                'title'    => ucfirst($title),
                'receiver' => $this->news->setCategory($category),
                'students' => $students ? $students : null,
                'classes'  => $classes ? $classes : null
            ]
        ]);
    }
    
    /**
     * Store News
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Redirect
     */
    public function store(Request $request)
    {
        $rules = $this->news->getRules();

        $this->validate($request, array_only($rules, ['description']));
        
        $news = $request->except('_token');
        $news['student_id'] = to_array($news['student_id']);
        $news['class_room'] = to_array($news['class_room'][0]);

        switch ($request->receiver) {
            case "Class":
                $news = array_except($news, ['student_id']);
                $this->news->storeClassCategory($news);
                break;
            
            case "Student":
                $news = array_except($news, ['class_room']);
                $this->news->storeStudentCategory($news);
                break;

            default:
                $news = array_except($news, ['class_room', 'student_id']);
                $this->news->store($news);
                break;
        }

        session()->forget('title');
        session()->forget('receiver');

    	return to_route_with_flag(
            "news", 
            "store", 
            trans('confirmation.store', ['data' => "Berita"])
        );
    }
    
    /**
     * Detail News
     * 
     * @param $id <News ID>
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $news = $this->news->find($id);

        return view(dashboard("news", "detail"), compact('news'));
    }
    
    /**
     * Edit news category
     * 
     * @param $id <News ID>
     * @return \Illuminate\Http\Response
     */
    public function editCategory(ORM_StudentRepository $students, $id)
    {   
        $news = $this->news->withCriteria([
            new EagerLoad(['students', 'classnews'])
        ])->find($id);
        
        $students = $students->select(['id', 'nama_siswa as name', 'nisn'])->all();
        // dd($news->toArray(), $students->toArray());
		return view(dashboard("news.storeORupdate", "create_category"), compact('students', 'news'));
    }

    /**
     * Edit News body
     * 
     * @param \Illuminate\Http\Request $request
     * @param $title 
     * @param $category 
     * @return \Illuminate\Http\Response
     */
    public function editNews(Request $request, $id, $title, $category)
    {
        $rules = $this->news->getRules();
        $this->validate($request, array_except($rules, ['description']));
        
        $news = $this->news->find($id);
        $students = $request->student_id;
        $classes = $request->class_room;
        $request->session()->put('title', $title);
        $request->session()->put('receiver', $category);
        
        return view(dashboard("news.storeORupdate", "create_news"), [
            'datas' => [
                'title'    => ucfirst($title),
                'receiver' => $this->news->setCategory($category),
                'students' => $students ? $students : null,
                'classes'  => $classes ? $classes : null,
            ],
            'news' => $news
        ]);
    }
    
    /**
     * Update News
     * 
     * @param \Illuminate\Http\Request $request
     * @param $id <News ID>
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news = $this->getProperties($request);
        $news['student_id'] = to_array($news['student_id']);
        $news['class_room'] = to_array($news['class_room'][0]);

        switch ($request->receiver) {
            case 'Class':
                $news = array_except($news, ['student_id']);
                $this->news->updateNewsCategoryClass($id, $news);
                break;
            case 'Student':
                $news = array_except($news, ['class_room']);
                $this->news->updateNewsCategoryStudent($id, $news);
                break;

            default:
                $news = array_except($news, ['student_id', 'class_room']);
                $this->news->updated($id, $news);
                break;
        }
        
        session()->forget('title');
        session()->forget('receiver');

        return to_route_with_param_and_flag(
            "news.detail", 
            $id,
            "update", 
            trans('confirmation.update', ['data' => "Berita"])    
        );
    }
    
    /**
     * Update News Status
     * 
     * @param \Illuminate\Http\Request $request 
     * @return \Illuminate\Http\Response 
     */
    public function updateNewsStatus(Request $request)
    {
        $done = null;
        $id = $request->by_id;
        $title = $request->by_title;
        $status = $request->status;
        $msg = "Berita $title Berhasil Dinonaktifkan";

        if ($status == "Aktif") {
            
            $done = $this->news->update($id, ["status" => "active"]);
            $msg = "Berita $title Berhasil Diaktifkan";
        }
        else 
            $done = $this->news->update($id, ["status" => "deactive"]);

        return $done ? 
            response()->json(['msg' => $msg, 'aHref' => "{$title} id_{$id}", 'status' => $status], 200) : 
            response()->json(['msg' => "Terjadi Kesalahan!!!, Mohon Coba Lagi"], 500);
    }
    
    /**
     * Delete News
     * 
     * @param int $id <News ID>
     * @return \Illuminate\Http\RedirectResponse 
     */
    public function delete($id)
    {
		$this->news->destroy($id);

        return comeback_with_flag("delete", "Berita Berhasil Dihapus");
    }
}
