<?php

namespace App\Http\Controllers\Questionnaire;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\ORM_QuizRepository as Quiz;

class StatusScoreController extends Controller
{
	/**
	 * Repositori model.
	 * 
	 * @var Quiz
	 */
	protected $quiz;

	/**
	 * Membuat instance baru.
	 * 
	 * @param  Quiz $quiz
	 * @return Void 
	 */
	public function __construct(Quiz $quiz)
	{
		$this->quiz = $quiz;
	}

	/**
	 * Perbarui status nilai kuisioner siswa
	 * tabel el_student_quizs.
	 * 
	 * @param  Int    $quiz_id
	 * @param  Int    $std_id 
	 * @return \Illuminate\Http\JsonResponse       
	 */
    public function update(Int $quiz_id, Int $std_id)
    {
    	$isAccepted = $this->quiz->updateStudentQuizStatus($quiz_id, $std_id);
        
        return $isAccepted ? 
                response()->json(trans('confirmation.update_lock', ['data' => "Kuis"]), 200) :
                response()->json("Oops!, Terjadi Kesalahan");
    }
}
