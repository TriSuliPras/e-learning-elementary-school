<?php

namespace App\Http\Controllers\Questionnaire;

use Carbon\Carbon as Date;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Services\Quiz\QuestionsControl;
use App\Http\Requests\StoreTeacherQuiz;
use App\Http\Requests\UpdateTeacherQuiz;
use App\Services\OfficeDocuments\OfficeExcel;
use App\Repositories\Eloquent\ORM_QuizRepository as Quiz;
use App\Repositories\Eloquent\Criteria\{EagerLoad, Teacher, Select, Join};

class QuizController extends Controller
{
	/**
	 * Repositori model.
	 * 
	 * @var Quiz
	 */
	protected $quiz;

	/**
	 * Membuat instance baru.
	 * 
	 * @param  Quiz $quiz
	 * @return Void 
	 */
	public function __construct(Quiz $quiz)
	{
		$this->quiz = $quiz;
	}

	/**
	 * Display Form kuisioner baru.
	 * 
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return view(dashboard("quiz", "storeORupdate"));
	}

	/**
     * Display list data quiz berdasarkan tahun ajaran dan semester.
     * 
     * @param  Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
    	// Request query string url.
        $p = $request->periode; $s = $request->semester;

        $quizs = $this->quiz->withCriteria([
            new Teacher,
            new Join('tahun_ajaran.id', 'per_year_id'),
            new Join('semester.id', 'semester_id'),
            new EagerLoad([
                'lessons' => function ($l) {
                    $l->select(['id', 'nama_matapelajaran']);
                }
            ]),
            new Select([
                'el_quizs.id', 'user_id', 'per_year_id', 'el_quizs.status',
                'count_down', 'lesson_id', 'title', 'tahun_ajaran as year', 
                'starting_time', 'pararel', 'semester_id', 'tahun_ajaran.status as periode_status', 
                'class_room',
            ]),
        ]);

        if (auth()->guard('web')->check()) {
            $quizs = $quizs->wheres(['user_id = ' . management()->getId()]);
        }
        else {
            $quizs = $quizs->wheres([
                'class_room = ' . student()->getClass(),
                'pararel = ' . student()->getPararel()
            ]);
        }

        // Jika filter periode tahun dan semester
        if ($p && $s) {
            $quizs = $quizs->wheres([
                "semester_id = $s", 
                "tahun_ajaran.tahun_ajaran = " . periode($p),
            ]);
        }
        else if ($p || $s) {
            if ($p) // Jika filter hanya periode tahun 
                $quizs = $quizs->wheres(['tahun_ajaran.tahun_ajaran = ' . periode($p)]);

            if ($s) // Jika filter hanya periode tahun 
                $quizs = $quizs->wheres(["semester_id = $s", 'tahun_ajaran.status = aktif']);
        }
        else {
            $quizs = $quizs->wheres(['tahun_ajaran.status = aktif', 'semester.status = aktif']);
        }
        // dd($quizs->all()->toArray());
        return view(dashboard("quiz", "index"), [
            'datas' => $quizs->all()
        ]);
    }

    /**
     * Display form edit kuis.
     * 
     * @param  Int    $id
     * @return \Illuminate\View\View
     */
    public function edit(Int $id)
    {
    	$quiz = $this->quiz->find($id);

        $time = explode(' ', $quiz->starting_time)[1];
        $time = explode(':', $time)[0]. ':' .explode(':', $time)[1];
        
        return view(dashboard("quiz", "storeORupdate"), compact('quiz', 'time'));
    }

    /**
     * Menampilkan detil data kuis.
     * 
     * @param  Int              $id      
     * @param  OfficeExcel      $excel   
     * @param  QuestionsControl $question
     * @return \Illuminate\View\View               
     */
    public function show(Int $id, OfficeExcel $excel, QuestionsControl $question)
    {
    	if (auth()->user()) {
            $quiz = $this->quiz->findWithLessonsAndStudentsQuiz($id);
        }
        else {
            // Get student ID
            $student = student()->getId();
            $quiz = $this->quiz->findWithLessonsAndStudent($id, $student);
        }

        $questions = $question->from($quiz->file_path, $excel)->count();

        return view(dashboard("quiz", "detail"), compact('quiz', 'questions'));
    }

    /**
     * Simpan record kuisioner baru kedalam database
     * tabel el_quizs.
     *  
     * @param  Request          $request   
     * @param  StoreTeacherQuiz $validator 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, StoreTeacherQuiz $validator)
    {   
        $validator->validated();

        $class = extract_class_room($request->class_room);
        $pararel = $class['pararel'];
        $grade = $class['grade'];
        
        $this->quiz->createThroughFile([
        	'per_year_id'     => school_year()->id,
            'semester_id'     => DB::table('semester')->where('status', 'aktif')->first()->id,
            'user_id'         => $request->user_id,
            'lesson_id'       => $request->lesson_id,
            'class_room'      => $grade,
            'pararel'         => $pararel,
            'title'           => ucfirst($request->title),
            'description'     => $request->description,
            'starting_time'   => Date::parse($request->datetime)->toDateTimeString(), 
            'weight_multiple' => $request->weight_multiple,
            'weight_essay'    => $request->weight_essay,
            'count_down'      => $request->count_down == null ? 120 : $request->count_down,
            'file_path'       => $request->file_path
        ], "public/management/quiz/{$grade}{$pararel}/lid_{$request->lesson_id}");

        return redirect()->route('quiz')->with('store', trans('confirmation.store', ['data' => "Kuis"]));
    }

    /**
     * Update record kuisioner ke database tabel el_quisz.
     * 
     * @param  Int               $id       
     * @param  Request           $request  
     * @param  UpdateTeacherQuiz $validator
     * @return \Illuminate\View\View                  
     */
    public function update(Int $id, Request $request, UpdateTeacherQuiz $validator)
    {
        $validator->validated();
        $class = extract_class_room($request->class_room);
        $pararel = $class['pararel'];
        $grade = $class['grade'];
        $lid = $request->lesson_id;
        $cls = implode(' ', array_values($class));
       
        try {
            $quiz = $this->quiz->find($id);
            $oldpath = $quiz->file_path;
            $pathToFile = explode('/', $oldpath)[5];
            $properties = [
            	'per_year_id'     => school_year()->id,
	            'semester_id'     => DB::table('semester')->where('status', 'aktif')->first()->id,
	            'user_id'         => $request->user_id,
	            'lesson_id'       => $lid,
	            'class_room'      => $grade,
	            'pararel'         => $pararel,
	            'title'           => ucfirst($request->title),
	            'description'     => $request->description,
	            'starting_time'   => Date::parse($request->datetime)->toDateTimeString(), 
	            'weight_multiple' => $request->weight_multiple,
	            'weight_essay'    => $request->weight_essay,
	            'count_down'      => $request->count_down == null ? 120 : $request->count_down,
	            'file_path'       => $request->file_path
            ];

            if (null($request->file_path)) {

                if ($quiz->lesson_id != $lid && !is_equal($quiz->class_room, $grade)) {
                    $newpath = "storage/management/quiz/$cls/lid_$lid/$pathToFile";
                    $properties['class_room'] = $grade;
                    $properties['lesson_id'] = $lid;
                    $properties['file_path'] = $newpath;

                    $this->quiz->updateClassRoomAndLessonId($id, $properties);
                }
                else if ($quiz->class_room !== $grade) {
                    $newpath = "storage/management/quiz/$cls/lid_$lid/$pathToFile";
                    $properties['class_room'] = $grade;
                    $properties['file_path'] = $newpath;

                    $this->quiz->updateClassroom($id, $properties);
                }
                else if ($quiz->lesson_id != $lid) {
                    $newpath = "storage/management/quiz/$cls/lid_$lid/$pathToFile";
                    $properties['lesson_id'] = $lid;
                    $properties['file_path'] = $newpath;

                    $this->quiz->updateLessonId($id, $properties);
                } 
                else {
                    $this->quiz->update($id, array_except($properties, ['file_path']));
                }

                if (isset($newpath)) {
                    if (!File::exists(dirname($newpath))) 
                        File::makeDirectory(dirname($newpath), 0755, true);
                    File::move($oldpath, $newpath);
                }
            }
            else {
                
                $path = $this->quizPath($request);
                
                $this->quiz->updateWithPath($id, $properties, $path);
            }
        } catch (\Exception $e) { 
            return redirect()->route("quiz.detail", $id);
        }

        return redirect()->route("quiz.detail", $id)->with('update', trans('confirmation.update', [
        	'data' => "Kuis"])
    	);
    }

    /**
     * Hapus record kuisioner dari database.
     * 
     * @param  Int    $id
     * @return \Illuminate\Http\RedirectResponse  
     */
    public function delete(Int $id)
    {
    	$this->quiz->deleteThroughFile($id);

        return redirect()->route("quiz")->with("delete", trans('confirmation.delete', ['data' => "Kuis"]));
    }
}
