<?php

namespace App\Http\Controllers\Questionnaire;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Services\Quiz\QuestionsControl;
use App\Services\OfficeDocuments\OfficeExcel;
use App\Repositories\Eloquent\Criteria\EagerLoad;
use App\Repositories\Eloquent\ORM_QuizRepository as Quiz;
use App\Libraries\AutomatedGrading\Corrector\EssayAnswerCorrector;
use App\Libraries\AutomatedGrading\Corrector\MultipleChoiceAnswerCorrector;

class TeacherController extends Controller
{
	/**
	 * Repositori model.
	 * 
	 * @var Quiz
	 */
	protected $quiz;

	/**
	 * Membuat instance baru.
	 * 
	 * @param  Quiz $quiz
	 * @return Void 
	 */
	public function __construct(Quiz $quiz)
	{
		$this->quiz = $quiz;
	}

	/**
	 * Display kumpulan soal kuisioner berdasarkan id quiz.
	 * 
	 * @param  Int\Integer      $id      
	 * @param  Request          $request 
	 * @param  OfficeExcel      $excel   
	 * @param  QuestionsControl $question
	 * @return \Illuminate\View\View                  
	 */
	public function show(Int $id, Request $request, OfficeExcel $excel, QuestionsControl $question)
	{
		$quiz = $this->quiz->findWithLessons($id);
        $questions = $question->from($quiz->file_path, $excel)->get();

        return view(dashboard("quiz", "show"), compact('quiz', 'questions'));
	}

	/**
	 * Koreksi jawaban kuisioner siswa dan menyimpan jawaban kedalam
	 * file baru lalu menyimpan nilai kuisioner siswa kedalam database.
	 *  
	 * @param  Int              $id  
	 * @param  Request          $request  
	 * @param  OfficeExcel 		$excel
	 * @param  QuestionsControl $questions
	 * @return \Illuminate\Http\RedirectResponse                
	 */
    public function store(Int $id, Request $request, OfficeExcel $excel, QuestionsControl $questions)
    {
    	$std = student();
    	$multiple = 0;
    	$essay = 0; 
    	$quiz = $this->quiz->find($id);
    	$exercise = $questions->from($quiz->file_path, $excel)->get();
		$stdClass = $std->getClass() . $std->getPararel();
		$date = explode(' ', $quiz->starting_time)[0];
		$filename = $std->getId(). '-' .$std->getName(). '-' . $std->getNisn();
		$path_to_file = "storage/student/quiz/{$stdClass}/lid_{$quiz->lesson_id}/{$date}";

    	Excel::create($filename, function($excel) use ($request, $quiz, $exercise, &$multiple, &$essay) {
			if ($request->has('multiple')) {
	    		$corrector = new MultipleChoiceAnswerCorrector($exercise['multiple'], $quiz->weight_multiple);
	    		$corrector->setAnswers($request->multiple);
	    		$corrector->correction();

	    		$excel->sheet("MultipleChoice", function($sheet) use ($corrector, &$multiple) {
					$sheet->cell('A1', function($cell) {
						$cell->setBackground("#18a566");
						$cell->setFontColor("#ffffff");
						$cell->setValue('Nomor');
					});
					$sheet->cell('B1', function($cell) {
						$cell->setBackground("#18a566");
						$cell->setFontColor("#ffffff");
						$cell->setValue('Answers');
					});
					$sheet->cell('C1', function($cell) {
						$cell->setBackground("#18a566");
						$cell->setFontColor("#ffffff");
						$cell->setValue('Answers Key');
					});

					$sheet->setWidth(['A' => 10, 'B' => 20, 'C' => 20]);

					$row = 1;
					foreach($corrector->getQuestions() as $key => $value) {
						$sheet->row(++$row, [
							($key+1), 
							$corrector->getAnswers()[$key], 
							strtoupper($corrector->getAnswersKey()[$key])
						]);
					}

					$multiple = $corrector->getScore();
				});
	    	}

	    	if ($request->has('essay')) {
	    		$corrector = new EssayAnswerCorrector($exercise['essay'], $quiz->weight_essay);
	    		$corrector->setAnswers($request->essay);
	    		$corrector->correction();

				$excel->sheet("Essay", function($sheet)  use ($corrector, &$essay){
					$sheet->cell('A1', function($cell) {
						$cell->setBackground("#18a566");
						$cell->setFontColor("#ffffff");
						$cell->setValue('Nomor');
					});
					$sheet->cell('B1', function($cell) {
						$cell->setBackground("#18a566");
						$cell->setFontColor("#ffffff");
						$cell->setValue('Answers');
					});
					$sheet->cell('C1', function($cell) {
						$cell->setBackground("#18a566");
						$cell->setFontColor("#ffffff");
						$cell->setValue('Answers Key');
					});
					$sheet->cell('D1', function($cell) {
						$cell->setBackground("#18a566");
						$cell->setFontColor("#ffffff");
						$cell->setValue('Score');
					});

					$sheet->setWidth(['A' => 10, 'B' => 100, 'C' => 100, 'D' => 20]);

					$row = 1;
					foreach($corrector->getQuestions() as $key => $value) {
						$sheet->row(++$row, [
							($key+1), 
							$corrector->getAnswers()[$key], 
							$corrector->getAnswersKey()[$key],
							$corrector->answersScore[$key]
						]);
					}

					$essay = $corrector->getScore();
				});
	    	}
		})->store('xlsx', $path_to_file);

    	$this->quiz->withCriteria([
			new EagerLoad(["lessons"])
		])->find($id)->students()->attach($std->getId(), [
    		'per_year_id'      => school_year()->id,
            'semester_id'      => DB::table('semester')->where('status', 'aktif')->first()->id,
            'student_id'       => $std->getId(),
            'multiple_scores'  => $multiple,
            'essay_scores'     => $essay,
            'final_scores'     => $multiple + $essay,
            'file_path'		   => "{$path_to_file}/{$filename}.xlsx"
		]);

		return redirect()->route("student.quiz")->with('success', trans('confirmation.store_student_quiz'));
    }
}
