<?php

namespace App\Http\Controllers\AuthStudent;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers as AuthenticatesStudent;
use App\Models\Actors\Student;

class LoginController extends Controller
{
	use AuthenticatesStudent;

    public function showLoginForm()
    {
        return view(dashboard("welcome", "index"));
    }

    public function authentication(Request $request)
    {
        $this->validate($request, [
            'nisn'     => "required",
            'password' => "required"
        ]);

        if (Auth::guard("student")->attempt($request->except("_token", "name"))) {

            return to_route("student.home");
        }

        $unauthorized = [
            'msg' => trans('confirmation.unauthenticate')
        ];

        return response()->json($unauthorized, 401);  
    }

    public function logout()
    {
        Auth::guard("student")->logout();

        return redirect()->to('/');
    }

    public function username()
    {
        return "nisn";
    }

    protected function guard()
    {
    	return Auth::guard("student");
    }
}
