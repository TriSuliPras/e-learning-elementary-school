<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Actors\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view(dashboard("welcome", "index"));
    }

    public function authentication(Request $request)
    {
        $this->validate($request, [
            'name'     => "required",
            'password' => "required"
        ]);

        if (! auth()->attempt($request->except("_token", "nisn"))) {
            
            $unauthorized = [
                'msg' => trans('confirmation.unauthenticate')
            ];
            return response()->json($unauthorized, 401);    
        }

        return to_route("management.home");
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->to('/');
    }

    protected function username()
    {
        return "name";
    }
}
