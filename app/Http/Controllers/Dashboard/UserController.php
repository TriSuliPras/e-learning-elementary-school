<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\{
	ORM_UserRepository,
    ORM_MaterialRepository,
    ORM_TaskRepository,
    ORM_NewsRepository,
	ORM_QuizRepository
};

class UserController extends Controller
{
	protected $users;
    protected $materials;
    protected $tasks;
	protected $quizs;
    protected $news;

	public function __construct(
        ORM_UserRepository $users,
        ORM_MaterialRepository $materials, 
        ORM_TaskRepository $tasks,
        ORM_NewsRepository $news,
        ORM_QuizRepository $quizs
    ) {
		$this->users = $users;
        $this->quizs = $quizs;
        $this->materials = $materials;
        $this->tasks = $tasks;
        $this->news = $news;
		$this->middleware("auth:web"); 
	}

    public function home()
    {
    	$tasks = $this->users->getTasks();
    	$quizs = $this->quizs->getAll();
        $quizToday = $this->quizs->getQuizToday();
        $materials = $this->materials->getAll();
        $tasks = $this->tasks->allByPeriode();
        $taskToday = $this->tasks->getTaskToday();
        $news = $this->news->findWhereReceiver('Common');
    	
    	return view("dashboard.management.home", 
            compact('tasks', 'taskToday', 'quizs', 'quizToday', 'materials', 'news')
        );
    }
}
