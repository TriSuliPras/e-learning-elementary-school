<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\InvalidDay;
use App\Rules\InvalidMonth;

class UpdateEmployeeProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nip' => "required|numeric",
            'nama_pegawai' => "required",
            'alamat' => "required",
            'no_telp' => "required",
            'foto_pegawai' => "image|mimes:jpg,jpeg,png",
            'CPNS_TMT' => 'nullable|min:4|max:4',
            'PNS_TMT' => 'nullable|min:4|max:4',
            'tmt_guru_tanggal' => [
                'nullable',
                'max:2',
                new InvalidDay
            ],
            'tmt_guru_bulan' => [
                'nullable',
                'max:2',
                new InvalidMonth
            ],
            'tmt_guru_tahun' => [
                'nullable',
                'min:4',
                'max:4'
            ],
            'sertifikasi_tahun' => [
                'nullable',
                'min:4',
                'max:4'
            ]
        ];
    }
}
