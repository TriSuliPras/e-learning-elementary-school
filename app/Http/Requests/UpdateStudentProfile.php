<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStudentProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'foto_siswa'  => 'nullable|mimes:jpg,jpeg,png',
            'tinggi_badan' => [
                'nullable',
                'numeric',
            ],
            'berat_badan' => [
                'nullable',
                'numeric',
            ],
            'no_tlp' => [
                'nullable',
                'numeric',
                'digits_between:10,10'
            ],  
            'no_hp' => [
                'nullable',
                'numeric',
                'digits_between:11,12'
            ],
            'email' => "nullable|email",
            'jarak_rumah_kesekolah_km' => [
                'nullable',
                'numeric'
            ],
            'ijazahterakhir_tahun' => [
                'nullable',
                'numeric'
            ],
            'npsn' => [
                'nullable',
                'numeric'
            ],
            'nama_ayah' => 'required',
            'tahunlahir_ayah' => [
                'required',
                'min:4',
                'max:4'
            ],
            'pekerjaan_ayah' => 'required',
            'pendidikan_ayah' => 'required',
            'penghasilan_ayah' => 'required',
            'nama_ibu' => 'required',
            'tahunlahir_ibu' => [
                'required',
                'min:4',
                'max:4'
            ],
            'pekerjaan_ibu' => 'required',
            'pendidikan_ibu' => 'required',
            'penghasilan_ibu' => 'required',
            'nama_wali' => 'required',
            'tahunlahir_wali' => [
                'required',
                'min:4',
                'max:4'
            ],
            'pekerjaan_wali' => 'required',
            'pendidikan_wali' => 'required',
            'penghasilan_wali' => 'required'
        ];
    }
}
