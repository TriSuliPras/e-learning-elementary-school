<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\NotGreaterThanOneHundred;
use App\Rules\NotGreaterThanOneHundredIfAddedUp;

class UpdateTeacherQuiz extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'title'           => "required|max:75",
            'lesson_id'       => "required",
            'class_room'      => "required",
            'datetime'        => "required|unique:el_quizs,starting_time",
            'weight_multiple' => [
                'required_without:weight_essay', 
                'required_unless:weight_essay,100',
                'max:3', 
                new NotGreaterThanOneHundred,
                new NotGreaterThanOneHundredIfAddedUp($request->weight_essay)
            ],
            'weight_essay'    => [
                'required_without:weight_multiple', 
                'required_unless:weight_multiple,100',
                'max:3', 
                new NotGreaterThanOneHundred,
                new NotGreaterThanOneHundredIfAddedUp($request->weight_multiple)
            ],
            'count_down'      => "digits_between:1,3"
        ];
    }
}
