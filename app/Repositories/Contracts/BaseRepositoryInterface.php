<?php 

namespace App\Repositories\Contracts;

interface BaseRepositoryInterface
{	
	public function all();

	public function find($id);

	public function findWhere($colums, $value);

	public function findAndWhere($column1, $value1, $colum2, $value2);
	
	public function findAndWhereFirst($column1, $value1, $colum2, $value2);

	public function findWhereFirst($colums, $value);

	public function findWherePaginate($colums, $value, $page);

	public function findAndWherePaginate($column1, $value1, $colum2, $value2, $page);

	public function findWhereNotIn($column, array $value);

	public function findWhereWithNotIn($column, $value, $columns, $values);

	public function findWithPaginate($id, $perPage = 10);

	public function findAndDelete($column, $value);

	public function paginate($perPage = 10);

	public function create(array $properties);

	public function update($id, array $properties);

	public function delete($id);
}