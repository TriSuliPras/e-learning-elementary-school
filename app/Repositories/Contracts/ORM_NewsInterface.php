<?php 

namespace App\Repositories\Contracts;

interface ORM_NewsInterface
{
	public function getAll();

	public function store(array $properties);

	public function storeClassCategory(array $createProps);

	public function storeStudentCategory(array $createProps);

	public function updated($id, array $properties);

	public function destroy($id);
}