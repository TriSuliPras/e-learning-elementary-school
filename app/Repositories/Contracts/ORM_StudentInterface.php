<?php 

namespace App\Repositories\Contracts;

interface ORM_StudentInterface
{
	public function updateWithImageFile(int $id, array $properties);
	
	public function getId();
	
	public function getName();

	public function getNisn();

	public function getClass();

	public function getAdministration($id);

	public function findStudentQuiz($quizId, $studentId);

	public function showTask($taskID);
}