<?php 

namespace App\Repositories\Contracts;

interface ORM_ClassRoomInterface
{	
	public function getQuizsByClass($className);
}