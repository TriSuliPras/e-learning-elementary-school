<?php 

namespace App\Repositories\Contracts;

interface ORM_MaterialInterface
{	
	public function createWithFile(array $properties, string $path);
}