<?php 

namespace App\Repositories\Contracts;

interface ORM_CrudJobs
{
	public function create(array $properties);

	public function show($id);

	public function update($id, array $properties);

	public function delete($id);
}