<?php 

namespace App\Repositories\Contracts;

interface ORM_ManagementInterface
{
	public function updateWithImageFile(int $id, array $properties);
}