<?php 

namespace App\Repositories\Contracts;

interface ORM_LessonInterface
{
	public function getWhereStatus($status);

	public function getWithCurriculums();
}