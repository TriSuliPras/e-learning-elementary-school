<?php 

namespace App\Repositories\Contracts;

use App\Services\OfficeDocuments\OfficeExcel;
use App\Services\Quiz\QuestionsControl;

interface ORM_QuizInterface
{	
	/**
	 * Create quiz and save the file
	 *
	 * @param Array $properties <\Illuminate\Http\Request>
	 * @param String $path <Quiz file path>
	 * @return App\Models\Data\Quiz
	 */
	public function createThroughFile(array $properties, string $path);
	
	/**
	 * Update Quiz and file path directory
	 *
	 * @param $id <Quiz ID>
	 * @param Array $properties <\Illuminate\Http\Request>
	 * @param String $path <Quiz file path>
	 * @return App\Models\Data\Quiz
	 */
	public function updateWithPath($id, array $properties, string $path);
	
	/**
	 * Update Quiz class_room field if it has the new value
	 * if has, then will cut the quiz file into the new directory
	 * depend on the class_room
	 *
	 * @param $id <Quiz ID>
	 * @param Array $properties <\Illuminate\Http\Request>
	 * @return App\Models\Data\Quiz
	 */
	public function updateClassroom($id, array $properties);
	
	/**
	 * Update Quiz lesson_id field if it has the new value
	 * if has, then will cut the quiz file into the new directory
	 * depend on the lesson_id
	 *
	 * @param $id <Quiz ID>
	 * @param Array $properties <\Illuminate\Http\Request>
	 * @return App\Models\Data\Quiz
	 */
	public function updateLessonId($id, array $lessonId);
	
	/**
	 * Update Quiz class_room adn Lesson_id field if it has the new value
	 * if has, then will cut the quiz file into the new directory
	 * depend on the class_room adn Lesson_id
	 *
	 * @param $id <Quiz ID>
	 * @param Array $properties <\Illuminate\Http\Request>
	 * @return App\Models\Data\Quiz
	 */
	public function updateClassroomAndLessonId($id, array $properties);
	
	/**
	 * Update Quiz status to true,
	 * so if the status is true, then quiz is over
	 *
	 * @param $status <Quiz status>
	 * @return App\Models\Data\Quiz
	 */
	public function updateQuizStatus($status);
	
	/**
	 * Update student essay scores, scores it's given
	 * manually by the teacher
	 * 
	 * @param $quizId 
	 * @param Array $properties <\Illuminate\Http\Request>
	 * @return App\Models\Data\Quiz
	 */
	public function updateStudentEssayScores(QuestionsControl $question, $quizId, array $properties);
	
	/**
	 * Delete Quiz with that file
	 * 
	 * @param $id <Quiz ID>
	 * @return App\Models\Data\Quiz
	 */
	public function deleteThroughFile($id);

	/**
	 * get All quiz for student and management
	 *
	 * @return App\Models\Data\Quiz 
	 */
	public function getWithLessons();
	
	/**
	 * Get Quiz file path 
	 *
	 * @param $id <Quiz ID>
	 * @return String 
	 */
	public function getQuizPathById($id) : String;

	/**
	 * Get student Quiz file path 
	 *
	 * @param $id <Quiz ID>
	 * @param $studentId
	 * @return String 
	 */
	public function getStudentQuizPathById($id, $studentId) : String;
	
	/**
	 * find quiz with realtions lesson By id
	 * and student by id whiches depend on student ID
	 *
	 * @param $quizId
	 * @param $studentId 
	 * @return App\Models\Data\Quiz 
	 */
	public function findWithLessonsAndStudent($quizId, $studentId);

	/**
	 * find quiz with realtions lesson By id
	 * and all student by id whiches depend on student ID
	 *
	 * @param $quizId
	 * @param $studentId 
	 * @return App\Models\Data\Quiz 
	 */
	public function findWithLessonsAndStudentsQuiz($id);
	
	/**
	 * find all quiz with realtions lesson By id
	 *
	 * @param $id <Quiz ID>
	 * @return App\Models\Data\Quiz 
	 */
	public function findWithLessons($id);
}