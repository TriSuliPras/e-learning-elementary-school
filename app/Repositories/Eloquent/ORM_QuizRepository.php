<?php 

namespace App\Repositories\Eloquent;

use DB;
use Cache;
use File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Data\{Quiz, QuizPath};
use App\Services\OfficeDocuments\OfficeExcel;
use App\Repositories\BaseRepository;
use App\Repositories\Contracts\ORM_QuizInterface;
use App\Repositories\Eloquent\Criteria\{EagerLoad, Teacher};
use App\Services\Quiz\QuestionsControl;

class ORM_QuizRepository extends BaseRepository implements ORM_QuizInterface
{
	public function entity()
	{
		return Quiz::class;
	}

	/**
	 * Create quiz and save the file
	 *
	 * @param Array $properties <\Illuminate\Http\Request>
	 * @param String $path <Quiz file path>
	 * @return App\Models\Data\Quiz
	 */
	public function createThroughFile(array $properties, string $path)
	{
		$file = $properties['file_path'];
		$filename = uniqid(uniqid()) . '.' .$file->getClientOriginalExtension();
		
		Storage::putFileAs($path, $properties['file_path'], $filename);
		
		$path = str_replace("public", "storage", $path) . "/$filename";
		
		$properties['file_path'] = $path;
		$properties['file_name'] = $file->getClientOriginalName();

		return $this->create($properties);
	}

	/**
	 * update quiz including file path,
	 * if quiz file has change, then remove di oldfile
	 * and replace it with the new one. 
	 *
	 * @param $id <find quiz by id> 
	 * @param Array $properties <class_room, file_path>
	 * @param String $path
	 * @return App\Models\Data\Quiz
	 */
	public function updateWithPath($id, array $properties, string $path)
	{
		$quiz = $this->find($id);
	
		$file = $properties['file_path'];
		$filename = uniqid(uniqid()) . '.' .$file->getClientOriginalExtension();
		
		if ( File::exists($quiz->file_path) )
			File::delete($quiz->file_path);

		Storage::putFileAs($path, $properties['file_path'], $filename);
		
		$path = str_replace("public", "storage", $path) . "/$filename";
		$properties['file_path'] = $path;
		$properties['file_name'] = $file->getClientOriginalName();
		
		return $quiz->update($properties);
	}

	/**
	 * update quiz classroom, 
	 * and move quiz path into new directory
	 * relative to the class_room field.
	 * 
	 * @param Array $properties <class_room, file_path>
	 * @param $id <find quiz by id>
	 * @return App\Models\Data\Quiz
	 */
	public function updateClassroom($id, array $properties)
	{
		return $this->update($id, $properties);
	}
	
	/**
	 * update quiz lesson_id, 
	 * and move quiz path into new directory
	 * relative to the lesson_id field.
	 * 
	 * @param Array $properties <lesson_id, file_path>
	 * @param $id <find quiz by id>
	 * @return App\Models\Data\Quiz
	 */
	public function updateLessonId($id, array $properties)
	{	
		return $this->update($id, $properties);
	}

	/**
	 * update quiz lesson_id, class_room 
	 * and move quiz path into new directory
	 * relative to the lesson_id and class_room field.
	 * 
	 * @param Array $properties <lesson_id, class_room, file_path>
	 * @param $id <find quiz by id>
	 * @return App\Models\Data\Quiz
	 */
	public function updateClassroomAndLessonId($id, array $properties)
	{
		return $this->update($id, $properties);
	}
	
	/**
	 * Update Quiz status to true,
	 * so if the status is true, then quiz is over
	 *
	 * @param $status <Quiz status>
	 * @return App\Models\Data\Quiz
	 */
	public function updateQuizStatus($status)
	{
		$quizs = $this->all();

		if (! empty($quizs->toArray())) {
			
			foreach ($quizs as $quiz) {	
				
				if ($quiz->status == "true") {
					continue;
				}
				else {
				$quizTime = \Carbon\Carbon::parse($quiz->starting_time);
		
					if ($quizTime->toDateTimeString() < \Carbon\Carbon::now()->toDateTimeString() && 
						$quizTime->addMinutes($quiz->count_down)->toDateTimeString() < \Carbon\Carbon::now()->toDateTimeString()
					) {
						$this->update($quiz->id, ['status' => $status]);
					}
					
				} 
			}
		}
	}
	
	/**
	 * Update student essay scores, scores it's given
	 * manually by the teacher
	 * 
	 * @param $quizId 
	 * @param Array $properties <\Illuminate\Http\Request>
	 * @return App\Models\Data\Quiz
	 */
	public function updateStudentEssayScores(QuestionsControl $question, $quizId, array $properties)
	{	
		$multipleScore = $properties['mtp_score'];

		$essayScore = $question->correctionEssayQuestions($this, $properties, $quizId)
							   ->getResultEssayQuestion();

		$finalScores = $multipleScore + $essayScore;
		
		return DB::table(
			'el_student_quizs'
		)->where(
			'quiz_id', $quizId
		)->where(
			'student_id', $properties['student_id']
		)->update([
			'is_updated'   => true,
			'essay_scores' => $essayScore,
			'final_scores' => $finalScores
		]);
	}

	public function updateStudentQuizStatus(int $quizId, int $stdId)
	{
		return DB::table(
			'el_student_quizs'
		)->where(
			'quiz_id', $quizId
		)->where(
			'student_id', $stdId
		)->update([
			'status' => "lock"
		]);
	}
	
	/**
	 * Delete Quiz with that file
	 * 
	 * @param $id <Quiz ID>
	 * @return App\Models\Data\Quiz
	 */
	public function deleteThroughFile($id)
	{
		$quizfile = $this->getQuizPathById($id);

		if ( File::exists($quizfile) ) {

			DB::table('el_student_quizs')->select([
				'file_path'
			])->where('quiz_id', $id)->get()->each(function ($item) {
				File::delete($item->file_path);
			});

			DB::table('el_student_quizs')->where('quiz_id', $id)->delete();

			File::delete($quizfile);

			return $this->delete($id);
		}
		else
			return $this->delete($id);
	}

	public function getAll()
	{
		if (management()->loggedIn()) {
			
			if (management()->userIs("admin")) {
				
				return $this->all();
			}
			else if (management()->userIs("guru")) {

				return $this->findWhere('user_id', management()->getId());
			}
		}

		return $this->findWhere('class_room', student()->getClass());
	}

	public function getQuizToday()
	{
		$midnight = \Carbon\Carbon::parse(date('Y-m-d 00:00:00'))->toDateTimeString();
		$night = \Carbon\Carbon::parse(date('Y-m-d 23:59:59'))->toDateTimeString();

		if (management()->loggedIn()) {
			
			if (management()->userIs('admin')) {
					
				return $this->Entity->whereBetween('starting_time', [$midnight, $night])->get();
			}
			else if (management()->userIs('guru')) {
				
				$uid = management()->getId();
				$task = $this->Entity->where('user_id', $uid);
				
				return $task->whereBetween('starting_time', [$midnight, $night])->get();

			}
		}
	}
	
	/**
	 * get All quiz for student and management
	 *
	 * @return App\Models\Data\Quiz 
	 */
	public function getWithLessons()
	{
		$quiz = $this->withCriteria([
			new Teacher,	
			new EagerLoad(["lessons"])
        ]);

        if (management()->loggedIn()) {
			
			if (management()->userIs("admin")) {
				
				return $quiz->all();
			}
			else if (management()->userIs("guru")) {
				
				return $quiz->findWhere('user_id', management()->getId());
			}
		}

		return $quiz->findWhere('class_room', student()->getClass());
	}
	
	/**
	 * Get Quiz file path 
	 *
	 * @param $id <Quiz ID>
	 * @return String 
	 */
	public function getQuizPathById($id) : String
	{
		return $this->find($id)->file_path;
	}
	
	/**
	 * Get student Quiz file path 
	 *
	 * @param $id <Quiz ID>
	 * @param $studentId
	 * @return String 
	 */
	public function getStudentQuizPathById($id, $studentId) : String 
	{
		$quiz = $this->withCriteria([
			new EagerLoad(["students" => function ($query) use ($studentId) {
				$query->select(
					"el_student_quizs.file_path"
				)->where('el_student_quizs.student_id', $studentId);
			}])
		])->find($id);

		return $quiz->students[0]->file_path;
	}

	public function getTodayByClassRoomAndPararel(string $classroom, string $pararel)
	{
		$midnight = \Carbon\Carbon::parse(date('Y-m-d 00:00:00'))->toDateTimeString();
		$night = \Carbon\Carbon::parse(date('Y-m-d 23:59:59'))->toDateTimeString();
		
		return $this->Entity->where(
			'class_room', $classroom
		)->where(
			'pararel', $pararel
		)->whereBetween(
			'starting_time', [$midnight, $night]
		)->get();
	}
	
	/**
	 * find all quiz with realtions lesson By id
	 *
	 * @param $id <Quiz ID>
	 * @return App\Models\Data\Quiz 
	 */
	public function findWithLessons($id)
	{
		return $this->withCriteria([new Teacher, new EagerLoad(["lessons"])])->find($id);
	}
	
	/**
	 * find quiz with realtions lesson By id
	 * and student by id whiches depend on student ID
	 *
	 * @param $quizId
	 * @param $studentId 
	 * @return App\Models\Data\Quiz 
	 */
	public function findWithLessonsAndStudent($quizId, $studentId)
	{
		return $this->withCriteria([
			new Teacher,
			new EagerLoad(["lessons"]),
			new EagerLoad(["students" => function ($query) use ($studentId) {
				$query->select(
					"student_id", 
					"nisn", 
					"nama_siswa", 
					"multiple_scores", 
					"essay_scores", 
					"final_scores",
					"el_student_quizs.status",
					"el_student_quizs.file_path"
				)->where('el_student_quizs.student_id', $studentId);
			}])
		])->find($quizId);
	}
	
	/**
	 * find quiz with realtions lesson By id
	 * and all student by id whiches depend on student ID
	 *
	 * @param $quizId
	 * @param $studentId 
	 * @return App\Models\Data\Quiz 
	 */
	public function findWithLessonsAndStudentsQuiz($id)
	{
		return $this->withCriteria([
			new Teacher,
			new EagerLoad(["lessons"]),
			new EagerLoad(["students" => function ($query) {
				$query->select(
					"student_id", 
					"nisn", 
					"nama_siswa", 
					"multiple_scores", 
					"essay_scores", 
					"final_scores",
					"el_student_quizs.status");
			}])
		])->find($id);
	}

	public function findByClassRoomAndPararel(string $classroom, string $pararel)
	{
		return $this->findAndWhere('class_room', $classroom, 'pararel', $pararel);
	}
}