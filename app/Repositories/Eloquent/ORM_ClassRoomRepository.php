<?php 

namespace App\Repositories\Eloquent;

use App\Models\Actors\User;
use App\Models\Data\{ClassRoom, ClassNews};
use App\Repositories\BaseRepository;
use App\Repositories\Contracts\ORM_ClassRoomInterface;
use App\Repositories\Eloquent\Criteria\{EagerLoad, Join, ByGroup};

class ORM_ClassRoomRepository extends BaseRepository implements ORM_ClassRoomInterface
{
	public function entity()
	{
		return ClassRoom::class;
	}

	public function getQuizsByClass($className)
	{
		return $this->withCriteria([
			new EagerLoad(["quizs" => function ($query) {
				
				$query->with(["lessons", "teachers" => function ($query) {
					$query->with(["employees" => function ($query) {
						$query->join("pegawai", "user_pegawai.pegawai_id", '=', "pegawai.id");
					}]);
				}]);
			}]),
			new ByGroup("nama_kelas")	
		])->findWhereFirst("nama_kelas", $className);
	}

	public function getNews($status, $classname, $palarel)
	{
		$classNews = ClassNews::where(
			"class_room", $classname
		)->where(
			"pararel", $palarel
		)->with([
			'news' => function($news) use ($status) {
				$news->with([
					'teachers' => function ($teachers) {
						$teachers->with([
							'employees' => function($employees) {
								$employees->join("pegawai", "user_pegawai.pegawai_id", '=', "pegawai.id");
							}	
						]);
					}
				])->where("status", $status);
			}
		])->get();

		foreach ($classNews as $k => $class) {
			
			if (is_null($class->news)) {
				
				unset($classNews[$k]);
			}
		}

		return $classNews;
	}
}