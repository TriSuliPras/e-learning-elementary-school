<?php 

namespace App\Repositories\Eloquent;

use App\Models\Data\Lesson;
use App\Repositories\BaseRepository;
use App\Repositories\Contracts\ORM_LessonInterface;
use App\Repositories\Eloquent\Criteria\{
	EagerLoad,
	Select
};

class ORM_LessonRepository extends BaseRepository implements ORM_LessonInterface
{
	public function entity()
	{
		return Lesson::class;
	}

	public function getWhereStatus($status)
	{
		return $this->findWhere('status', $status);
	}

	public function getWithCurriculums()
	{
		return $this->withCriteria([
			new EagerLoad(['curriculums'])
		])->findWhere('status', "Aktif");
	}
}