<?php 

namespace App\Repositories\Eloquent;

use DB;
use App\Models\Actors\User;
use App\Repositories\BaseRepository;
use App\Repositories\Eloquent\ORM_EmployeeRepository;
use App\Repositories\Contracts\ORM_UserInterface;
use App\Repositories\Eloquent\Criteria\{
	EagerLoad, 
	Join
};

class ORM_UserRepository extends BaseRepository implements ORM_UserInterface
{
	public function entity()
	{
		return User::class;
	}

	public function maxId()
	{
		return $this->Entity::max('id');
	}

	public function loggedIn()
	{
		return auth()->user();
	}

	public function userIs($role)
	{
		return auth()->user()->hasRole($role);
	}

	public function getId()
	{
		return auth()->user()->id;
	}

	public function getName()
	{
		return auth()->user()->name;
	}

	public function getMail()
	{
		return auth()->user()->email;
	}

	public function getEmployeeId()
	{
		return auth()->user()->pegawai_id;
	}

	public function getSchedule()
	{
		$schedules = DB::table(
			'waktu'
		)->join(
			'detil_jadwal', 'waktu.id', 'detil_jadwal.waktu_id'
		)->join(
			'matapelajaran', 'detil_jadwal.matapelajaran_id', 'matapelajaran.id'
		)->leftJoin('pegawai',
			function ($join) {
				if ($this->userIs('admin')) {
					$join->on('detil_jadwal.pegawai_id', 'pegawai.id');
				}
				else if ($this->userIs('guru')) {
					$join->on(
						'detil_jadwal.pegawai_id', 'pegawai.id'
					)->where('pegawai.id', '=', $this->getEmployeeId());
				}
			}
		)->join(
			'kelas', 'detil_jadwal.kelas_id', "kelas.id"
		)->where(
			'detil_jadwal.hari_id', $this->getDay(\Carbon\Carbon::now()->format('D'))->id
		)->orderBy(
			'waktu.pukul'
		)->select(
			"waktu.id", "waktu.jam_ke", "waktu.pukul",
			"matapelajaran.nama_matapelajaran AS lesson", 
			"pegawai.nama_pegawai AS teacher",
			"kelas.nama_kelas AS class", "kelas.palarel AS category"
		)->get();
		
		// dd($schedules->toArray());

		return $schedules;
	}

	public function getDay($day)
	{
		switch ($day) {
			case 'Sun':
				$day = "Minggu";
				break;
			case 'Mon':
				$day = "Senin";
				break;
			case 'Tue':
				$day = "Selasa";
				break;
			case 'Wed':
				$day = "Rabu";
				break;
			case 'Thu':
				$day = "Kamis";
				break;
			case 'Fri':
				$day = "Jumat";
				break;
			case 'Sat':
				$day = "Sabtu";
				break;
		}

		return DB::table(
			'hari'
		)->where(
			'hari', $day
		)->first();
	}

	public function getQuizs()
	{
		$quizs =  $this->withCriteria([
			new EagerLoad(["quizs" => function ($query) {
				$query->with(["lessons"]);
			}])
		])->find($this->getId());

		$index = 0;
		$allQuiz = [];
		foreach ($quizs->quizs as $quiz) {
			
			if (explode(' ', $quiz->starting_time)[0] === \Carbon\Carbon::now()->format('Y-m-d')) {
				
				$allQuiz[$index] = $quiz->toArray();
				$index++;
			} 
			else continue;
		}
		
		unset($quizs['quizs']);
		$quizs['quizs'] = $allQuiz;

		return $quizs;
	}

	public function getTasks()
	{
		$tasks = $this->withCriteria([
			new EagerLoad(["tasks" => function ($query) {
				$query->with(["lessons"]);
			}])
		])->find($this->getId());

		$index = 0;
		$allTasks = [];
		foreach ($tasks->tasks as $task) {
			
			if (explode(' ', $task->deadline)[0] === \Carbon\Carbon::now()->format('Y-m-d')) {
				
				$allTasks[$index] = $task->toArray();
				$index++;
			} 
			else continue;
		}
		
		unset($tasks['tasks']);
		$tasks['tasks'] = $allTasks;

		return $tasks;
	}

	public function lessons($relations)
	{
		$this->withCriteria([
			new EagerLoad(['teach' => function ($query) use ($relations) {

				$query->with([$relations => function ($query) {

					$query->where('user_id', $this->getId());
				}]);
			}])
		]);

		return $this;
	}

/*	public function get($relations, $lessonId)
	{	
		$this->withCriteria([
			new EagerLoad(['teach' => function ($query) use ($relations, $lessonId) {
				
				switch ($relations) {
					case 'materials':
						$query->with([$relations => function ($query) {
							
							$query->where('user_id', $this->getId());
							$query->with(["lessons", "classes"]);
						}])->where('lesson_id', $lessonId);
						break;

					case 'tasks':
						$query->with([$relations => function ($query) use ($lessonId) {
								$query->with(["students" => function ($query) {
									$query->select("nama_siswa", "nisn", "kelas", "pararel", "file_path", "scores");
								}]);
								$query->with(["lessons", "classes"]);
						}])->where('lesson_id', $lessonId);
						break;
					
					case 'quizs':
						$query->with([$relations => function ($query) use ($lessonId) {
							
							$query->with(["students" => function ($query) {
								$query->select('nama_siswa', 'nisn', 'kelas', 'multiple_scores', 'essay_scores');
							}]);
							$query->with(["lessons", "classes"]);
						}])->where('lesson_id', $lessonId);
						break;
				}
				
			}])
		]);

		return $this;
	}*/
}