<?php 

namespace App\Repositories\Eloquent;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Models\Data\Material;
use App\Repositories\BaseRepository;
use App\Repositories\Contracts\ORM_MaterialInterface;
use App\Repositories\Eloquent\Criteria\{EagerLoad, Teacher};

class ORM_MaterialRepository extends BaseRepository implements ORM_MaterialInterface
{
	public function entity()
	{
		return Material::class;
	}

	public function createWithFile(array $properties, string $path)
	{
		$file = $properties['file_path'];
		$filename = uniqid(uniqid()) . '.' .$file->getClientOriginalExtension();
		
		Storage::putFileAs($path, $properties['file_path'], $filename);
		
		$path = str_replace("public", "storage", $path) . "/$filename";
		
		$properties['file_path'] = $path;
		$properties['file_name'] = $file->getClientOriginalName();

		return $this->create($properties);
	}

	/**
	 * Update task and delete the oldfile 
	 * if the incoming request has a new file
	 * 
	 * @param $id <Task ID>
	 * @param Array $properties [\illuminate\Http\Request]
	 * @param $oldpath <this will be deleted>
	 * @param String $newpath <store the new one after deleted the oldpath>
	 * @return \App\Models\Data\Task 
	 */
	public function updateWithCreateNewFile($id, array $properties, $oldpath, string $newpath)
	{
		$file = $properties['file_path'];
		$filename = uniqid(uniqid()) . '.' .$file->getClientOriginalExtension();
		
		if (File::exists($oldpath)) {

			File::delete($oldpath);
		}

		Storage::putFileAs($newpath, $properties['file_path'], $filename);
		
		$newpath = str_replace("public", "storage", $newpath) . $filename;
		
		$properties['file_path'] = $newpath;
		$properties['file_name'] = $file->getClientOriginalName();

		return $this->update($id, $properties);
	}

	/**
	 * Update task and move the oldfile 
	 * if oldfile exists
	 * 
	 * @param $id <Task ID>
	 * @param Array $properties [\illuminate\Http\Request]
	 * @param String $oldpath <this will be deleted>
	 * @param String $newpath <store the new one after deleted the oldpath>
	 * @return \App\Models\Data\Task 
	 */
	public function updateWithMoveFile($id, array $properties, string $oldpath, string $newpath)
	{
		if (!File::exists(dirname($newpath))) {
            File::makeDirectory(dirname($newpath), 0755, true);
            File::move($oldpath, $newpath);
		}
		else
			File::move($oldpath, $newpath);

		$properties['file_path'] = $newpath;

		return $this->update($id, $properties);
	}

	/**
	 * Delete Task with that file
	 * 
	 * @param $id <Quiz ID>
	 * @return App\Models\Data\Quiz
	 */
	public function deleteWithFile($id)
	{
		$material = $this->find($id);

		if ( File::exists($material->file_path) ) {

			File::delete($material->file_path);

			return $this->delete($material->id);
		}
		else
			return $this->delete($material->id);
	}

	public function getRules()
	{
		return $this->Entity::RULE;
	}

	public function findByClassRoomAndPararel(string $classroom, string $pararel)
	{
		return $this->findAndWhere('class_room', $classroom, 'pararel', $pararel);
	}

	public function getAll()
	{
		$materials = $this->withCriteria([new Teacher]);

		if (management()->loggedIn()) {
			
			if (management()->userIs("admin")) {
				
				return $materials->all();
			}
			else if (management()->userIs("guru")) {

				return $materials->findWhere('user_id', management()->getId());
			}
		}

		return $materials->findWhere('class_room', student()->getClass());
	}
}