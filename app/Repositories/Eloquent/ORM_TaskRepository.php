<?php 

namespace App\Repositories\Eloquent;

use App\Models\Data\Task;
use App\Repositories\BaseRepository;
use App\Repositories\Contracts\ORM_TaskInterface;
use App\Repositories\Eloquent\Criteria\{EagerLoad, Teacher, Select, Join};
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ORM_TaskRepository extends BaseRepository implements ORM_TaskInterface
{
	public function entity()
	{
		return Task::class;
	}
	
	/**
	 * Get Task model rules
	 * 
	 * @return Array 
	 */
	public function getRules()
	{
		return $this->Entity::RULE;
	}

	public function allByPeriode(Int $start = 0, Int $end = 0, String $semester = null, String $s = null)
	{
		$periode = $start . '/' . $end;

		$tasks = $this->withCriteria([
			new Teacher,
			new EagerLoad([
				'lessons' => function ($l) {
					$l->select(['id', 'nama_matapelajaran']);
				}
			]),
			new Select([
				'el_tasks.id', 'user_id', 'per_year_id', 'lesson_id',
				'title', 'tahun_ajaran as year', 'pararel',
				'tahun_ajaran.status as periode_status', 'class_room',
			]),
			new Join('tahun_ajaran.id', 'per_year_id'),
		]);

		if (auth()->guard('web')->check()) {
			$tasks = $tasks->wheres(['user_id = ' . management()->getId()]);
		}
		else {
			$tasks = $tasks->wheres([
				'class_room = ' . student()->getClass(),
				'pararel = ' . student()->getPararel()
			]);
		}

		if ($start !== 0 && $end !== 0) {
			$tasks = $tasks->wheres([
				"tahun_ajaran.tahun_ajaran = $periode", 
			]);
		}

		if ($semester) {
			$tasks = $tasks->wheres([
				"semester_id = $semester", 
			]);
		}

		if (! is_null($s)) {
			$tasks = $tasks->wheres([
				"tahun_ajaran.status = $s", 
			]);
		}

		return $tasks->all();
	}

	/**
	 * Create task and including students relation
	 * and save the task file
	 * if file is present
	 * 
	 * @param Array $properties [\illuminate\Http\Request]
	 * @param String $path 
	 * @return \App\Models\Data\Task 
	 */
	public function createWithFileAndStudents(array $properties, string $path)
	{
		$file = $properties['file_path'];
		$filename = uniqid(uniqid()) . '.' .$file->getClientOriginalExtension();
		
		Storage::putFileAs($path, $properties['file_path'], $filename);
		
		$path = str_replace("public", "storage", $path) . "/$filename";
		
		$properties['file_path'] = $path;
		$properties['file_name'] = $file->getClientOriginalName();

		$task = $this->create($properties);
		
		$students = student()->withCriteria([
			new Select(['id'])
		])->findAndWhere('kelas', $properties['class_room'], 'pararel', $properties['pararel']);

		foreach ($students as $student) {
			$task->students()->attach($task->id, [
				'per_year_id'     => school_year()->id,
            	'semester_id'     => \DB::table('semester')->where('status', 'aktif')->first()->id,
				'student_id' => $student->id
			]);
		}
	}
	
	/**
	 * Create task and including students relation
	 * 
	 * @param Array $properties [\illuminate\Http\Request]
	 * @param String $path 
	 * @return \App\Models\Data\Task 
	 */
	public function createWithStudents(array $properties) 
	{
		$task = $this->create($properties);
		
		$students = student()->withCriteria([
			new Select(['id'])
		])->findAndWhere('kelas', $properties['class_room'], 'pararel', $properties['pararel']);

		foreach ($students as $student) {
			$task->students()->attach($task->id, [
				'per_year_id'     => school_year()->id,
            	'semester_id'     => \DB::table('semester')->where('status', 'aktif')->first()->id,
				'student_id' => $student->id
			]);
		}
	}
	
	/**
	 * Update task and delete the oldfile 
	 * if the incoming request has a new file
	 * 
	 * @param $id <Task ID>
	 * @param Array $properties [\illuminate\Http\Request]
	 * @param $oldpath <this will be deleted>
	 * @param String $newpath <store the new one after deleted the oldpath>
	 * @return \App\Models\Data\Task 
	 */
	public function updateWithCreateNewFile($id, array $properties, $oldpath, string $newpath)
	{
		$task = $this->find($id);
		$student_id = []; 
		$file = $properties['file_path'];
		$filename = uniqid(uniqid()) . '.' .$file->getClientOriginalExtension();
		
		if (File::exists($oldpath)) {

			File::delete($oldpath);
		}

		Storage::putFileAs($newpath, $properties['file_path'], $filename);
		
		$newpath = str_replace("public", "storage", $newpath) . $filename;
		
		$properties['file_path'] = $newpath;
		$properties['file_name'] = $file->getClientOriginalName();
		
		$students = student()->withCriteria([
			new Select(['id'])
		])->findAndWhere('kelas', $properties['class_room'], 'pararel', $properties['pararel']);
		
		foreach ($students as $i => $student) {
			$student_id[$i] = $student->id;
		}

		$task->update($properties);

		return $task->students()->sync($student_id);
	}

	/**
	 * Update task and move the oldfile 
	 * if oldfile exists
	 * 
	 * @param $id <Task ID>
	 * @param Array $properties [\illuminate\Http\Request]
	 * @param String $oldpath <this will be deleted>
	 * @param String $newpath <store the new one after deleted the oldpath>
	 * @return \App\Models\Data\Task 
	 */
	public function updateWithMoveFile($id, array $properties, string $oldpath, string $newpath)
	{
		$student_id = [];
		$task = $this->find($id);

		if (!File::exists(dirname($newpath))) {
            File::makeDirectory(dirname($newpath), 0755, true);
            File::move($oldpath, $newpath);
		}
		else
			File::move($oldpath, $newpath);

		$properties['file_path'] = $newpath;

		$students = student()->withCriteria([
			new Select(['id'])
		])->findAndWhere('kelas', $properties['class_room'], 'pararel', $properties['pararel']);
		
		foreach ($students as $i => $student) {
			$student_id[$i] = $student->id;
		}
		
		$task->update($properties);

		return $task->students()->sync($student_id);
	}

	public function updateWithSyncStudents($id, array $properties)
	{
		$student_id = [];
		$task = $this->find($id);

		$students = student()->withCriteria([
			new Select(['id'])
		])->findAndWhere('kelas', $properties['class_room'], 'pararel', $properties['pararel']);
		
		foreach ($students as $i => $student) {
			$student_id[$i] = $student->id;
		}
		
		$task->update($properties);

		return $task->students()->sync($student_id);
	}

	public function updateStudentScores(array $properties)
	{
		foreach ($properties['scores'] as $id => $value) {
			
			$this->find($properties['task_id'])->students()->updateExistingPivot($id, $value);
		}
	}

	public function updateStudentScoreStatus(int $taskId, int $stdId)
	{
		return DB::table(
			'el_student_tasks'
		)->where(
			'task_id', $taskId
		)->where(
			'student_id', $stdId
		)->update([
			'status' => "lock"
		]);
	}

	public function createStudentTask($taskId, $pathToFile)
	{	
		$std = student();
		$task = $this->find($taskId);

		$lid = $task->lesson_id;
        $cls = $std->getClass() . $std->getPararel();
        $student = $std->getId(). '-' .$std->getName(). '-' . $std->getNisn();
        $filename = "{$student} " . uniqid() . '.' .$pathToFile->getClientOriginalExtension();
        $date = explode(' ', $task->deadline)[0];

        $path = "public/student/task/{$cls}/lid_{$lid}/{$date}";
		
		Storage::putFileAs($path, $pathToFile, $filename);

		$pathToFile = str_replace("public", "storage", $path) . "/$filename";

		return $task->students()->syncWithoutDetaching([
			$std->getId() => ['file_path' => $pathToFile]
		]);
	}

	/**
	 * Delete Task with that file
	 * 
	 * @param $id <Quiz ID>
	 * @return App\Models\Data\Quiz
	 */
	public function deleteWithFile($id)
	{
		$task = $this->find($id);

		if ( File::exists($task->file_path) ) {

			File::delete($task->file_path);

			return $this->delete($task->id);
		}
		else
			return $this->delete($task->id);
	}

	public function findTaskWithLessonAndStudent(int $id, int $studentId)
	{
		return $this->withCriteria([
			new Teacher,
			new EagerLoad(["lessons"]),
			new EagerLoad(["students" => function ($students) use ($studentId) {
				$students->select(
						"student_id", 
						"nisn", 
						"nama_siswa", 
						"file_path", 
						"scores", 
						"el_student_tasks.status"
					)->where('student_id', $studentId);
				/*else 
					$query->select(
						"student_id", 
						"nisn", 
						"nama_siswa", 
						"file_path", 
						"scores", 
						"el_student_tasks.status"
					)->where('student_id', $this->students()->getId());*/
			}])
		])->find($id);
	}

	public function findTaskWithLessonAndStudents(int $id)
	{
		return $this->withCriteria([
			new Teacher,
			new EagerLoad(["lessons"]),
			new EagerLoad(["students" => function ($students) {
				$students->select(
						"student_id", 
						"nisn", 
						"nama_siswa", 
						"file_path", 
						"scores", 
						"el_student_tasks.status"
				);
			}])
		])->find($id);
	}

	public function findByClassRoomAndPararel(string $classroom, string $pararel)
	{
		return $this->findAndWhere('class_room', $classroom, 'pararel', $pararel);
	}

	public function getTaskToday()
	{
		$midnight = \Carbon\Carbon::parse(date('Y-m-d 00:00:00'))->toDateTimeString();
		$night = \Carbon\Carbon::parse(date('Y-m-d 23:59:59'))->toDateTimeString();

		if (management()->loggedIn()) {
			
			if (management()->userIs('admin')) {
				
				return $this->Entity->whereBetween('deadline', [$midnight, $night])->get();
			}
			else if (management()->userIs('guru')) {
				
				$uid = management()->getId();
				$task = $this->Entity->where('user_id', $uid);
				
				return $task->where('deadline', [$midnight, $night])->get();

			}
		}
	}

	public function getTaskTodayByClassRoomAndPararel(string $classroom, string $pararel)
	{
		$midnight = \Carbon\Carbon::parse(date('Y-m-d 00:00:00'))->toDateTimeString();
		$night = \Carbon\Carbon::parse(date('Y-m-d 23:59:59'))->toDateTimeString();

		return $this->Entity->where(
			'class_room', $classroom
		)->where(
			'pararel', $pararel
		)->whereBetween(
			'deadline', [$midnight, $night]
		)->get();
	}

	/**
	 * @return \App\Models\Data\Task 
	 */
	public function getAll()
	{
		$tasks = $this->withCriteria([new Teacher]);
		$management = auth()->guard('web');

		if ($management->check()) {
			
			if (management()->userIs("admin")) {
				
				return $tasks->all();
			}
			else if (management()->userIs("guru")) {

				return $tasks->findWhere('user_id', management()->getId());
			}
		}

		return $tasks->findAndWhere('class_room', student()->getClass(), 'pararel', student()->getPararel());
	}

	public function getWhereStudentHaveNotCollected(int $id, string $classroom, string $pararel)
	{

		return $this->withCriteria([
			new EagerLoad(['students'])
		])->findAndWhere('class_room', $classroom, 'pararel', $pararel);
	}
}