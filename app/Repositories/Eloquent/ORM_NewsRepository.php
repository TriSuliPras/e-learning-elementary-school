<?php 

namespace App\Repositories\Eloquent;

use Illuminate\Support\Facades\File;
use App\Models\Actors\User;
use App\Models\Data\News;
use App\Repositories\BaseRepository;
use App\Repositories\Contracts\ORM_NewsInterface;
use App\Repositories\Eloquent\Criteria\{EagerLoad, Teacher, Select, OrderBy};
use App\Services\Dom\DomDocument;

class ORM_NewsRepository extends BaseRepository implements ORM_NewsInterface
{
	public function entity()
	{
		return News::class;
	}

	public function getRules()
	{
		return $this->Entity::RULE;
	}

	public function setCategory(string $category)
	{
		return $this->Entity->getCategory($category);
	}

	public function getAll()
	{
		if (auth()->guard('web')->check()) {
			
			return $this->withCriteria([
				new Teacher,
				new OrderBy('status')
			])->all();
		}

		return $this->withCriteria([
			new Teacher,
			new EagerLoad(['students' => function ($student) {
				$student->where('student_id', student()->getId())->get();
			}]),
			new EagerLoad(['classnews' => function ($class) {
				$class->where('class_room', student()->getClass())->where('pararel', student()->getPararel());
			}]),
			new OrderBy('status')
		])->findAndWhereWithNotEqual('status', "active", "receiver", "Management");
	}

	public function findWhereReceiver(string $receiver)
	{
		$receiver = ucfirst($receiver);

		return $this->findAndWhere('status', "active", 'receiver', $receiver);
	}

	public function store(array $properties)
	{
		$dom = new DomDocument();
		$description = $properties['description'];
		$properties['description'] = $dom->saveImages($description)->getHtml();
		
		return $this->create($properties);
	}

	public function storeClassCategory(array $createProps)
	{
		$dom = new DomDocument();
		$description = $createProps['description'];
		$classes = $createProps['class_room'];
		$createProps['description'] = $dom->saveImages($description)->getHtml();

		$news = $this->create($createProps);
		for ($i = 0; $i < count($classes); $i++) { 
			
			$attribute = $classes[$i];

			$class_room = explode(' ', $attribute)[0];
			$pararel = explode(' ', $attribute)[1];
			
			$news->classnews()->create([
				'class_room' => $class_room,
				'pararel'	 => $pararel
			]);
		}

		return $news;
	}

	public function storeStudentCategory(array $createProps)
	{
		$dom = new DomDocument();
		$description = $createProps['description'];
		$students = $createProps['student_id'];
		$createProps['description'] = $dom->saveImages($description)->getHtml();

		return $this->create($createProps)->students()->attach($students);
	}

	public function updated($id, array $updateProps)
	{
		// Get news data that should update
		$news = $this->find($id);

		// make new DomDocument object
		// for manipulate if update contains new images
		$dom = new DomDocument();

		// Get the new properties description
		$description = $updateProps['description'];
		
		// if news contains images then DomDocument will extracted
		// and converting into base64_decode and return it as
		// HTML tag include the images
		$updateProps['description'] = $dom->saveImages($description)->getHtml();

		// Get images form news data that should be update
		$images = $dom->getImages($news->description)->remove();
		
		return $this->update($id, $updateProps);
	}

	public function updateNewsCategoryClass($id, array $updateProps)
	{
		// find news that should be update
		$news = $this->find($id);

		// get class rooms that have change 
		// from update properties
		$classrooms = $updateProps['class_room'];

		// and delete news class room by 
		// news id, then we make the new one
		$news->classnews()->delete($id);

		for ($i = 0; $i < count($classrooms); $i++) { 
			
			$class_room = explode(' ', $classrooms[$i])[0];
			$pararel = explode(' ', $classrooms[$i])[1];
			
			$news->classnews()->create([
				'class_room' => $class_room,
				'pararel'	 => $pararel
			]);
		}

		// make new DomDocument object
		// for manipulate if update contains new images
		$dom = new DomDocument();

		// Get the new properties description
		$description = $updateProps['description'];
		
		// if news contains images then DomDocument will extracted
		// and converting into base64_decode and return it as
		// HTML tag include the images
		$updateProps['description'] = $dom->saveImages($description)->getHtml();

		// Get images form news data that should be update
		$images = $dom->getImages($news->description)->remove();

		return $this->update($id, $updateProps);
	}

	public function updateNewsCategoryStudent($id, array $updateProps)
	{
		// make new DomDocument object
		// for manipulate if update contains new images
		$dom = new DomDocument();

		// find news that should be update
		$news = $this->find($id);

		// get class rooms that have change 
		// from update properties
		$students = $updateProps['student_id'];
		

		// Get the new properties description
		$description = $updateProps['description'];
		
		// if news contains images then DomDocument will extracted
		// and converting into base64_decode and return it as
		// HTML tag include the images
		$updateProps['description'] = $dom->saveImages($description)->getHtml();

		// Get images form news data that should be update
		$images = $dom->getImages($news->description)->remove();

		$news->update($updateProps);

		$news->students()->sync($students);
	}

	public function destroy($id)
	{
		$news = $this->find($id);
		$dom = new DomDocument;
		// Get images form news data that should be update
		$images = $dom->getImages($news->description)->remove();

		return $this->delete($id);
	}
}