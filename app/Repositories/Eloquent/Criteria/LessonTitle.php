<?php 

namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\Contracts\Criteria\CriterionInterface;

class LessonTitle implements CriterionInterface
{
	protected $name;

	public function __construct($name)
	{
		$this->name = $name;
	}

	public function apply($entity)
	{
		return $entity->with(["lessons" => function ($query){
						
				$query->where('nama_matapelajaran', $this->name);
			}])
		;
	}
}