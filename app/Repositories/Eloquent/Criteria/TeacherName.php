<?php 

namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\Contracts\Criteria\CriterionInterface;

class TeacherName implements CriterionInterface
{
	protected $name;

	public function __construct($name)
	{
		$this->name = $name;
	}

	public function apply($entity)
	{
		return $entity->with(["teachers" => function ($query) {
				$query->with(["employees" => function ($query) {
					$query->join("pegawai", "user_pegawai.pegawai_id", '=', "pegawai.id")
						  ->where('pegawai.nama_pegawai', $this->name);
				}]);
			}])
		;
	}
}