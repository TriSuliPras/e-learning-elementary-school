<?php 

namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\Contracts\Criteria\CriterionInterface;

class Teacher implements CriterionInterface
{
	public function apply($entity)
	{
		return $entity->with(["teachers" => function ($query) {
				$query->with(["employees" => function ($query) {
					$query->join("pegawai", "user_pegawai.pegawai_id", '=', "pegawai.id");
				}]);
			}])
		;
	}
}