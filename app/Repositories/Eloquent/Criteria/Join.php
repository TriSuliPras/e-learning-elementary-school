<?php 

namespace App\Repositories\Eloquent\Criteria;

use Illuminate\Support\Str;
use App\Repositories\Contracts\Criteria\CriterionInterface;

class Join implements CriterionInterface
{
	/**
	 * Tabel key constraint entity.
	 * 
	 * @var String
	 */
	protected $entityKey;

	/**
	 * Operator join.
	 * 
	 * @var String
	 */
	public $opt;

	/**
	 * Tabel yang akan dijoin.
	 * 
	 * @var String
	 */
	protected $tableConstraint;
	
	/**
	 * Set table yang akan dijoin.
	 * 
	 * @param  String $t_const
	 * @return Void
	 */
	public function __construct(String $t_const, String $entity_key, String $opt = '=')
	{
		$this->tableConstraint = $t_const;
		$this->entityKey = $entity_key;
		$this->opt = $opt;
	}

	/**
	 * Mengimplementasikan kebutuhan query entity.
	 * 
	 * @param   $entity [description]
	 * @return  \Illuminate\Query\Builder
	 */
	public function apply($entity)
	{
		// Tabel entity yang digunakan
		$on_entity = $entity->getModel()->getTable() . '.' . $this->entityKey;
		
		$t_inner = Str::before($this->tableConstraint, '.');

		return $entity->join($t_inner, $on_entity, $this->opt, $this->tableConstraint);
	}
}