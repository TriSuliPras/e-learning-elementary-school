<?php 

namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\Contracts\Criteria\CriterionInterface;

class Select implements CriterionInterface
{
	protected $fields;
	
	public function __construct(array $fields)
	{
		$this->fields = $fields;
	}

	public function apply($entity)
	{
		$fields = implode(",", $this->fields);

		return $entity->select($this->fields);
	}
}