<?php 

namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\Contracts\Criteria\CriterionInterface;

class ByGroup implements CriterionInterface
{
	protected $groupBy;

	public function __construct($groupBy)
	{
		$this->groupBy = $groupBy;
	}

	public function apply($entity)
	{
		return $entity->groupBy($this->groupBy);
	}
}