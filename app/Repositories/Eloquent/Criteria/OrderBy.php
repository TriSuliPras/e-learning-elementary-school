<?php 

namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\Contracts\Criteria\CriterionInterface;

class OrderBy implements CriterionInterface
{
	protected $order;

	protected $iterate;

	public function __construct($order, $iterate = "ASC")
	{
		$this->order = $order;
		$this->iterate = $iterate;
	}

	public function apply($entity)
	{
		return $entity->orderBy($this->order, $this->iterate);
	}
}