<?php 

namespace App\Services\Quiz;

use Illuminate\Support\Facades\Cache;
use App\Services\OfficeDocuments\OfficeExcel;
use App\Services\Quiz\Contratcs\Randomize;
use App\Services\Quiz\Contratcs\Correction;
use App\Repositories\Eloquent\ORM_QuizRepository;

final class QuestionsControl implements Randomize, Correction
{
	/**
	 * @property [#] Array $questions
	 * 
	 */
	protected $questions;

	/**
	 * @property [#] Integet $resultMultipleChoice
	 * 
	 */
	protected $resultMultipleChoice;

	/**
	 * @property [#] Integet $resultEssay
	 * 
	 */
	protected $resultEssay;
	
	public function __construct()
	{
		// inittial questions to empty array
		$this->questions = [];

		// inittial default resultMultipleChoice to zero
		$this->resultMultipleChoice = 0;

		// inittial default resultEssay to zero
		$this->resultEssay = 0;
	}
	
	/**
	 * Get multiple and essay exercises from
	 * questions property and return as Array
	 * 
	 * @return Array
	 */
	public function get() : Array
	{
		// Insert image into image key depend on exercises number
		return [
			'multiple' => $this->setImagesToExercises($this->questions['multiple']),
			'essay' => $this->setImagesToExercises($this->questions['essay']),
		];
	}

	public function getResultMultipleQuestion()
	{
		return $this->resultMultipleChoice;
	}

	public function getResultEssayQuestion()
	{
		return $this->resultEssay;
	}
	
	/**
	 * Get how many exercises in multiple and essay choice
	 * and return it as number that pass to array
	 * 
	 * @return Array 
	 */
	public function count() : Array
	{		
		return [
			'multiple' => count($this->questions['multiple']['exercises']),
			'essay' => count($this->questions['essay']['exercises'])
		];
	}
	
	/**
	 * 
	 * Load the question file from excel using OfficeExcel service
	 * after that, get the sheet of each type of question
	 * to then assign that value to the questions property
	 * 
	 * @param String $pathToFile 
	 * @param pp\Services\OfficeDocuments\OfficeExcel $officeExcel
	 * @return self
	 */
	public function from( string $pathToFile, OfficeExcel $officeExcel ) : self
	{	
		$excel = $officeExcel->load($pathToFile);
		
		// get MultipleChoice sheet
		$multiple = $excel->sheet("MultipleChoice");

		// get Essay sheet
		$essay = $excel->sheet("Essay");
		
		if (null($essay)) {
			
			$this->questions = [
				'multiple' => [
					'images' => $officeExcel->getImages($multiple),
					'exercises' => $this->getMultipleChoiceExercises($multiple->toArray())
				],
				'essay' => [
					'images' => [],
					'exercises' => []
				]
			];
		}
		else if (null($multiple)) {
			
			$this->questions = [
				'multiple' => [
					'images' => [],
					'exercises' => []
				],
				'essay' => [
					'images' => $officeExcel->getImages($essay),
					'exercises' => $this->getMultipleChoiceExercises($essay->toArray())
				],
			];
		}
		else {
			$this->questions = [
				'multiple' => [
					'images' => $officeExcel->getImages($multiple),
					'exercises' => $this->getMultipleChoiceExercises($multiple->toArray())
				],
				'essay' => [
					'images' => $officeExcel->getImages($essay),
					'exercises' => $this->getMultipleChoiceExercises($essay->toArray())
				],
			];
		}
		
		return $this;
	}
	
	/**
	 * make questions to random
	 * @return self
	 */
	public function randomize()
	{
		foreach ($this->questions as $type => $questions) {
			
			if (is_equal($type, "multiple")) {

				if (! empty($questions['exercises'])) {

					$multiple = $questions['exercises'];

					// randomize index of array $multipeChoice
					$indexMtp = range(0, count($multiple)-1); shuffle($indexMtp);

					// reassign value of property questions multiple
					for ($i = 0; $i < count($indexMtp); $i++) {

						$this->questions['multiple']['exercises'][$i] = $multiple[$indexMtp[$i]];
					}
				}
			}
			else if (is_equal($type, "essay")) {
				
				if (! empty($questions['exercises'])) {

					$essay = $questions['exercises'];

					// randomize index of array $essay
					$indexEsy = range(0, count($essay)-1); shuffle($indexEsy);

					// reassign value of property questions essay
					for ($i = 0; $i < count($indexEsy); $i++) {

						$this->questions['essay']['exercises'][$i] = $essay[$indexEsy[$i]];
					}
				}
			}
		}

		return $this;
	}
	
	/**
	 * Correction quiz multiple choice
	 * @param \App\Repositories\Eloquent\ORM_QuizRepository $quiz <Quiz Model>
	 * @param Array $answers
	 * @param $id <Quiz id>
	 * @return \App\Services\Quiz\QuestionsControl
	 */
	public function correctionMultipleQuestions(ORM_QuizRepository $quiz, array $answers, $id)
	{
		$result = [];

		$trueAnswers = 0;

		// Get the master Questions of quiz
		$mtpQuestions = $this->questions['multiple']['exercises'];

		// Get weight of multiple choice questions
		$questionsWeight = (int) $quiz->find($id)->weight_multiple;

		// if the answers is not empty
		if (! empty($answers))  {
			
			// loop the master questions
            foreach ($mtpQuestions as $question)  {
				
				// loop the student answers
                foreach ($answers as $key => $answer) {

                    if (array_has($answers, "exam_".(int) $question['no'])) {

                        if ((int) $question['no'] === (int) explode('_', $key)[1]) {
							
                            $option = explode('_', $answer)[0].strToUpper(explode('_', $answer)[1]);

                            if ($question['key'] === $option) {
								
								// if the answers os correct, then
								// increment trueAnswers variables by 1
								$trueAnswers+= 1;

                                $result[$question['no']] = [
                                    'answer' => $option,
                                    'status' => true,
                                    'score'	 => 100 / count($mtpQuestions)
                                ];
                            } else {
                                $result[$question['no']] = [
                                    'answer' => $option,
                                    'status' => false,
                                    'score'	 => 0
                                ];
                            }
                        } 

                    } else {
                        $result[$question['no']] = [
                            'answer' => "empty",
                            'status' => false,
                            'score'	 => 0
                        ];
                    }
                }   
            }  
        } else {
            foreach ($mtpQuestions as $key => $value) {

                $result[++$key] = "Kosong";
            }
        }
		
		$this->resultMultipleChoice += ($trueAnswers / count($mtpQuestions) ) * $questionsWeight;

		// dd($mtpQuestions, $answers, $questionsWeight, $result, $this->resultMultipleChoice);
        
		/*foreach ($result as $value)
		{
			if ($value !== "Kosong") {
				
				$this->resultMultipleChoice += $value['score'];
			}
		}*/

		return $this;
	}
	
	/**
	 * Correction quiz essay
	 * @param \App\Repositories\Eloquent\ORM_QuizRepository $quiz <Quiz Model>
	 * @param Array $answers
	 * @param $id <quiz id>
	 * @return void 
	 */
	public function correctionEssayQuestions(ORM_QuizRepository $quiz, array $answers, $id)
	{
		$trueAnswers = 0;

		$point = 0;
		
		// Get weight of essay questions
		$questionsWeight = $quiz->find($id)->weight_essay;
		
		// Total Questions
		$questions = $answers['essay_scores'];

		for($i = 1; $i <= count($questions); $i++) {
			
			if (
				not_null($questions["no$i"]) ||
				$questions["no$i"] != 0
			) {
				
				$trueAnswers += 1;
				$point += (int) $questions["no$i"];
			}
		}
		
		if ($point == 0 && $trueAnswers == 0) {
			$this->resultEssay = 0;
		}
		else
			$this->resultEssay += (($point / count($questions)) / 100) * $questionsWeight;

		return $this;
	}
	
	/**
	 * get the question of multiple choice quizzes from excel file
	 * the excel cell is not empty
	 * 
	 * @param Array $exercises
	 * @return Array 
	 */
	protected function getMultipleChoiceExercises(array $exercises) : Array
	{
		$index = 0;
		$multipleExcercises = [];

		foreach ($exercises as $i => $cell) {
			if ($i > 0) {
				if (! is_null($cell[0])) {
					$multipleExcercises[$index] = collect($cell)->filter(function ($item) {
						return ! is_null($item);
					})->values()->toArray();
					
					$index++;
				}
			}
			/*if (not_null($cell['no'])) {
				
				$multipleExcercises[$index] = $cell;
				$index++;
			}	*/
		}

		// dd($multipleExcercises);
		return $multipleExcercises;
	}
	
	/**
	 * obtaining quiz essay from excel file if
	 * the excel cell is not empty
	 * 
	 * @param Array $exercises
	 * @return Array 
	 */
	protected function getEssayExercises(array $exercises) : Array
	{
		$index = 0;
		$essayExcercises = [];

		foreach ($exercises as $i => $cell) {
			
			if (not_null($cell['no'])) {
				
				$essayExcercises[$index] = $cell;
				$index++;
			}	
		}

		return $essayExcercises;
	}
	
	/**
	 * insert the image into the key image of 
	 * each type of question according to the sequence number of the question.
	 * 
	 * @param  Array $questions 
	 * @return Array <array soal beserta gambar>
	 */
	protected function setImagesToExercises(array $questions) : Array
	{
		$exercises = [];
		// $mtpExcercises['exercises'], $mtpExcercises['images']
		// dd($questions['images']);
		
		foreach ($questions['exercises'] as $i => $question) {

			$number = (int) $question[0];
			$key = "exe_{$number}";

			if ( count($question) > 4 ) {
				$exercises[$i] = [
					'no' => $number,
					'exercise' => $question[2],
					'img'	=> array_key_exists($key, $questions['images']) ?  $questions['images'][$key] : null,
					'option' => [
						'A' => $question[3],
						'B' => $question[4],
						'C' => $question[5],
						'D' => $question[6],
					],
					'key' => strtolower(substr($question[1], 6))
				];
			}
			else {
				$exercises[$i] = [
					'no' => $number,
					'exercise' => $question[1],
					'img'	=> array_key_exists($key, $questions['images']) ?  $questions['images'][$key] : null,
					'key' => $question[2]
				];
			}
		}
		
		return $exercises;
	}
}