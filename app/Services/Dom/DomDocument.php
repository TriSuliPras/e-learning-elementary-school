<?php 

namespace App\Services\Dom;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

final class DomDocument
{
	protected $dom;

	protected $html;

	public function __construct()
	{
		$this->dom = new \domdocument();
		$this->html = "";
	}

	public function getImages(string $document)
	{
		$this->setUp($document);

		return $this;
	}

	public function remove()
	{
		$images = $this->dom->getelementsbytagname('img');
		
		/**
		 * if images exists then we loop through them
		 * for checking the old path of the images 
		 * if the file is exist, then deleted
		 */
		if ($images->length > 0) {
			
			foreach ($images as $i => $img) {
				
				$src = $img->getattribute('src');
				
				$path = explode('/', $src);
				$pathToFile = implode('/', array_except($path, [0, 1, 2]));
					
				$imgStillExist = str_contains($this->html, $src);
				
				if (! $imgStillExist) {
					
					if (File::exists($pathToFile)) {
						File::delete($pathToFile);
					}
				}
			}
		}
	}

	public function saveImages(string $document)
	{
		$this->setUp($document);

		$images = $this->dom->getelementsbytagname('img');

		if ($images->length > 0) {

			foreach ($images as $img) {
				
				// $filename = explode('.', $img->getattribute('data-filename'))[0];
				$src = $img->getattribute('src');

				if (preg_match('/data:image/', $src)) {
					
					/*list($type, $src) = explode(';', $src);
					list(, $src)	   = explode(',', $src);*/

					// $src = base64_decode($src);
					// // get the mimetype
					preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
					$mimetype = $groups['mime'];
					$filename  = uniqid();
					$ext = ".png";
					$pathToFile = "/public/management/news/{$filename}{$ext}";

					if ( !File::exists( dirname( storage_path('app/public/management/news') ) ) )
						File::makeDirectory(
							dirname(storage_path("/app/{$pathToFile}")),
							0755,
							true
						);
						
					Image::make($src)
						 ->encode($mimetype, 100)
						 ->save(storage_path("/app/{$pathToFile}"));
					
					$newSrc = str_replace("public", "storage", $pathToFile);
					$img->removeattribute('src');
					$img->setattribute('src', asset($newSrc));
				}
			}
		}
		
		$this->html = $this->dom->savehtml();

		return $this;
	}

	public function getHtml()
	{
		return $this->html;
	}

	private function setUp(string $document)
	{
		libxml_use_internal_errors(true);

		return $this->dom->loadHtml($document, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
	}
}