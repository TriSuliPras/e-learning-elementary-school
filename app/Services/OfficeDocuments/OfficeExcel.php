<?php 

namespace App\Services\OfficeDocuments;

use Illuminate\Support\Collection;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;
use Maatwebsite\Excel\Facades\Excel;

final class OfficeExcel
{
	protected $excel;

	protected $drafter;

	protected $sheet;

	protected $extension;

	protected $filename;

	public function __construct( Excel $excel, Drawing $drafter, Spreadsheet $sheet )
	{
		$this->excel = $excel;
		$this->drafter = $drafter;
		$this->sheet = $sheet;
	}

	public function load( string $pathToFile )
	{
		return $this->excel::load($pathToFile);
	}

	public function create(string $filename, array $datas, string $pathToFile)
	{
		return $this->excel::create($filename, function($excel) use ($datas) {
			
			$excel->sheet("MultipleChoice", function($sheet) use ($datas) {
				$sheet->cell('A1', function($cell) {
					$cell->setBackground("#18a566");
					$cell->setFontColor("#ffffff");
					$cell->setValue('Nomor');
				});

				$sheet->cell('B1', function($cell) {
					$cell->setBackground("#18a566");
					$cell->setFontColor("#ffffff");
					$cell->setValue('Answers');
				});

				$sheet->setWidth([
					'A' => 10,
					'B' => 20
				]);
				
				$key = 0;
				$row = 1;
				foreach ($datas['multiple'] as $answer) {

					if (is_null($answer)) {

						$sheet->row(++$row, array(array_keys($datas['multiple'])[$key], "Tidak Diisi"));
					} 
					else {
						
						$sheet->row(++$row, array(array_keys($datas['multiple'])[$key], $answer));		
					}

					$key++;
				}
			});

			$excel->sheet("Essay", function($sheet)  use ($datas){
				
				$sheet->cell('A1', function($cell) {
					$cell->setBackground("#18a566");
					$cell->setFontColor("#ffffff");
					$cell->setValue('Nomor');
				});

				$sheet->cell('B1', function($cell) {
					$cell->setBackground("#18a566");
					$cell->setFontColor("#ffffff");
					$cell->setValue('Answers');
				});

				$sheet->setWidth([
					'A' => 10,
					'B' => 100
				]);
				
				$key = 0;
				$row = 1;
				foreach ($datas['essay'] as $answer) {

					if (is_null($answer)) {

						$sheet->row(++$row, array(array_keys($datas['essay'])[$key], "Tidak Diisi"));
					} 
					else {
						
						$sheet->row(++$row, array(array_keys($datas['essay'])[$key], $answer));		
					}

					$key++;
				}
			});
			
		})->store('xlsx', $pathToFile);
	}

	public function getImages(object $sheet)
	{
		$img = [];
		
		foreach ($sheet->getDrawingCollection() as $drafter) {

			if ($drafter instanceof MemoryDrawing) {
				
				// Activated output buffer
				ob_start();
				call_user_func(
		            $drafter->getRenderingFunction(),
		            $drafter->getImageResource()
		        );
		        $images = ob_get_contents();
		        // DeActivated output buffer And Clean
		        ob_end_clean();

		        switch ($drafter->getMimeType()) {
		            case MemoryDrawing::MIMETYPE_PNG :
		                $extension = 'png';
		                break;
		            case MemoryDrawing::MIMETYPE_GIF:
		                $extension = 'gif';
		                break;
		            case MemoryDrawing::MIMETYPE_JPEG :
		                $extension = 'jpg';
		                break;
		        }
			}
			else {
				
				preg_match('!\d+!', $drafter->getCoordinates(), $matches);
				$key = (int) $matches[0];

				$zipReader = fopen($drafter->getPath(),'r');
		        $images = '';
				
		        while (!feof($zipReader)) {
		            $images .= fread($zipReader,1024);
		            
		        }

		        fclose($zipReader);
		        $extension = $drafter->getExtension();
			}

			$iter = (int) $key - 1;
			$myFileName = uniqid().'.'.$extension;
			$img["exe_{$iter}"] = base64_encode($images);
		}
		
		return $img;
	}
}