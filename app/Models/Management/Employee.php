<?php

namespace App\Models\Management;

use Illuminate\Database\Eloquent\Model;
use App\Models\Data\ClassRoom;

class Employee extends Model 
{
    protected $table = "pegawai";

    protected $guarded = [];

    public function user_teacher()
    {
    	return $this->hasOne(UserTeacher::class, "pegawai_id");
    }

    public function homeroom()
    {
    	return $this->hasOne(ClassRoom::class, "pegawai_id");
    }
}
