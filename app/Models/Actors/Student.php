<?php

namespace App\Models\Actors;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Data\{
    Administration,
    Quiz,
    Task,
    News
};

class Student extends Authenticatable
{
    use Notifiable;

    protected $table = "siswas";

    const RULE = [
        'foto_siswa' => "image|mimes:jpg,jpeg,png"
    ];

    protected $fillable = [
        'nisn',
        'nis',
        'email',
        'password',
        'nik',
        'nama_siswa',
        'kelas',
        'foto_siswa',
        'jenis_tinggal',
        'surabaya_alamat',
        'surabaya_rt',
        'surabaya_rw',
        'surabaya_kel_desa',
        'surabaya_kecamatan',
        'surabaya_kab_kota',
        'surabaya_kodepos',
        'luar_alamat',
        'luar_rt',
        'luar_rw',
        'luar_kel_desa',
        'luar_kecamatan',
        'luar_kab_kota',
        'luar_kodepos',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'agama',
        'tinggi_badan',
        'berat_badan',
        'anak_ke',
        'berkebutuhan_khusus',
        'no_tlp',
        'no_hp',
        'ijazahterakhir_asal',
        'ijazahterakhir_no',
        'ijazahterakhir_tahun',
        'program_pengajaran',
        'npsn',
        'nama_ayah',
        'tahunlahir_ayah',
        'pekerjaan_ayah',
        'pendidikan_ayah',
        'penghasilan_ayah',
        'nama_ibu',
        'tahunlahir_ibu',
        'pekerjaan_ibu',
        'pendidikan_ibu',
        'penghasilan_ibu',
        'nama_wali',
        'tahunlahir_wali',
        'pekerjaan_wali',
        'pendidikan_wali',
        'penghasilan_wali'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function administrations()
    {
        return $this->hasMany(Administration::class, "siswa_id");
    }

    public function quizs()
    {
        return $this->belongsToMany(Quiz::class, "el_student_quizs", "student_id", "quiz_id");
    }

    public function tasks()
    {
        return $this->belongsToMany(Task::class, "el_student_tasks", "student_id", "task_id");
    }

    public function news()
    {
        return $this->belongsToMany(News::class, "el_student_news", "student_id", "news_id");
    }
}
