<?php 

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;

class ClassNews extends Model
{
	protected $table = "el_class_news";

	protected $fillable = ['class_room', 'pararel'];

	public $timestamps = false;


    public function news()
    {
        return $this->belongsTo(News::class, "news_id");
    }
}