<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;
use App\Models\Actors\{
    User,
    Student
};

class Quiz extends Model 
{   
    /**
     * Define database table name
     *
     * @property [#] $table
     */
    protected $table = "el_quizs";
    
    /**
     * Define database table field
     * Should be fillable
     *
     * @property [#] $fillable
     */
    protected $fillable = [
        "per_year_id",
        "semester_id",
		"user_id",
		"lesson_id",
        "class_room",
        "pararel",
		"title",
		"description",
        "starting_time",
		"status",
        "count_down",
        'weight_multiple',
        'weight_essay',
        "file_name",
		"file_path",
        "created_at",
        "updated_at"
    ];
    
    
    public function lessons()
    {
    	return $this->belongsTo(Lesson::class, "lesson_id", 'id');
    }

    public function teachers()
    {
        return $this->belongsTo(User::class, "user_id", "id");
    }

    public function students()
    {
        return $this->belongsToMany(Student::class, "el_student_quizs", "quiz_id", "student_id");
    }
}
