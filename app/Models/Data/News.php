<?php

namespace App\Models\Data;

use App\Models\Actors\User;
use App\Models\Actors\Student;
use App\Models\Data\ClassNews;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = "el_news";

    CONST RULE = [
        'title'         => "required|max:75",
        'receiver'      => "required",
        'description'   => "required",
    ];

    CONST COMMON = "Common";
    CONST MANAGEMENT = "Management";
    CONST CLASSES = "Class";
    CONST STUDENT = "Student";

    protected $fillable = [
        "user_id",
    	"title",
		"receiver",
        "status",
		"description",
		"created_at", 
		"updated_at"
    ];

    public function teachers()
    {
    	return $this->belongsTo(User::class, "user_id", 'id');
    }

    public function students()
    {
        return $this->belongsToMany(Student::class, "el_student_news", "news_id", "student_id");
    }

    public function classnews()
    {
        return $this->hasMany(ClassNews::class, "news_id");
    }

    public function getCategory(string $category)
    {
        switch ($category) {
            case "umum":
                return News::COMMON;

            case "manajemen":
                return News::MANAGEMENT;

            case "kelas":
                return News::CLASSES;

            case "siswa":
                return News::STUDENT;
            
            default:
                return $category;
        }
    }
}
