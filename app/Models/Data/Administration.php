<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;
use App\Models\Actors\Student;

class Administration extends Model
{
    protected $table = "spp";

    public function paymentStudent()
    {
    	return $this->belongsTo(Student::class, "siswa_id");
    }
}
