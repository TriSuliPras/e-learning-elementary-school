<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;
use App\Models\Actors\User;

class Material extends Model
{   
    protected $table = "el_materials";

    const RULE = [
        'title'       => "required|max:75",
        'class_room'  => "required",
        'lesson_id'   => "required",
        'file_path'   => "required|file|mimes:doc,docx,ppt,pptx,pdf"
    ];

    protected $fillable = [
        'per_year_id',
        'semester_id',
        "user_id",
		"lesson_id",
        "class_room",
        "pararel",
		"title",
		"description",
        "file_name",
		"file_path",
        "created_at",
        "updated_at"
    ];

    public function teachers()
    {
        return $this->belongsTo(User::class, "user_id", 'id');
    }

    public function lessons()
    {
    	return $this->belongsTo(Lesson::class, "lesson_id", 'id');
    }
}
