<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;
use App\Models\Actors\{
    User,
    Student
};

class Task extends Model
{
    protected $table = "el_tasks";

    /**
     * Define constant rule
     *
     * @static RULE
     */
    CONST RULE = [
        'title'           => "required|max:75",
        'description'     => "nullable",
        'lesson_id'       => "required",
        'class_room'      => "required",
        'datetime'        => "required",
        'file_path'       => "required_without:description|file|mimes:pdf,doc,docx,ppt,pptx"
    ];

    protected $fillable = [
        "per_year_id",
        "semester_id",
		"user_id",
		"lesson_id",
        "class_room",
        "pararel",
		"title",
		"description",
		"deadline",
        "file_name",
        "file_path",
        "status",
		"created_at",
		"updated_at"
    ];

    public function teachers()
    {
    	return $this->belongsTo(User::class, "user_id", "id");
    }

    public function lessons()
    {
    	return $this->belongsTo(Lesson::class, "lesson_id", "id");
    }

    public function students()
    {
        return $this->belongsToMany(Student::class, "el_student_tasks", "task_id", "student_id");
    }
}
