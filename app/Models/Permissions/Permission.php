<?php

namespace App\Models\Permissions;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $guard = [];

    protected $table = "permissions";

    public function roles()
    {
    	return $this->belongsToMany(Role::class, 'roles_permissions');
    }
}
