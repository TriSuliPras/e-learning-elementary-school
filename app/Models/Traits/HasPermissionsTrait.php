<?php

namespace App\Models\Traits;

use App\Models\Permissions\{Role, Permission};

trait HasPermissionsTrait
{
	public function givePermissionTo(...$permissions)
	{
		$permissions = $this->getAllPermissions(array_flatten($permissions));
		
		if ($permissions === null) {
			return $this;
		}

		$this->permissions()->saveMany($permissions);

		return $this;
	}

	public function withdrawPermissionTo(...$permissions)
	{
		$permissions = $this->getAllPermissions(array_flatten($permissions));

		$this->permissions()->detach($permissions);

		return $this;
	}

	public function updatePermissions(...$permissions)
	{
		$this->permissions()->detach();

		return $this->givePermissionTo($permissions);
	}

	public function hasRole(...$roles)
	{
		foreach ($roles as $role) {
			if ($this->roles->contains('name', $role)){
				return true;
			}
		}
		return false;
	}

	public function hasPermissionTo($permission)
	{
		return $this->hasPermissionThoughRole($permission) || $this->hasPermission($permission);
	}

	public function hasPermissionThoughRole($permission)
	{
		foreach ($permission->roles as $role) {
			if ($this->roles->contains($role)) {
				return true;
			}
		}
		return false;
	}


	public function hasPermission($permission)
	{
		return (bool) $this->permissions->where('name', $permission->name)->count();
	}

	protected function getAllPermissions(array $permissions)
	{
		return Permission::whereIn('name', $permissions)->get();
	}

	public function roles()
	{
		return $this->belongsToMany(Role::class, 'users_roles');
	}

	public function permissions()
	{
		return $this->belongsToMany(Permission::class, 'users_permissions');
	}

}