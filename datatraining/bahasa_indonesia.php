<?php 

/////////////////////////////////////////////////////////
// Contoh format soal mata pelajaran Bahasa Indonesia  //
/////////////////////////////////////////////////////////

return [
	'multiple_choice' => [
		[
			'question' 	 => 'Rani bertemu bu guru siang hari Rani mengucapkan ...',
			'option'	 => [
				'a' => 'Selamat pagi',
				'b' => 'Selamat siang',
				'c' => 'Selamat sore',
				'd' => 'Selamat malam'
			],
			'answer_key' => 'b'
		],
		[
			'question' 	 => 'Saat berjumpa teman pada sore hari maka kita mengucapkan ...',
			'option'	 => [
				'a' => 'Selamat pagi',
				'b' => 'Selamat siang',
				'c' => 'Selamat sore',
				'd' => 'Selamat malam'
			],
			'answer_key' => 'c'
		],
		[
			'question' 	 => 'Saat berpisah dengan teman kita mengucapkan ...',
			'option'	 => [
				'a' => 'Sampai jumpa',
				'b' => 'Selamat malam',
				'c' => 'terima kasih',
				'd' => 'Asshiap'
			],
			'answer_key' => 'a'
		],
		[
			'question' 	 => 'Ucapan selamat malam diucapkan saat ...',
			'option'	 => [
				'a' => 'Malam hari',
				'b' => 'Sore hari',
				'c' => 'Pagi hari',
				'd' => 'Siang hari'
			],
			'answer_key' => 'a'
		],
		[
			'question' 	 => 'Jika bertemu teman harus ...',
			'option'	 => [
				'a' => 'Dijauhi',
				'b' => 'Disapa',
				'c' => 'Dimarahi',
				'd' => 'Dipukul'
			],
			'answer_key' => 'b'
		],
		[
			'question' 	 => 'Ibu menaruh bunga di dalam ...',
			'option'	 => [
				'a' => 'Piring',
				'b' => 'Pot',
				'c' => 'Mangkok',
				'd' => 'Penjara'
			],
			'answer_key' => 'b'
		],
		[
			'question' 	 => 'Ibu mengambil air menggunakan ...',
			'option'	 => [
				'a' => 'Ember',
				'b' => 'Gunting',
				'c' => 'Gayung',
				'd' => 'Buku'
			],
			'answer_key' => 'c'
		],
		[
			'question' 	 => 'Rumahku bersih karena sering ...',
			'option'	 => [
				'a' => 'Dikotori',
				'b' => 'Disapu',
				'c' => 'Disiram',
				'd' => 'Dicuci'
			],
			'answer_key' => 'b'
		],
		[
			'question' 	 => 'Suara dari ayam adalah ...',
			'option'	 => [
				'a' => 'Wek-wek',
				'b' => 'Petok-petok',
				'c' => 'gug-gug',
				'd' => 'Wik-wik'
			],
			'answer_key' => 'b'
		],
		[
			'question' 	 => 'Membuang sampah harus di ...',
			'option'	 => [
				'a' => 'Jalan',
				'b' => 'Sungai',
				'c' => 'Tempat Sampah',
				'd' => 'Mana saja'
			],
			'answer_key' => 'c'
		],
	],
	'essay'		=> [
		[
			'question'	=> 'Jelaskan cara menggunakan telepon untuk menelepon seseorang!',
			'answer_key'	=> 'Angkat gagang telepon, Masukkan nomor telepon yang dituju, Tunggu hingga ada jawaban dari nomor yang dituju, Ucapkan salam, Sampaikan pesan yang ingin dibicarakan, Ucapkan salam penutup, Taruh gagang telepon kembali ke tempatnya'
		],
		[
			'question'	=> 'Sebutkan bagian-bagian dalam surat pribadi!',
			'answer_key'	=> 'Tempat dan tanggal penulisan surat, Alamat penerima surat, Salam pembuka, Kalimat pembuka, Isi surat, Kalimat penutup, Salam penutup, Nama dan tanda tangan pengirim surat'
		],
		[
			'question'	=> "Carilah kata baku dari kata-kata berikut ini : \na.Atlit\nb.Kwintal",
			'answer_key'	=> 'Atlet, Kuintal'
		],
		[
			'question'	=> "Buatlah pemenggalan kata dari kata-kata di bawah ini dengan tepat : \na.Transportasi\nb.Kendaraan\nc.Perkebunan",
			'answer_key'	=> 'Trans – por – ta – si, Ken – da – ra – an, Per – ke – bun – an'
		],
		[
			'question'	=> [
				'a' => 'Apakah pokok pikiran dari paragraf di atas?',
				'b' => 'Berapakah jumlah kalimat dalam paragraf di atas?',
				'c' => 'Dilihat dari letak kalimat utamanya, termasuk jenis paragraf apakah bacaan di atas?',
				'd' => 'Siapakah yang bisa mengikuti lomba Mata Pelajaran yang di adakah oleh SD Brilian Jaya?'
			],
			'answer_key'	=> 'SD Brilian Jaya akan mengadakan Lomba Mata Pelajaran, 9 kalimat, Paragraf deduktif, Siswa kelas 4 sampai kelas 6 SD',
		],
	]
];