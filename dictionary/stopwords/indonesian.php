<?php

return [
	2 => [
		'di', 'ia', 'ke', 'se',
	], 
	3 => [
		'ada', 'apa', 'aku', 'dan', 'dia', 'dua', 'hal', 'ibu', 'ini', 'itu', 'kan', 'kok', 'mau', 'nah', 'pak', 'per', 'pun', 'lah', 'tak', 
		'toh', 'wah',
	],
	4 => [
		'agak', 'agar', 'akan', 'atas', 'atau', 'awal', 'asal', 'anda', 'amat', 'bagi', 'baik', 'baru', 'beri', 'bila', 'bisa', 'buat', 'bung', 
		'cara', 'cuma', 'demi', 'dini', 'diri', 'dong', 'dulu', 'dari', 'guna', 'hari', 'ikut', 'jika', 'juga', 'jauh', 'jadi', 'kala', 'kami', 
		'kamu', 'kata', 'kita', 'lagi', 'lain', 'lalu', 'lama', 'lima', 'luar', 'maka', 'mana', 'masa', 'kini', 'kira', 'mula', 'naik', 'oleh', 
		'pada', 'para', 'pula', 'rasa', 'rata', 'saat', 'saja', 'sama', 'sana', 'satu', 'saya', 'sela', 'siap', 'sini', 'soal', 'tadi', 'tahu', 
		'tapi', 'tiap', 'tiba', 'tiga', 'ucap', 'ujar', 'umum', 'usah', 'usai', 'wong', 'yang', 
	],
	5 => [
		'akhir', 'apaan', 'antar', 'bahwa', 'bagai', 'bakal', 'balik', 'bapak', 'bawah', 'belum', 'benar', 'cukup', 'besar', 'betul', 'biasa', 
		'boleh', 'bukan', 'bulan', 'dekat', 'depan', 'dalam', 'dapat', 'empat', 'entah', 'hanya', 'ingat', 'ingin', 'harus', 'kasus', 'kecil', 
		'kedua', 'ialah', 'jawab', 'jelas', 'kalau', 'kapan', 'lebih', 'lewat', 'macam', 'makin', 'malah', 'mampu', 'masih', 'meski', 'minta', 
		'mirip', 'misal', 'mulai', 'namun', 'nanti', 'pasti', 'perlu', 'pihak', 'pukul', 'punya', 'sebab', 'sebut', 'sejak', 'semua', 'serta', 
		'siapa', 'suatu', 'sudah', 'tahun', 'tanpa', 'tanya', 'tegas', 'telah', 'tentu', 'tepat', 'terus', 'tetap', 'tidak', 'turut', 'tutur', 
		'untuk', 'waduh', 'wahai', 'waktu', 'walau', 'yaitu', 'yakin', 'yakni',
	],
	6 => [
		'adanya', 'adalah', 'adapun', 'akulah', 'akhiri', 'apatah', 'apakah', 'antara', 'bagian', 'bahkan', 'banyak', 'begini', 'begitu', 
		'berada', 'berapa', 'berupa', 'dahulu', 'disini', 'datang', 'dengan', 'dialah', 'diberi', 'dibuat', 'dikira', 'enggak', 'hampir', 
		'hendak', 'hingga', 'ibarat', 'inikah', 'inilah', 'itukah', 'itulah', 'jangan', 'jumlah', 'justru', 'kalian', 'karena', 'kelima', 
		'keluar', 'kenapa', 'kepada', 'ketika', 'kurang', 'lagian', 'lanjut', 'masing', 'maupun', 'memang', 'menuju', 'merasa', 'mereka', 
		'nyaris', 'paling', 'pantas', 'pernah', 'saling', 'sambil', 'sampai', 'sangat', 'sebaik', 'sebuah', 'secara', 'sedang', 'segala', 
		'segera', 'sejauh', 'sekali', 'selain', 'selaku', 'selalu', 'selama', 'semasa', 'semata', 'sempat', 'semula', 'seolah', 'sering', 
		'serupa', 'sesaat', 'sesama', 'setiap', 'setiba', 'seusai', 'supaya', 'tambah', 'tampak', 'tandas', 'tempat', 'tengah', 'terasa', 
		'tetapi', 'tinggi', 'ungkap', 'tunjuk',
	],
	7 => [
		'agaknya', 'akankah', 'artinya', 'asalkan', 'apabila', 'andalah', 'amatlah', 'awalnya', 'apalagi', 'ataukah', 'ataupun', 'bermula', 
		'bersama', 'bersiap', 'berujar', 'bilakah', 'bisakah', 'caranya', 'bakalan', 'bekerja', 'berarti', 'berawal', 'berikan', 'berikut', 
		'berkata', 'berlalu', 'ditanya', 'didapat', 'diingat', 'dilalui', 'dilihat', 'diminta', 'dimulai', 'dirinya', 'disebut', 'dijawab', 
		'jadilah', 'jadinya', 'jawaban', 'jikalau', 'kamilah', 'kamulah', 'katakan', 'katanya', 'keadaan', 'gunakan', 'kembali', 'kinilah', 
		'kiranya', 'kitalah', 'lainnya', 'lamanya', 'makanya', 'malahan', 'masalah', 'melalui', 'melihat', 'memberi', 'membuat', 'memihak', 
		'meminta', 'memulai', 'menaiki', 'menanti', 'menanya', 'mengapa', 'mengira', 'menjadi', 'olehnya', 'padahal', 'padanya', 'panjang', 
		'menurut', 'mulanya', 'mungkin', 'penting', 'percuma', 'rasanya', 'rupanya', 'saatnya', 'sajalah', 'pertama', 'sayalah', 'sebagai', 
		'sebelum', 'sebesar', 'sedikit', 'seingat', 'sejenak', 'sekadar', 'sekecil', 'sekitar', 'seluruh', 'semacam', 'semakin', 'semampu', 
		'semasih', 'semisal', 'sendiri', 'seorang', 'seperti', 'sepihak', 'sesuatu', 'sesudah', 'setelah', 'sewaktu', 'sinilah', 'soalnya', 
		'tadinya', 'tentang', 'terdiri', 'terjadi', 'terkira', 'terlalu', 'tertuju', 'ucapnya', 'ujarnya', 'umumnya',
	],
	8 => [
		'akhirnya', 'berbagai', 'bermacam', 'betulkah', 'biasanya', 'bagaikan', 'beberapa', 'beginian', 'belakang', 'belumlah', 'cukupkah', 
		'cukuplah', 'benarkah', 'benarlah', 'berakhir', 'bertanya', 'berturut', 'bertutur', 'bolehkah', 'bolehlah', 'bukankah', 'bukanlah', 
		'bukannya', 'daripada', 'demikian', 'diakhiri', 'diantara', 'dimaksud', 'dimintai', 'dipunyai', 'ditunjuk', 'ditanyai', 'entahlah', 
		'hanyalah', 'haruslah', 'harusnya', 'inginkah', 'inginkan', 'jawabnya', 'jelaskan', 'jelaslah', 'jelasnya', 'kalaulah', 'kalaupun', 
		'kapankah', 'kapanpun', 'keduanya', 'kelamaan', 'kemudian', 'masihkah', 'mampukah', 'manakala', 'manalagi', 'menanyai', 'mendapat', 
		'mengenai', 'menjawab', 'menunjuk', 'meskipun', 'meyakini', 'misalkan', 'misalnya', 'mulailah', 'nantinya', 'nyatanya', 'pastilah', 
		'perlukah', 'perlunya', 'pihaknya', 'sebabnya', 'sebagian', 'sebanyak', 'sebegini', 'sebegitu', 'seberapa', 'sebutlah', 'sebutnya', 
		'sehingga', 'sejumlah', 'sekalian', 'sekarang', 'seketika', 'semaunya', 'semuanya', 'sesampai', 'sesegera', 'sesekali', 'setempat', 
		'setengah', 'setinggi', 'siapakah', 'siapapun', 'sudahkah', 'sudahlah', 'tanyakan', 'tanyanya', 'tegasnya', 'tentulah', 'tentunya', 
		'terakhir', 'terdapat', 'terhadap', 'teringat', 'terlebih', 'terlihat', 'termasuk', 'ternyata', 'tersebut', 'tertentu', 'terutama', 
		'tidakkah', 'tuturnya', 'waktunya', 'walaupun',
	],
	9 => [
		'antaranya', 'bagaimana', 'beginikah', 'beginilah', 'begitukah', 'begitulah', 'begitupun', 'berapakah', 'berapalah', 'berapapun', 
		'berjumlah', 'berkenaan', 'berlainan', 'bermaksud', 'ibaratkan', 'ibaratnya', 'diberikan', 'dibuatnya', 'digunakan', 'dikatakan', 
		'diketahui', 'dilakukan', 'diperbuat', 'disinilah', 'ditujukan', 'ditunjuki', 'diucapkan', 'enggaknya', 'jangankan', 'janganlah', 
		'jumlahnya', 'karenanya', 'hendaklah', 'hendaknya', 'kebetulan', 'keinginan', 'kelihatan', 'kepadanya', 'khususnya', 'kira-kira', 
		'lanjutnya', 'persoalan', 'melainkan', 'melakukan', 'mempunyai', 'mendatang', 'mengingat', 'menunjuki', 'merekalah', 'merupakan', 
		'sama-sama', 'sampaikan', 'sangatlah', 'sebaiknya', 'sebisanya', 'sedangkan', 'seenaknya', 'segalanya', 'sekalipun', 'sekiranya', 
		'selamanya', 'sekaligus', 'sementara', 'sendirian', 'sepanjang', 'seringnya', 'seseorang', 'setibanya', 'tambahnya', 'tampaknya', 
		'tandasnya', 'terbanyak', 'terdahulu', 'tiba-tiba', 'ungkapnya',
	],

	10 => [
		'bahwasanya', 'belakangan', 'diingatkan', 'diinginkan', 'dijelaskan', 'dikerjakan', 'dimisalkan', 'dimulailah', 'dimulainya', 
		'disebutkan', 'dipastikan', 'diperlukan', 'ditanyakan', 'ditegaskan', 'dituturkan', 'berikutnya', 'berlebihan', 'katakanlah', 
		'kesampaian', 'masalahnya', 'melihatnya', 'memastikan', 'memberikan', 'memerlukan', 'memintakan', 'memisalkan', 'memperbuat', 
		'menantikan', 'menanyakan', 'mendatangi', 'menuturkan', 'menegaskan', 'mengakhiri', 'mengatakan', 'mengetahui', 'menyangkut', 
		'menyatakan', 'menyeluruh', 'menyiapkan', 'meyakinkan', 'mungkinkah', 'pentingnya', 'pertanyaan', 'sebagainya', 'sebaliknya', 
		'sebelumnya', 'sebenarnya', 'sebetulnya', 'secukupnya', 'sedemikian', 'sedikitnya', 'seharusnya', 'sekadarnya', 'sekitarnya', 
		'seluruhnya', 'semampunya', 'semisalnya', 'sendirinya', 'seperlunya', 'sepertinya', 'sesuatunya', 'sesudahnya', 'seterusnya', 
		'setidaknya', 'terjadilah', 'terjadinya',
	],

	11 => [
		'berakhirlah', 'berdatangan', 'berkehendak', 'demikianlah', 'diakhirinya', 'diantaranya', 'berakhirnya', 'didatangkan', 'diibaratkan', 
		'dikarenakan', 'disampaikan', 'diungkapkan', 'ditambahkan', 'ditandaskan', 'ditunjukkan', 'ditunjuknya', 'dimaksudkan', 'dimaksudnya', 
		'berlangsung', 'ingat-ingat', 'kemungkinan', 'keseluruhan', 'keterlaluan', 'menambahkan', 'menandaskan', 'mendapatkan', 'mengerjakan', 
		'menggunakan', 'menghendaki', 'mengucapkan', 'menjelaskan', 'menunjukkan', 'menunjuknya', 'menyebutkan', 'sebagaimana', 'pertanyakan', 
		'sekali-kali', 'sekurangnya', 'selanjutnya', 'semata-mata', 'seolah-olah', 'sepantasnya', 'terhadapnya', 'tersebutlah',
	],

	12 => [
		'bagaimanapun', 'bagaimanakah', 'berkali-kali', 'berkeinginan', 'bersama-sama', 'bersiap-siap', 'diberikannya', 'dikatakannya', 
		'diketahuinya', 'dimungkinkan', 'diperbuatnya', 'dipergunakan', 'diperkirakan', 'dipersoalkan', 'diucapkannya', 'kelihatannya', 
		'memungkinkan', 'mendatangkan', 'mengingatkan', 'menginginkan', 'menyampaikan', 'pertama-tama', 'tersampaikan', 
	],
	
	13 => [
		'dijelaskannya', 'diperlihatkan', 'diperlukannya', 'dipertanyakan', 'disebutkannya', 'dituturkannya', 'masing-masing', 'mempergunakan', 
		'memperkirakan', 'mempersiapkan', 'mempersoalkan', 'mengatakannya', 'mengibaratkan', 'mengungkapkan', 'menanti-nanti', 'sampai-sampai',
	],

	14 => [
		'bermacam-macam', 'bertanya-tanya', 'berturut-turut', 'diibaratkannya', 'dimaksudkannya', 'ditunjukkannya', 'kemungkinannya',
		'keseluruhannya', 'memperlihatkan', 'mempertanyakan', 'mengucapkannya', 'sebaik-baiknya', 'sepantasnyalah', 'selama-lamanya', 
		'teringat-ingat',
	],

	15 => [
		'mengibaratkannya', 'setidak-tidaknya',
	],

	16 => [
		'sekurang-kurangnya',	
	],
];