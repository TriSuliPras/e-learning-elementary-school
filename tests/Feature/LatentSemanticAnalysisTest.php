<?php

namespace Tests\Feature;

use App\Libraries\Semantic\Document;
use App\Libraries\Semantic\LatentSemanticAnalysis;
use Tests\TestCase;

class LatentSemanticAnalysisTest extends TestCase
{
	/**
	 * @test
	 * @return void
	 */
    public function analize_document_using_lsa()
    {
        // '10 kurang 5'
        // '10 tambah 5'
        // 
        // 
        
    	$lsa = new LatentSemanticAnalysis(
            new Document('training', 'Saya masih punya hubungan darah dengan keluarga Bu Rani.'),
    		new Document('testing', 'Tubuhnya berlumuran darah setelah kepalanya terbentur tiang listrik.')
    	);

    	$lsa = $lsa->analyze();

    	$this->assertIsFloat($lsa->getResult());
    }
}
