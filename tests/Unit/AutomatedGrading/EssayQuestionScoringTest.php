<?php

namespace Tests\Unit\AutomatedGrading;

use Tests\TestCase;
use App\Libraries\AutomatedGrading\Corrector\EssayAnswerCorrector;

class EssayQuestionScoringTest extends TestCase
{
	/**
	 * @test
	 * @return void
	 */
    public function correction_bahasa_indonesia()
    {
        // load soal.
        $question = require __DIR__ . '/../../../datatraining/bahasa_indonesia.php';

        $corrector = new EssayAnswerCorrector($question['essay'], 100);
        $corrector->setAnswers([
            'angkat gagang telepon, masukan nomor, tunggu ada jawaban, salam, menyampaikan pesan, salam penutup',
            'tempat dan tanggal surat, alamat penerima, salam pembuka',
            'Atlet, Kwintal',
            'trans - por - ta - si',
            'ada lomba'
        ]);

        $corrector->correction();

        $this->assertGreaterThan(50.0, $corrector->getScore());
    }
}
