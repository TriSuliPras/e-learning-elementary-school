<?php

namespace Tests\Unit\AutomatedGrading;

use Tests\TestCase;
use App\Libraries\AutomatedGrading\Corrector\MultipleChoiceAnswerCorrector;

class MultipleChoiceQuestionScoringTest extends TestCase
{
    /**
     * Korektor soal pilihan ganda.
     * 
     * @var MultipleChoiceAnswerCorrector
     */
    protected $corrector;

    /**
     * Menyiapkan testing environtment.
     * 
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // load soal.
        $question = require __DIR__ . '/../../../datatraining/bahasa_indonesia.php';

        $this->corrector = new MultipleChoiceAnswerCorrector($question['multiple_choice'], 100);
    }

    /**
     * Testing: Soal ujian sudah tersedia.
     *
     * @test
     * @return void
     */
    public function make_sure_questions_isset() : Void
    {
        $totalQuestion = $this->corrector->count();

        $this->assertGreaterThan(0, $totalQuestion);
        $this->assertTrue(is_array($this->corrector->getQuestions()));
    }

    /**
     * Testing: Pilihan jawaban soal sudah tersedia.
     *
     * @test
     * @return void
     */
    public function check_that_multiple_choice_is_always_has_options() : Void
    {
        $options = $this->corrector->getAnswersChoices();

        $this->assertClassHasAttribute('answerChoice', MultipleChoiceAnswerCorrector::class);
        $this->assertTrue( is_array($options) );
        $this->assertFalse( empty($options) );
    }

    /**
     * Testing: kunci jawaban sudah tersedia.
     *
     * @test
     * @return void
     */
    public function make_sure_that_answers_key_isset() : Void
    {
        $answersKey = $this->corrector->getAnswersKey();

        $this->assertTrue( is_array($answersKey) );
        $this->assertFalse( empty($answersKey) );
    }

    /**
     * Testing: jawaban sudah tersedia.
     *
     * @test
     * @return MultipleChoiceAnswerCorrector
     */
    public function make_sure_that_answers_isset() : MultipleChoiceAnswerCorrector
    {
        $this->corrector->setAnswers([
            'b', 'c', 'a', 'a',
            'b', 'b', 'c', 'b',
            'a', 'a'
        ]);

        $answers = $this->corrector->getAnswers();

        $this->assertTrue( is_array($answers) );
        $this->assertFalse( empty($answers) );

        return $this->corrector;
    }

	/**
	 * Testing: koreksi jawaban soal pilihan ganda mata pejalaran Bahasa Indonesia. 
	 *
	 * @test
     * @depends make_sure_that_answers_isset
     * @param  MultipleChoiceAnswerCorrector $corrector 
	 * @return void
	 */
    public function can_correction_bahasa_indonesia_answers(MultipleChoiceAnswerCorrector $corrector)
    {
    	$corrector->correction();

        $this->assertTrue( is_array($corrector->getCorrectAnswers(true)) );
        $this->assertTrue( is_array($corrector->getWrongAnswers(true)) );

    	$this->assertEquals($corrector->getCorrectAnswers(), 8);
    	$this->assertEquals($corrector->getWrongAnswers(), 2);

    	$this->assertEquals($corrector->getScore(), 80);
    }
}
