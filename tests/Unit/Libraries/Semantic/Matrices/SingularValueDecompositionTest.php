<?php

namespace Tests\Unit\Libraries\Semantic\Matrices;

use App\Libraries\Semantic\Math\SingularValueDecomposition;
use Tests\TestCase;

class SingularValueDecompositionTest extends TestCase
{
	/**
	 * Menghitung nilai svd dari matrix
	 * ordo 2x2
	 * 
	 * @test
	 * @return void
	 */
	public function find_svd_of_matrix_2_by_2()
	{
		$svd = new SingularValueDecomposition();
		$svd = $svd->SVD([
			[1, 1], 
			[2, 1]
		])['svd'];

		

		$this->assertEquals([[1, 1], [2, 1]], $svd->toArray());
	}
}
