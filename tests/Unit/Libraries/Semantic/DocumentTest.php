<?php

namespace Tests\Unit\Libraries\Semantic;

use Tests\TestCase;
use App\Libraries\Semantic\Document;
use App\Libraries\Contracts\Semantic\Document as DocumentContract;

class DocumentTest extends TestCase
{
	/**
	 * @test
	 * @return void
	 */
    public function can_create_document_with_body_and_title() : Void
    {
    	$document = new Document('Tematik Pjok', 'Dokumen yang baru.');

    	$this->assertSame('Dokumen yang baru.', $document->getBody());
    	$this->assertSame('Tematik Pjok', $document->getTitle());
    }

	/**
	 * @test
	 * @return Document
	 */
    public function can_create_document_without_body() : Document
    {
    	$document = new Document('Tematik Pjok');

    	$this->assertInstanceOf(DocumentContract::class, $document);
    	$this->assertTrue(is_null($document->getBody()));

    	return $document;
    }

    /**
     * @test
     * @depends can_create_document_without_body
     * @return Document
     */
    public function can_set_the_body_of_document(Document $document) : Document
    {
    	$document->setBody('Ini adalah isi dari dokumen yang baru.');

    	$this->assertIsString($document->getBody());

    	return $document;
    }

    /**
     * @test
     * @depends can_set_the_body_of_document
     * @return void
     */
    public function can_get_document_body_as_array(Document $document) : Void
    {
    	$this->assertSame(
    		['Ini', 'adalah', 'isi', 'dari', 'dokumen', 'yang', 'baru.'],
    		$document->toArray()
    	);
    }
}
