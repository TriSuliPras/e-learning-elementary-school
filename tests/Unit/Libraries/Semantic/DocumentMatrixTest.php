<?php

namespace Tests\Unit\Libraries\Semantic;

use Tests\TestCase;
use App\Libraries\Semantic\Document;
use App\Libraries\Semantic\DocumentMatrix;
use App\Libraries\Contracts\Semantic\DocumentMatrixable;

class DocumentMatrixTest extends TestCase
{
    private $trainingDocument;

    private $testingDocument;

    public function setUp()
    {
        parent::setUp();

        $this->trainingDocument = new Document('training','Mencoba Analisa dokumen menggunakan metode Latent Semantic Analisis');
        $this->testingDocument = new Document('testing', 'Menganalisa Latent Semantic Analisis');
    }

    /**
     * @test
     * @return DocumentMatrix
     */
	public function can_get_the_term_value()
    {
        $document = new DocumentMatrix($this->trainingDocument, $this->testingDocument);

        $this->assertEquals(
            ['analisa', 'analisis', 'coba', 'dokumen', 'guna', 'latent', 'menganalisa', 'metode', 'semantic'],
            $document->getTerms()
        );

        return $document;
    }

    /**
     * @test
     * @depends can_get_the_term_value
     * @return DocumentMatrix
     */
    public function can_get_the_term_frequency_value(DocumentMatrix $document)
    {
        $this->assertEquals(
            [
                'analisa' => [
                    'q'  => 1,
                    'd' => 0,
                    'tf' => 1
                ], 
                'analisis' => [
                    'q'  => 1,
                    'd' => 1,
                    'tf' => 2
                ], 
                'coba' => [
                    'q'  => 1,
                    'd' => 0,
                    'tf' => 1
                ], 
                'dokumen' => [
                    'q'  => 1,
                    'd' => 0,
                    'tf' => 1
                ], 
                'guna' => [
                    'q'  => 1,
                    'd' => 0,
                    'tf' => 1
                ],  
                'latent' => [
                    'q'  => 1,
                    'd' => 1,
                    'tf' => 2
                ],  
                'menganalisa' => [
                    'q'  => 0,
                    'd' => 1,
                    'tf' => 1
                ],  
                'metode' => [
                    'q'  => 1,
                    'd' => 0,
                    'tf' => 1
                ],  
                'semantic' => [
                    'q'  => 1,
                    'd' => 1,
                    'tf' => 2
                ], 
            ],
            $document->getTf()
        );

        return $document;
    }

}
