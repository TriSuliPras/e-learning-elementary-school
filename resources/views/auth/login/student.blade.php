@extends("auth.login")

@section("background")
    <div class="image" style="background-image: url({{ asset('web/images/bg-student.png') }});"></div>
@endsection

@section("form")
<form action="{{ route('student.auth') }}" method="POST">
    {{ csrf_field() }}
    <div class="form-group empw">
      <input class="form-control top" type="text" name="nisn" placeholder="Nama Pengguna"/>
      <input class="form-control bot" type="password" name="password" placeholder="Password"/>
        <a class="form-text forgot-pw" href="#" title="Forgot password">
            Lupa Password?
        </a>

        <a class="form-text forgot-pw" href="{{ url('/') }}" >
            Ke Halaman Awal
        </a>
    </div>
    <div class="form-action">
      <div class="checkbox mb-3">
        <input type="checkbox" id="remember_me" name="remember_me"/>
        <label for="remember_me">Ingat Saya</label>
      </div>
      <button class="btn btn-primary btn-block" type="submit">Submit</button>
    </div>
</form>
@endsection