<form action="{{ !isset($employee) ? route('management.employee.store') : route('management.employee.update', $employee->id) }}" method="POST" autocomplete="off" enctype="multipart/form-data">
	{{ csrf_field() }} @if (isset($employee))
		{{ method_field("PATCH") }}
	@endif
	<div class="row">
		<div class="col-md-8">
			<dl class="row">
				<dt class="col-4"><h4>{{ trans('label.for.nip') }}</h4></dt>
				<dd class="col-8">
					<input id="nip" 
						   name="nip"
					       type="text"
					       class="form-control {{ $errors->has("nip") ? 'is-invalid' : '' }}"
					       value="{{ isset($employee) ? $employee->nip : (old('nip') ? old('nip') : '') }}"
					       title="{{ trans('label.must_number') }}"
					       data-toggle="tooltip" 
					/>
					@if ($errors->has('nip')) <div class="invalid-feedback">{{ $errors->first('nip') }}</div>@endif
				</dd>

				<dt class="col-4"><h4>{{ trans('label.for.name') }}</h4></dt>
				<dd class="col-8">
					<input id="nama_pegawai" 
						   name="nama_pegawai"
						   type="text"
						   class="form-control {{ $errors->has("nip") ? 'is-invalid' : '' }}"
						   value="{{ isset($employee) ? $employee->nama_pegawai : (old('nama_pegawai') ? old('nama_pegawai') : '') }}"
					/>
					@if ($errors->has('nama_pegawai')) <div class="invalid-feedback">{{ $errors->first('nama_pegawai') }}</div>@endif
				</dd>

				<dt class="col-4"><h4>{{ trans('label.for.sex') }}</h4></dt>
				<dd class="col-8">
					<select name="jenis_kelamin" class="select-control" disabled="true">
						<option value="L" {{ isset($employee) && $employee->jenis_kelamin === 'L' ? 'selected' : '' }}>Laki-Laki</option>
						<option value="P" {{ isset($employee) && $employee->jenis_kelamin === 'P' ? 'selected' : '' }}>Perempuan</option>
					</select>
				</dd>

				<dt class="col-4"><h4>{{ trans('label.for.POB') }}</h4></dt>
				<dd class="col-8">
					<input name="tempat"
						   type="text"
						   class="form-control" 
						   value="{{ isset($employee) ? $place : (old('tempat') ? old('tempat') : '') }}" 
						   readonly
						  />
				</dd>

				<dt class="col-4"><h4>{{ trans('label.for.BOD') }}</h4></dt>
				<dd class="col-8">
					<div id="datetimepicker" class="input-group date" data-target-input="nearest">
						<input name="ttl"
							   type="text"
							   class="form-control datetimepicker-input"
							   data-target="#datetimepicker"
							   data-date-format="dd MM yyyy" 
							   value="{{ isset($employee) ? $employee->ttl : '' }}" 
							   readonly
						/>
	               		<div class="input-group-append" data-target="#datetimepicker" data-toggle="datetimepicker">
	                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
	                    </div>
					</div>
				</dd>
				
				<dt class="col-4"><h4>{{ trans('label.for.address') }}</h4></dt>
				<dd class="col-8">
					<input id="alamat" 
						   name="alamat"
						   type="text"
						   class="form-control {{ $errors->has("alamat") ? 'is-invalid' : '' }}"
						   value="{{ isset($employee) ? $employee->alamat : (old('alamat') ? old('alamat') : '') }}"
					/>
					@if ($errors->has('alamat')) <div class="invalid-feedback">{{ $errors->first('alamat') }}</div>@endif
				</dd>

				<dt class="col-4"><h4>{{ trans('label.for.group') }}</h4></dt>
				<dd class="col-8">
					<input name="gol"
						   type="text"
						   class="form-control" 
						   value="{{ isset($employee) ? $employee->gol : (old('gol') ? old('gol') : '') }}"
					/>
				</dd>
				
				<dt class="col-4"><h4>{{ trans('label.for.phone') }}</h4></dt>
				<dd class="col-8">
					<input id="no_telp" 
					       name="no_telp"
						   type="text"
						   class="form-control {{ $errors->has("no_telp") ? 'is-invalid' : '' }}"
						   value="{{ isset($employee) ? $employee->no_telp : (old('no_telp') ? old('no_telp') : '') }}" 
					/>
					@if ($errors->has('no_telp')) <div class="invalid-feedback">{{ $errors->first('no_telp') }}</div>@endif
				</dd>

				<dt class="col-4"><h4>{{ trans('label.for.as') }}</h4></dt>
				<dd class="col-8">
					<input name="jabatan"
						   type="text"
						   class="form-control"
						   value="{{ isset($employee) ? $employee->jabatan : (old('jabatan') ? old('jabatan') : '') }}">
				</dd>
				<dt class="col-4"><h4>{{ trans('label.for.pic') }}</h4></dt>
				<dd class="col-8">
				    <div class="custom-file">
				      <input id="foto_pegawai"
				      		 name="foto_pegawai" 
				           	 type="file" 
				           	 class="custom-file-input {{ !$errors->first('foto_pegawai') ?: 'is-invalid' }}"  
				           	value="{{ old('foto_pegawai') ? old('foto_pegawai') : '' }}" />
				          <div class="invalid-feedback" style="display: inline;">{{ $errors->first('foto_pegawai') }}</div> 
				        <label class="custom-file-label" for="foto_pegawai">{{ isset($employee) ? $employee->foto_pegawai : '' }}</label>
				    </div>
				</dd>
			</dl>

			<div class="mb-3">
				<ul class="nav nav-tabs" id="employee-tab" role="tablist">
					<li class="nav-item">
						<a id="education-tab" href="#education" role="tab" aria-controls="education" aria-selected="true" class="nav-link active" data-toggle="tab">{{ trans('label.for.education') }}</a>
					</li>

					<li class="nav-item">
						<a id="appointment-tab" href="#appointment" role="tab" aria-controls="appointment" aria-selected="false" class="nav-link" data-toggle="tab">{{ trans('label.for.appointed') }}</a>
					</li>

					<li class="nav-item">
						<a id="graduation-tab" href="#graduation" role="tab" aria-controls="graduation" aria-selected="false" class="nav-link" data-toggle="tab">{{ trans('label.for.tmt') }}</a>
					</li>

					<li class="nav-item">
						<a id="status-tab" href="#status" role="tab" aria-controls="status" aria-selected="false" class="nav-link" data-toggle="tab">{{ trans('label.for.-s') }}</a>
					</li>
				</ul>
			</div>

			<div id="employee-tab-content" class="tab-content">
				<div id="education" class="tab-pane fade show active" role="tabpanel" aria-labelledby="education-tab">
					<dl class="row">
						<dt class="col-2"><h4>{{ trans('label.for.last_edu') }}</h4></dt>
						<dd class="col-6">
							<input name="pendidikan_terakhir"
								   type="text"
								   class="form-control" 
								   value="{{ isset($employee) ? $employee->pendidikan_terakhir : (old('pendidikan_terakhir') ? old('pendidikan_terakhir') : '') }}" 
								   {{-- required="" --}}
							       {{-- oninvalid="this.setCustomValidity('Field Ini Harus Diisi')" --}}
		               			   {{-- oninput="this.setCustomValidity('')" --}}/>
						</dd>
					</dl>

					<dl class="row">
						<dt class="col-2"><h4>{{ trans('label.for.place') }}</h4></dt>
						<dd class="col-6">
							<input name="pendidikan_terakhir_tempat"
								   type="text"
								   class="form-control"
								   value="{{ isset($employee) ? $employee->pendidikan_terakhir_tempat : (old('pendidikan_terakhir_tempat') ? old('pendidikan_terakhir_tempat') : '') }}" 
								   {{-- required="" --}}
							       {{-- oninvalid="this.setCustomValidity('Field Ini Harus Diisi')" --}}
		               			   {{-- oninput="this.setCustomValidity('')" --}} />
						</dd>
					</dl>
				</div>

				<div id="appointment" class="tab-pane fade show" role="tabpanel" aria-labelledby="appointment-tab">
					<dl class="row">
						<dt class="col-2"><h4>{{ trans('label.for.cpns') }}</h4></dt>
						<dd class="col-6">
							<input id="CPNS_TMT" 
								   name="CPNS_TMT"
								   type="number"
								   class="form-control {{ $errors->has("CPNS_TMT") ? 'is-invalid' : '' }}"
								   value="{{ isset($employee) ? $employee->CPNS_TMT : (old('CPNS_TMT') ? old('CPNS_TMT') : '') }}"
									title="{{ trans('label.must_number') }}"
									data-toggle="tooltip" 
							/>
							@if ($errors->has('CPNS_TMT')) <div class="invalid-feedback">{{ $errors->first('CPNS_TMT') }}</div>@endif
						</dd>
					</dl>

					<dl class="row">
						<dt class="col-2"><h4>{{ trans('label.for.pns') }}</h4></dt>
						<dd class="col-6">
							<input id="PNS_TMT" 
								   name="PNS_TMT"
								   type="number"
								   class="form-control {{ $errors->has("PNS_TMT") ? 'is-invalid' : '' }}"
								   value="{{ isset($employee) ? $employee->PNS_TMT : (old('PNS_TMT') ? old('PNS_TMT') : '') }}"
								   title="{{ trans('label.must_number') }}"
									data-toggle="tooltip" 
								   >
							@if ($errors->has('PNS_TMT')) <div class="invalid-feedback">{{ $errors->first('PNS_TMT') }}</div>@endif
						</dd>
					</dl>
				</div>

				<div id="graduation" class="tab-pane fade show" role="tabpanel" aria-labelledby="graduation-tab">
					<dl class="row">
						<dt class="col-2"><h4>{{ trans('label.for.-d') }}</h4></dt>
						<dd class="col-6">
							<input id="tmt_guru_tanggal" 
								   name="tmt_guru_tanggal"
								   type="number"
								   class="form-control {{ $errors->has("tmt_guru_tanggal") ? 'is-invalid' : '' }}"
								   value="{{ isset($employee) ? $employee->tmt_guru_tanggal : (old('tmt_guru_tanggal') ? old('tmt_guru_tanggal') : '') }}"
									title="{{ trans('label.must_number') }}"
									data-toggle="tooltip" 
								   >
							@if ($errors->has('tmt_guru_tanggal')) <div class="invalid-feedback">{{ $errors->first('tmt_guru_tanggal') }}</div>@endif
						</dd>
					</dl>

					<dl class="row">
						<dt class="col-2"><h4>{{ trans('label.for.-m') }}</h4></dt>
						<dd class="col-6">
							<input id="tmt_guru_bulan" 
								   name="tmt_guru_bulan"
								   type="number"
								   class="form-control {{ $errors->has("tmt_guru_bulan") ? 'is-invalid' : '' }}"
								   value="{{ isset($employee) ? $employee->tmt_guru_bulan : (old('tmt_guru_bulan') ? old('tmt_guru_bulan') : '') }}"
									title="{{ trans('label.must_number') }}"
									data-toggle="tooltip" 
								   >
							@if ($errors->has('tmt_guru_bulan')) <div class="invalid-feedback">{{ $errors->first('tmt_guru_bulan') }}</div>@endif
						</dd>
					</dl>

					<dl class="row">
						<dt class="col-2"><h4>{{ trans('label.for.-y') }}</h4></dt>
						<dd class="col-6">
							<input id="tmt_guru_tahun" 
								   name="tmt_guru_tahun"
								   type="number"
								   class="form-control {{ $errors->has("tmt_guru_tahun") ? 'is-invalid' : '' }}"
								   value="{{ isset($employee) ? $employee->tmt_guru_tahun : (old('tmt_guru_tahun') ? old('tmt_guru_tahun') : '') }}"
								   title="{{ trans('label.must_number') }}"
									data-toggle="tooltip" 
								   >
							@if ($errors->has('tmt_guru_tahun')) <div class="invalid-feedback">{{ $errors->first('tmt_guru_tahun') }}</div>@endif
						</dd>
					</dl>
				</div>

				<div id="status" class="tab-pane fade show" role="tabpanel" aria-labelledby="status-tab">
					<dl class="row">
						<dt class="col-2"><h4>{{ trans('label.for.-s -w', ['is' => "GTT"]) }}</h4></dt>
						<dd class="col-6">
							<input name="GTT"
								   type="text"
								   class="form-control"
								   value="{{ isset($employee) ? $employee->GTT : (old('GTT') ? old('GTT') : '') }}" />
						</dd>
					</dl>

					<dl class="row">
						<dt class="col-2"><h4>{{ trans('label.for.-s -w', ['is' => "GTY"]) }}</h4></dt>
						<dd class="col-6">
							<input name="GTY"
								   type="text"
								   class="form-control"
								   value="{{ isset($employee) ? $employee->GTY : (old('GTY') ? old('GTY') : '') }}" />
						</dd>
					</dl>

					<dl class="row">
						<dt class="col-2"><h4>{{ trans('label.for.-s -w', ['is' => "NUPTK"]) }}</h4></dt>
						<dd class="col-6">
							<input name="NUPTK"
								   type="text"
								   class="form-control"
								   value="{{ isset($employee) ? $employee->NUPTK : (old('NUPTK') ? old('NUPTK') : '') }}" />
						</dd>
					</dl>

					<dl class="row">
						<dt class="col-2"><h4>{{ trans('label.for.-s -w', ['is' => "Sertifikasi"]) }}</h4></dt>
						<dd class="col-6">
							<input id="sertifikasi_tahun" 
								   name="sertifikasi_tahun"
								   type="number"
								   class="form-control {{ $errors->has("sertifikasi_tahun") ? 'is-invalid' : '' }}"
								   value="{{ isset($employee) ? $employee->sertifikasi_tahun : (old('sertifikasi_tahun') ? old('sertifikasi_tahun') : '') }}" 
								   title="{{ trans('label.must_number') }}"
									data-toggle="tooltip" 
		               			   />
		               		@if ($errors->has('sertifikasi_tahun')) <div class="invalid-feedback">{{ trans('validation.invalid_date', ['what' => "Tahun"]) }}</div>@endif
						</dd>
					</dl>
				</div>
			</div>
			
			<br />
			<div class="row">
				<div class="form-action">
					<a class="btn btn-outline-secondary btn-round" href="{{ route('management.employee.show', $employee->id) }}" data-toggle="tooltip" title="{{ trans('label.back_to', ['what' => "Profil"]) }}">
						Batal</a>
                	<button class="btn btn-success btn-round" type="submit">{{ !isset($employee) ? trans('label.btn.sv') : trans('label.btn.ud') }}</button>
              	</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="row">
				<div>
					<div id="img-profil"></div>
				</div>
			</div>
		</div>
	</div>
</form>