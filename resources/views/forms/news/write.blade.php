<form action="{{ !isset($news) ? route('news.store') : route('news.update', $news->id) }}" method="POST" class="mb-5">
	{{ csrf_field() }} @isset ($news)
      {{ method_field('PATCH') }}
  @endisset

	<input name="status" type="text" hidden value="active">
	<input name="user_id" type="text" hidden value="{{ auth()->user()->id }}">
    <input name="title" type="text" hidden value="{{ $datas['title'] }}">
    <input name="receiver" type="text" hidden value="{{ $datas['receiver'] }}">
    <input name="student_id" type="text" hidden value="{{ to_json($datas['students']) }}">
    <input name="class_room[]" type="text" hidden value="{{ to_json($datas['classes']) }}">

    <div class="form-group">
        <textarea id="editor" name="description" class="form-control">@isset ($news)
            {{ $news->description }}
        @endisset</textarea>
	</div>

	<hr/>
  	<div class="form-action">
        <a class="btn btn-outline-info btn-round" title="Halaman sebelumnya" data-toggle="tooltip" href="{{ route('news.add.category') }}">
          {{ trans('label.btn.cn') }}
        </a>
    	<button class="btn btn-primary btn-round" type="submit">@isset ($news)
          {{ trans('label.btn.ud') }} @else {{ trans('label.btn.sv') }}
      @endisset</button>
  	</div>
</form>