<form method="POST" class="mb-5" autocomplete="off">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="title" >{{ trans('label.for.-T') }}</label>
        <input name="title"
               type="text" class="form-control {{ $errors->has("title") ? 'is-invalid' : '' }}"
               placeholder="{{ trans('label.placeholder.max_75') }}"
               value="{{ isset($news) ? $news->title : ( old('title') ? old('title') : ( session('title') ? session('title') : '' ) ) }}"
               {{-- required --}}
               oninvalid="this.setCustomValidity('Field Belum Diisi')"
               oninput="this.setCustomValidity('')">
        @if ($errors->has('title')) <div class="invalid-feedback">{{ $errors->first("title") }}</div>@endif
    </div>

    <div class="form-group">
        <label for="title" >{{ trans('label.category') }}</label>
        <select name="receiver" class="select-control" title="Pilih..." data-toggle="tooltip" {{-- required --}} oninvalid="this.setCustomValidity('Field Belum Diisi')" onchange="this.setCustomValidity('')">
            <option value="common" @if (isset($news))
                {{ is_equal($news->receiver, "Common") ? 'selected' : ( old('receiver') ? old('receiver') : '' ) }}
            @endif>
                {{ trans('label.common') }}
            </option>
            <option value="management" @if (isset($news))
                {{ is_equal($news->receiver, "Management") ? 'selected' : ( old('receiver') ? old('receiver') : '' ) }}
            @endif>{{ trans('label.for.-M') }}</option>
            <option value="student" @if (isset($news))
                {{ is_equal($news->receiver, "Student") ? 'selected' : ( old('receiver') ? old('receiver') : '' ) }}
            @endif>{{ trans('label.for.-S') }}</option>
            <option value="classroom" @if (isset($news))
                {{ is_equal($news->receiver, "Class") ? 'selected' : ( old('receiver') ? old('receiver') : '' ) }}
            @endif>{{ trans('label.for.-C') }}</option>
        </select>
    </div>

    <div id="cls" class="form-group" style="display: none;">
        <div class="row">
            <div class="col-md-6">
                <label>{{ trans('label.pararel', ['what' => "A"]) }}</label>
                <div class="checkbox">
                    <input name="class_room[]" type="checkbox" id="class1A" value="1 A" />
                    <label for="class1A">{{ trans('label.classroom', ['what' => 1]) }}</label>
                </div>

                <div class="checkbox">
                    <input name="class_room[]" type="checkbox" id="class2A" value="2 A"/>
                    <label for="class2A"> {{ trans('label.classroom', ['what' => 2]) }} </label>
                </div>

                <div class="checkbox">
                    <input name="class_room[]" type="checkbox" id="class3A" value="3 A"/>
                    <label for="class3A"> {{ trans('label.classroom', ['what' => 3]) }} </label>
                </div>

                <div class="checkbox">
                    <input name="class_room[]" type="checkbox" id="class4A" value="4 A"/>
                    <label for="class4A"> {{ trans('label.classroom', ['what' => 4]) }} </label>
                </div>

                <div class="checkbox">
                    <input name="class_room[]" type="checkbox" id="class5A" value="5 A"/>
                    <label for="class5A"> {{ trans('label.classroom', ['what' => 5]) }} </label>
                </div>

                <div class="checkbox">
                    <input name="class_room[]" type="checkbox" id="class6A" value="6 A"/>
                    <label for="class6A"> {{ trans('label.classroom', ['what' => 6]) }} </label>
                </div>
            </div>

            <div class="col-md-6">
                <label>{{ trans('label.pararel', ['what' => "B"]) }}</label>
                <div class="checkbox">
                    <input name="class_room[]" type="checkbox" id="class1B" value="1 B"/>
                    <label for="class1B"> {{ trans('label.classroom', ['what' => 1]) }} </label>
                </div>

                <div class="checkbox">
                    <input name="class_room[]" type="checkbox" id="class2B" value="2 B"/>
                    <label for="class2B"> {{ trans('label.classroom', ['what' => 2]) }} </label>
                </div>

                <div class="checkbox">
                    <input name="class_room[]" type="checkbox" id="class3B" value="3 B"/>
                    <label for="class3B"> {{ trans('label.classroom', ['what' => 3]) }} </label>
                </div>

                <div class="checkbox">
                    <input name="class_room[]" type="checkbox" id="class4B" value="4 B"/>
                    <label for="class4B"> {{ trans('label.classroom', ['what' => 4]) }} </label>
                </div>

                <div class="checkbox">
                    <input name="class_room[]" type="checkbox" id="class5B" value="5 B"/>
                    <label for="class5B"> {{ trans('label.classroom', ['what' => 5]) }} </label>
                </div>

                <div class="checkbox">
                    <input name="class_room[]" type="checkbox" id="class6B" value="6 B"/>
                    <label for="class6B"> {{ trans('label.classroom', ['what' => 6]) }} </label>
                </div>
            </div>
        </div>
    </div>

    <div id="std" class="form-group" style="display: none;">
        <label for="student">Siswa</label>
        <select id="student" class="select-control" title="Pilih Siswa" data-live-search="true" multiple="true">
            @isset ($news)
                @if (! empty($news->students->toArray()))
                    @foreach ($news->students as $student)
                        @foreach ($students as $std)
                            @if (is_equal($student->id, $std->id))
                                <option value="{{ $std->id }}" data-subtext="{{ "$std->nisn" }}" selected>{{ $std->name }}</option>
                            @else
                                <option value="{{ $std->id }}" data-subtext="{{ "$std->nisn" }}">{{ $std->name }}</option>
                            @endif
                        @endforeach
                    @endforeach
                @else
                    @foreach ($students as $std)
                        <option value="{{ $std->id }}" data-subtext="{{ "$std->nisn" }}">{{ $std->name }}</option>
                    @endforeach
                @endif
            @else
                @foreach ($students as $std)
                    <option value="{{ $std->id }}" data-subtext="{{ "$std->nisn" }}">{{ $std->name }}</option>
                @endforeach
            @endisset
        </select>
        <div id="display" cols="30" rows="5" class="form-control" style="height: auto;"></div>
    </div>
	<hr/>
  	<div class="form-action">
        <a class="btn btn-outline-info btn-round" href="{{ route('news') }}" title="{{ trans('label.back_to', ['what' => "Berita"]) }}" data-toggle="tooltip">Batal</a>
    	<button class="btn btn-primary btn-round" type="submit">{{ trans('label.btn.sv') }}</button>
  	</div>
</form>