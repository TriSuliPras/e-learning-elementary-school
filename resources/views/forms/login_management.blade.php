<form action="{{ route('management.auth') }}" method="POST" autocomplete="off">
    {{ csrf_field() }}
    <div class="form-group emp">
        <input class="form-control top" type="text" name="name" placeholder="{{ trans('label.placeholder.uname') }}"/>
        <input class="form-control bot" type="password" name="password" placeholder="{{ trans('label.placeholder.pass') }}"/>
        <div class="invalid-feedback" style="display: inline;"></div> 
        <a class="form-text forgot-pw" href="#" title="Forgot password">{{ trans('label.forget') }}</a>
    </div>
    <div class="form-action">
        <div class="checkbox mb-3">
            <input type="checkbox" id="remember_token2" name="remember_token"/>
            <label for="remember_token2">{{ trans('label.remember_me') }}</label>
        </div>
        <button class="btn btn-primary btn-block" type="submit">{{ trans('label.btn.lg') }}</button>
    </div>
</form>