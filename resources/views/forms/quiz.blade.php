<form class="needs-validation" action="{{ !isset($quiz) ? route('quiz.store') : route('quiz.update', $quiz->id) }}" method="POST" enctype="multipart/form-data" autocomplete="off">
	{{ csrf_field() }}
	@if (isset($quiz)) {{ method_field("PATCH") }} @endif

	<div class="form-group" hidden>
		<input name="user_id"
			   type="text"
			   class="form-control"
			   value="{{ auth()->user()->id }}">
	</div>

    <div class="form-group">
    	<label for="title">{{ trans('label.for.-T') }}</label>
		<input id="title" 
			   name="title"
			   type="text" 
			   class="form-control {{ !$errors->first('title') ?: 'is-invalid' }}" 
			   placeholder="{{ trans('label.placeholder.max_75') }}" 
			   value="{{ isset($quiz) ? $quiz->title : old('title') }}" 
		>
        <div class="invalid-feedback" style="display: inline;">{{ $errors->first('title') }}</div> 
	</div>
	
	<div class="row">
		<div class="form-group col-md-4 col-sm-6">
			<label for="class_room">{{ trans('label.for.-C') }}</label>
			<select id="class_room"
					name="class_room"
					class="select-control {{ !$errors->first('class_room') ?: 'is-invalid' }}"
					title="{{ trans('label.choose') }}">
	        	@foreach ($classes as $classroom)
	        		@if ( isset($quiz) && (is_equal(is_equal($quiz->class_room, $classroom->nama_kelas), is_equal($quiz->pararel, $classroom->palarel))) )
	                	<option value="{{ "$classroom->nama_kelas $classroom->palarel" }}" selected>{{ "$classroom->nama_kelas $classroom->palarel" }}</option>
					@else
						<option value="{{ "$classroom->nama_kelas $classroom->palarel" }}" {{ is_equal(old('class_room'), "$classroom->nama_kelas $classroom->palarel") ? "selected" : '' }}>{{ "$classroom->nama_kelas $classroom->palarel" }}</option>
	        		@endif
	        	@endforeach
	        </select>
	        <div class="invalid-feedback" style="display: inline;">{{ $errors->first('class_room') }}</div> 
		</div>

		<div class="form-group col-md-8 col-sm-6">
			<label for="lesson_id">{{ trans('label.for.-L') }}</label>
	        <select id="lesson_id" 
	        		name="lesson_id"
	        		class="select-control {{ !$errors->first('lesson_id') ?: 'is-invalid' }}"
	        		title="{{ trans('label.choose') }}"
	        		data-live-search="true" style="{{ $errors->has('lesson_id') ? 'border: 1px solid red;' : '' }}">
	        	@foreach ($lessons as $lesson)
	                @if (isset($quiz) && (is_equal($quiz->lesson_id, $lesson->id)) )
	                    <option value="{{ $lesson->id }}" data-subtext=" ({{ $lesson->curriculums['nama_kurikulum'] }})" selected>{{ $lesson->nama_matapelajaran }}</option>
	                @else
	                    <option value="{{ $lesson->id }}" data-subtext=" ({{ $lesson->curriculums['nama_kurikulum'] }})" {{ is_equal((int) old('lesson_id'), $lesson->id) ? "selected" : '' }}>{{ $lesson->nama_matapelajaran }}</option>
	                @endif
	        	@endforeach
	        </select>
	        <div class="invalid-feedback" style="display: inline;">{{ $errors->first('lesson_id') }}</div> 
	    </div>
    </div>

	<div class="row">
		<div class="form-group col-md-8">
			<label for="datetime">{{ trans('label.for.--T') }}</label>
			<div class="date-control">
				<div class="input-group date" id="datetimepicker" data-target-input="nearest">
	                <input id="datetime" 
	                	   name="datetime"
	                	   type="text" 
	                	   class="form-control datetimepicker-input {{ !$errors->first('datetime') ?: 'is-invalid' }}" 
	                	   data-target="#datetimepicker" 
	                	   placeholder="bln-tgl-th j:m "
	                	   value="{{ isset($quiz) ? datetimepicker($quiz->starting_time) : (old('datetime') ? datetimepicker(old('datetime')) : '') }}" />
	                <div id="picker" class="input-group-append" data-target="#datetimepicker" data-toggle="datetimepicker">
	                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
	                </div>
	            </div>
            </div>
            <div class="invalid-feedback datetime" style="display: inline;">{{ $errors->first('datetime') }}</div> 
		</div>
		
        <div class="form-group col-md-4">
			<label for="count_down">{{ trans('label.for.-c') }} <small>{{ trans('label.optional') }}</small></label>
			<input id="count_down" 
				   name="count_down"
				   type="number"
				   class="form-control {{ !$errors->first('count_down') ?: 'is-invalid' }}"
				   value="{{ isset($quiz) ? $quiz->count_down : (old('count_down') ? old('count_down') : '120') }}"
				   title="Max 120 menit" 
				   data-toggle="tooltip"/>
			<div class="invalid-feedback" style="display: inline;">{{ $errors->first('count_down') }}</div>
        </div>
    </div>
	
	<h3 class="mb-2">{!! trans('label.weight_exercise') !!}</h3>
    <div class="row">
    	<div class="form-group col-md-6">
    		<label for="weight_multiple">{{ trans('label.multi') }}</label>
    		<input id="weight_multiple" 
    			   name="weight_multiple" 
    			   type="number" 
    			   class="form-control {{ !$errors->first('weight_multiple') ?: 'is-invalid' }}" 
    			   placeholder="ex: 50"
    			   value="{{ isset($quiz) ? $quiz->weight_multiple : ( old('weight_multiple') ? old('weight_multiple') : '' ) }}">
    		<div class="invalid-feedback" style="display: inline;">{{ $errors->first('weight_multiple') }}</div> 
    	</div>

    	<div class="form-group col-md-6">
    		<label for="weight_essay">{{ trans('label.essay') }}</label>
    		<input id="weight_essay" 
    			   name="weight_essay" 
    			   type="number" 
    			   class="form-control {{ !$errors->first('weight_essay') ?: 'is-invalid' }}" 
    			   placeholder="ex: 50"
    			   value="{{ isset($quiz) ? $quiz->weight_essay : ( old('weight_essay') ? old('weight_essay') : '' ) }}">
    		<div class="invalid-feedback" style="display: inline;">{{ $errors->first('weight_essay') }}</div> 
    	</div>
    </div>

	<div class="form-group">
        <label>{{ trans('label.for.-D') }} <small>{{ trans('label.optional') }}</small></label>
        <textarea name="description" class="form-control" rows="5" title="{{ trans('label.for.info') }}" data-toggle="tooltip">{{ isset($quiz) ? $quiz->description : (old('description') ? old('description') : '') }}</textarea>
    </div>
	
	<div class="form-group">
		<div class="custom-file">
	 		<input id="file_path" 
	 			   name="file_path" 
	 			   type="file" 
	 			   class="custom-file-input {{ !$errors->first('file_path') ?: 'is-invalid' }}" 
	 			   title="Pilih File" 
	 			   id="file_path" 
	 			   value="{{ old('file_path') ? old('file_path') : '' }}" />
	        <div class="invalid-feedback" style="display: inline;">{{ $errors->first('file_path') }}</div> 
	  		<label class="custom-file-label" for="file_path">{{ isset($quiz) ? $quiz->file_name : trans('label.file.q') }}</label>
		</div>
	</div>
    <div class="modal-footer">
        <a class="btn btn-outline-secondary btn-round" href="{{ route('quiz') }}" title="{{ trans('label.back_to', ['what' => "Kuis"]) }}" data-toggle="tooltip">
            {{ trans('label.btn.cn') }}
        </a>
        <button id="submit" class="btn btn-primary btn-round" type="submit">{{ !isset($quiz) ? trans('label.btn.sv') : trans('label.btn.ud') }}</button>
    </div>
</form>