<form action="{{ route('management.student.store') }}" method="POST">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-md-8">
			<dl class="row">
				<dt class="col-4"><h4>NISN</h4></dt>
				<dd class="col-8">
					<input name="nisn"
						   type="text"
						   class="form-control" 
						   required
						   title="{{ $errors->has('nisn') ? $errors->get('nisn')[0] : '' }}"
						   data-toggle="tooltip"
						   placeholder="Masukan Angka" 
						   value="{{ old('nisn') ? old('nisn') : '' }}" 
						    
					/>
				</dd>

				<dt class="col-4"><h4>NIS</h4></dt>
				<dd class="col-8">
					<input name="nis"
						   type="text"
						   class="form-control" 
						   value="{{ old('nis') ? old('nis') : '' }}"
						   readonly 
					/>
				</dd>

				<dt class="col-4"><h4>NIK</h4></dt>
				<dd class="col-8">
					<input name="nik"
						   type="text"
						   class="form-control" 
						   value="{{ old('nik') ? old('nik') : '' }}"
						   readonly="true"
					/>
				</dd>

				<dt class="col-4"><h4>Nama</h4></dt>
				<dd class="col-8">
					<input name="nama_siswa"
						   type="text"
						   class="form-control" 
						   value="{{ old('nama_siswa') ? old('nama_siswa') : '' }}"
						   readonly 
					/>
				</dd>

				<dt class="col-4"><h4>Kelas</h4></dt>
				<dd class="col-8">
					<select name="kelas" class="select-control" title="Pilih Kelas" required oninvalid="this.setCustomValidity('Field Ini Harus Diisi')" onchange="this.setCustomValidity('')">
						<option value="1" {{ old('kelas') && old('kelas') == '1' ? 'selected' : '' }}>1</option>
						<option value="2" {{ old('kelas') && old('kelas') == '2' ? 'selected' : '' }}>2</option>
						<option value="3" {{ old('kelas') && old('kelas') == '3' ? 'selected' : '' }}>3</option>
						<option value="4" {{ old('kelas') && old('kelas') == '4' ? 'selected' : '' }}>4</option>
						<option value="5" {{ old('kelas') && old('kelas') == '5' ? 'selected' : '' }}>5</option>
						<option value="6" {{ old('kelas') && old('kelas') == '6' ? 'selected' : '' }}>6</option>
					</select>
				</dd>

				<dt class="col-4"><h4>Status</h4></dt>
				<dd class="col-8">
					<input name="status"
						   type="text"
						   class="form-control" 
						   required
						   value="{{ old('status') ? old('status') : '' }}"
						   oninvalid="this.setCustomValidity('Field Ini Harus Diisi')"
               			   oninput="this.setCustomValidity('')" />
				</dd>

				<dt class="col-4"><h4>Foto</h4></dt>
				<dd class="col-8">
				    <div class="custom-file">
				      <input name="foto_siswa" 
				           type="file" 
				           class="custom-file-input {{ !$errors->first('foto_siswa') ?: 'is-invalid' }}" 
				           id="foto_siswa" 
				           value="{{ old('foto_siswa') ? old('foto_siswa') : '' }}" />
				          <div class="invalid-feedback" style="display: inline;">{{ $errors->first('foto_siswa') }}</div> 
				        <label class="custom-file-label" for="foto_siswa">{{ isset($material) ? $material->file_name : "Pilih..." }}</label>
				    </div>
				</dd>
			</dl>

			<div class="mb-3">
				<ul class="nav nav-tabs" id="employee-tab" role="tablist">
					<li class="nav-item">
						<a id="biodata-tab" href="#biodata" role="tab" aria-controls="biodata" aria-selected="true" class="nav-link active" data-toggle="tab">Biodata</a>
					</li>

					<li class="nav-item">
						<a id="transportation-tab" href="#transportation" role="tab" aria-controls="transportation" aria-selected="false" class="nav-link" data-toggle="tab">Transportasi</a>
					</li>

					<li class="nav-item">
						<a id="education-tab" href="#education" role="tab" aria-controls="education" aria-selected="false" class="nav-link" data-toggle="tab">Pendidikan</a>
					</li>

					<li class="nav-item">
						<a id="family-tab" href="#family" role="tab" aria-controls="family" aria-selected="false" class="nav-link" data-toggle="tab">Keluarga</a>
					</li>

					<li class="nav-item">
						<a id="address-tab" href="#address" role="tab" aria-controls="address" aria-selected="false" class="nav-link" data-toggle="tab">Akun</a>
					</li>
				</ul>
			</div>

			<div id="employee-tab-content" class="tab-content">
				@include("dashboard.management.user_management.student.information.biodata")

				@include("dashboard.management.user_management.student.information.transportation")	

				@include("dashboard.management.user_management.student.information.education")

				@include("dashboard.management.user_management.student.information.family")		

				@include("dashboard.management.user_management.student.information.address")				
			</div>
			
			<br />
			<div class="row">
				<div class="form-action">
					<a class="btn btn-danger" href="">Batal</a>
                	<button class="btn btn-primary" type="submit">{{ !isset($student) ? "Simpan" : "Perbarui" }}</button>
              	</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="row">
				<div>
					<div id="img-profil"></div>
					
					<div id="img-input" class="inputfile">
						<input name="foto_siswa" type="file" id="inputfile">
						<label for="inputfile" class="btn btn-info btn-round">
							Pilih File
						</label>
						<p>Nama File : </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>