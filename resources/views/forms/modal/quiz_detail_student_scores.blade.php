<div class="table-responsive">
    <table id="student_scores" class="table table-custom col-md-12">
        <thead>
            <tr>
                <th rowspan="2" width="1%">{{ trans('label.for.#') }}</th>
                <th rowspan="2" width="25%">{{ trans('label.for.name').' '.trans('label.for.-S') }}</th>
                <th rowspan="2" width="25%">{{ trans('label.for.nisn') }}</th>
                <th colspan="3">{{ trans('label.score') }}</th>
                <th rowspan="2" width="1%"></th>
            </tr>
            <tr>
                <th width="130px">{{ trans('label.multi') }}</th>
                <th width="130px">{{ trans('label.essay') }}</th>
                <th width="130px">{{ trans('label.score').' '.trans('label.end') }}</th>
            </tr>
        </thead>
        <tbody id="tbody">
            @foreach ($quiz->students as $student)
                @php
                    $mtp = $student->multiple_scores;
                    $esy = $student->essay_scores;
                @endphp
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $student->nama_siswa }}</td>
                    <td align="center">{{ $student->nisn }}</td>
                    @if ($questions['multiple'] > 0)
                        <td align="center">{{ $mtp }}</td>
                    @else
                        <td align="center" title="Nilai Kosong" data-toggle="tooltip">----</td>
                    @endif
                    @empty ($questions['essay'])
                    <td align="center">----</td>
                    @else
                        @if($esy == "")
                            <td align="center" title="Klik Untuk Mengkoreksi" data-toggle="tooltip">
                                <a href="{{ route('quiz.essay', [$quiz->id, $student->student_id]) }}">
                                    {{ trans('label.!correction') }}
                                </a>
                            </td>
                        @else
                            <td align="center">{{ $esy }}</td>
                        @endempty
                    @endempty
                    <td align="center">{{ $student->final_scores }}</td>
                    @if (is_equal(management()->getId(), $quiz->user_id))
                        @if ($student->status === "unlock" && not_null($quiz->weight_essay))
                            <td align="center">
                                @if (not_null($student->essay_scores))
                                <div class="dropdown">
                                    <button class="btn btn-outline-secondary btn-sm" type="button" data-toggle="dropdown">{{ trans('label.for.-O') }}</button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item edit" href="{{ route('quiz.essay', [$quiz->id, $student->student_id]) }}">
                                            {{ trans('label.act.edit') }}
                                            {{ trans('label.score') }}
                                            {{ trans('label.essay') }}
                                        </a>
                                        <a class="dropdown-item del lock" href="{{ route('quiz.lock', [$quiz->id, $student->student_id]) }}">
                                            {{ trans('label.lock') }}
                                        </a>
                                    </div>
                                </div>
                                @else
                                    ----
                                @endif
                            </td>
                        @else
                            <td align="center">
                                <span class="badge badge-info status">
                                    <i class="fa  fa-lock"></i>
                                    {{ trans('label.locked') }}
                                </span>
                            </td>
                        @endif
                    @else
                        @if ($student->status === "unlock" && not_null($quiz->weight_essay))
                            <td align="center">---</td>  
                        @else
                        <td align="center">
                            <span class="badge badge-info status">
                                <i class="fa  fa-lock"></i>
                                {{ trans('label.locked') }}
                            </span>
                        </td>  
                        @endif 
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>