<form action="{{ route('task.update.student.score', $task->id) }}" method="POST" autocomplete="off">
	{{ csrf_field() }} {{ method_field("PATCH") }}
    <div class="modal-body">
    	<div class="table-responsive">
            <table id="task" class="table table-custom">
                <thead>
                	<tr>
                        <th width="1%">#</th>
                        <th width="40%">{{ trans('label.for.name').' '.trans('label.for.-S') }}</th>
                        <th >{{ trans('label.for.nisn') }}</th>
                        @if ($task->user_id === auth()->user()->id)
                        <th align="center" width="30%">{{ trans('label.std_file_task') }}</th>
                        @endif
                        <th width="13%">{{ trans('label.score') }}</th>
                        @if ($task->user_id === auth()->user()->id)
                        	<th width="1%"></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($task->students as $student)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $student->nama_siswa }}</td>
                            <td align="center">{{ $student->nisn }}</td>
                            @if ($task->user_id === auth()->user()->id)
                            <td align="center">
                            	@if (not_null($student->file_path))
                            	<a href="{{ asset($student->file_path) }}" download="{{ "{$student->nisn}-{$student->nama_siswa}" . '.' . explode('.', $student->file_path)[1] }}">
                            		{{ "{$student->nisn}-{$student->nama_siswa}" }}
                            	</a>
                            	@else
                            		{{ trans('label.empty') }}
                            	@endif
                            </td>
                            @endif
                            <td width="1%" align="center">
                            	@if (empty($student->scores))
                            	    <input name="scores[{{ $student->pivot->student_id }}][scores]" 
                                           type="text" 
                                           {{ $task->user_id != auth()->user()->id ? "readonly" : '' }}
                                           class="form-control" @if ($errors->first("scores.{$student->pivot->student_id}.scores"))
                            	    	   style="border-color: red" 
                            	    	   title="Max [100]" 
                                           data-toggle="tooltip"
                            	    	   value="{{ old("scores.{$student->pivot->student_id}.scores") ?: old("scores.{$student->pivot->student_id}.scores") }}" 
                            	    @endif>
                            	@else
                                    @if (!$errors->first("scores.{$student->pivot->student_id}.scores"))
                                		<input name="scores[{{ $student->pivot->student_id }}][scores]" type="text" class="form-control" style="display: none;" value="{{ $student->scores }}">
                                		<span>{{ $student->scores }}</span>	
                                    @else
                                        <input name="scores[{{ $student->pivot->student_id }}][scores]" 
                                               type="text" 
                                               class="form-control" 
                                               style="border-color: red;" 
                                               title="Max [100]" 
                                               data-toggle="tooltip"
                                               value="{{ $student->scores }}">
                                    @endif
                            	@endif
                            </td>
                            @if ($task->user_id === auth()->user()->id)
                            <td align="center">
                            	@if (empty($student->scores))
                            	    -
                            	@else
                            		@if (is_equal($student->status, "unlock"))
                            		<div class="dropdown">
                            			<button class="btn btn-outline-secondary btn-sm" type="button" data-toggle="dropdown">{{ trans('label.for.-O') }}</button>
                            			<div class="dropdown-menu dropdown-menu-right">
											<a id="edit{{ $student->pivot->student_id }}" class="dropdown-item edit" href="#">
                                    			{{ trans('label.act.edit') }}
                                    		</a>
                                    		<a class="dropdown-item del lock" href="{{ route('task.lock', [$task->id, $student->student_id]) }}">{{ trans('label.lock') }}</a>
                            			</div>
                            		</div>	
                            		@else
                                		<span class="badge badge-info status">
                                            <i class="fa  fa-lock"></i>
                                            {{ trans('label.locked') }}
                                        </span>
                            		@endif	
                            	@endif
                            </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal-footer">
    	<input name="task_id" type="text" hidden value="{{ $task->id }}">
    	@if ($task->status === "unlock" && !empty($task->students->toArray()))
    		@if ($task->user_id === auth()->user()->id)
                <button class="btn btn-success btn-round">{{ trans('label.btn.sv') }}</button>
            @endif
        @endif
    </div>
</form>