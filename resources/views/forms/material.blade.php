<form action="{{! isset($material) ? route('material.save') : route('material.update', $material->id)}}" method="POST" class="mb-5" enctype="multipart/form-data" autocomplete="off">
	{{ csrf_field() }} @if (isset($material))
		{{ method_field("PATCH") }}
	@endif

	<div class="form-group" hidden>
		<input name="user_id" type="number" value="{{ auth()->user()->id }}">
	</div>

	<div class="form-group">
		<label for="title" >{{ trans('label.for.-T') }}</label>
		<input id="title" 
               name="title"
               type="text" class="form-control {{ $errors->has("title") ? 'is-invalid' : '' }}"
               placeholder="Maksimal 75 karakter"
               value="{{ isset($material) ? $material->title : (old('title') ? old('title') : "") }}"
               {{-- required
               oninvalid="this.setCustomValidity('{{ trans('validation.custom.title.required') }}')"
               oninput="this.setCustomValidity('')" --}}>
        @if ($errors->has('title')) <div class="invalid-feedback">{{ $errors->first("title") }}</div>@endif
	</div>
	<div class="row">
    <div class="form-group col-md-4 col-sm-6">
      <label for="class_room">{{ trans('label.for.-C') }}</label>
      <select id="class_room"
          name="class_room"
          class="select-control {{ !$errors->first('class_room') ?: 'is-invalid' }}"
          title="Pilih..."
          {{-- required=""
          oninvalid="this.setCustomValidity('{{ trans('validation.custom.class_room.required') }}')"
          onchange="this.setCustomValidity('')" --}}>
            @foreach ($classes as $classroom)
              @if ( isset($material) && (is_equal(is_equal($material->class_room, $classroom->nama_kelas), is_equal($material->pararel, $classroom->palarel))) )
                    <option value="{{ "$classroom->nama_kelas $classroom->palarel" }}" selected>{{ "$classroom->nama_kelas $classroom->palarel" }}</option>
          @else
            <option value="{{ "$classroom->nama_kelas $classroom->palarel" }}" {{ is_equal(old('class_room'), "$classroom->nama_kelas $classroom->palarel") ? "selected" : '' }}>{{ "$classroom->nama_kelas $classroom->palarel" }}</option>
              @endif
            @endforeach
          </select>
          <div class="invalid-feedback" style="display: inline;">{{ $errors->first('class_room') }}</div> 
    </div>

    <div class="form-group col-md-8 col-sm-6">
      <label for="lesson_id">{{ trans('label.for.-L') }}</label>
          <select id="lesson_id" 
              name="lesson_id"
              class="select-control {{ !$errors->first('lesson_id') ?: 'is-invalid' }}"
              title="Pilih..."
              data-live-search="true" style="{{ $errors->has('lesson_id') ? 'border: 1px solid red;' : '' }}"
              {{-- required
              oninvalid="this.setCustomValidity('{{ trans('validation.custom.lesson_id.required') }}')"
              onchange="this.setCustomValidity('')" --}}>
            @foreach ($lessons as $lesson)
                  @if (isset($material) && (is_equal($material->lesson_id, $lesson->id)) )
                      <option value="{{ $lesson->id }}" data-subtext=" ({{ $lesson->curriculums->nama_kurikulum }})" selected>{{ $lesson->nama_matapelajaran }}</option>
                  @else
                      <option value="{{ $lesson->id }}" data-subtext=" ({{ $lesson->curriculums->nama_kurikulum }})" {{ is_equal((int) old('lesson_id'), $lesson->id) ? "selected" : '' }}>{{ $lesson->nama_matapelajaran }}</option>
                  @endif
            @endforeach
          </select>
          <div class="invalid-feedback" style="display: inline;">{{ $errors->first('lesson_id') }}</div> 
      </div>
    </div>


    <div class="form-group">
		<label for="description">{{ trans('label.for.-D') }} <small>(optional)</small> </label>
        <textarea id="description" 
                  name="description" 
                  class="form-control {{ !$errors->first('description') ?: 'is-invalid' }}" 
                  rows="5" 
                  title="{{ trans('label.for.info') }}" 
                  data-toggle="tooltip">{{ isset($material) ? $material->description : (old('description') ? old('description') : "") }}</textarea>
        @if ($errors->has('description')) <div class="invalid-feedback">{{ $errors->first("description") }}</div>@endif
	</div>

	<div class="form-group">
    <div class="custom-file">
      <input id="file_path"
             name="file_path" 
             type="file" 
             class="custom-file-input {{ !$errors->first('file_path') ?: 'is-invalid' }}"  
             value="" />
        <div class="invalid-feedback" style="display: inline;">{{ $errors->first('file_path') }}</div> 
        <label class="custom-file-label" for="file_path">{{ isset($material) ? $material->file_name : trans('label.file.!q') }}</label>
     
    </div>
  </div>

	<hr/>
  	<div class="form-action">
      <a class="btn btn-outline-secondary btn-round" href="{{ route('material') }}" title="{{ trans('label.back_to', ['what' => "Materi"]) }}" data-toggle="tooltip">{{ trans('label.btn.cn') }}</a>
    	<button class="btn btn-primary btn-round" type="submit">{{ !isset($material) ? trans('label.btn.sv') : trans('label.btn.ud') }}</button>
  	</div>
</form>