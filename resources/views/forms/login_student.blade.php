<form action="{{ route('student.auth') }}" method="POST" autocomplete="off">
{{ csrf_field() }}
    <div class="form-group empw">
        <input name="nisn" 
               placeholder="{{ trans('label.placeholder.nisn') }}"
               type="text"
               class="form-control top"  
        />
        <input name="password" 
               placeholder="{{ trans('label.placeholder.pass') }}"
               type="password"
               class="form-control bot"
        />
        <div class="invalid-feedback" style="display: inline;"></div> 
        <a class="form-text forgot-pw" href="#" title="Forgot password">{{ trans('label.forget') }}</a>
    </div>
    <div class="form-action">
        <div class="checkbox mb-3">
            <input type="checkbox" id="remember_token1" name="remember_token"/>
            <label for="remember_token1">{{ trans('label.remember_me') }}</label>
        </div>
        <button class="btn btn-primary btn-block" type="submit">{{ trans('label.btn.lg') }}</button>
    </div>
</form>