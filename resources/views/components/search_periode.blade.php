<div class="row justify-content-between mb-4">
    <form class="col-6" action="#" method="GET" autocomplete="off">
        <div class="form-row search">
            <div class="col-3">
                <select class="select-control" name="semester" title="Semester">
                    @foreach (semester() as $sm)
                        <option value="{{ $sm->id }}">
                            {{ preg_replace('/Semester (I|II) [(]|[)]/', null, $sm->name) }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="col-4">
                <input name="periode" type="text" class="form-control" placeholder="Tahun ajaran" {{-- value="{{ session('periode') }}" --}} title="Tahun ajaran">
                <button class="btn btn-link fa fa-search" type="submit"></button>
            </div>
        </div>
    </form>
    {{-- <div class="col-md-2 col-sm-4">
        <div class="filter">
            <form>
                <select class="select-control" name="select" title="Semester">
                    <option value="1">Semester 1</option>
                    <option value="2">Semester 2</option>
                </select>
            </form>
        </div>
    </div>
    <div class="col-md-4 col-sm-5">
        <div class="search">
            <form>
                <input class="form-control" type="text" placeholder="Tahun ajaran {{ school_year()->year }}"/>
                <button class="btn btn-link fa fa-search" type="submit"></button>
            </form>
        </div>
    </div> --}}
</div>

{{-- <form action="#" method="GET" class="form-inline" autocomplete="off">
                <input id="in-periode" 
                       class="form-control mr-sm-2" 
                       type="text"
                       name="periode" 
                       placeholder="Tahun ajaran " 
                       aria-label="Search">
            </form> --}}