@extends("dashboard.student.home")

@push("css")
<link rel="stylesheet" href="{{ asset('web/customs/css/option-hover.css') }}">
@endpush

@section("content")
<div class="content-header">
	<h2 class="title">{{ trans('label.index_page_of', ['what' => "Materi"]) }}</h2>
    <div class="right-content">
    </div>
    @component('components.search_periode')@endcomponent
</div>

<div class="content-body">
	<div class="table-responsive-md">
		<table id="material" class="table table-custom">
			<thead>
                <th width="1%">{{ trans('label.for.numb') }}</th>
                <th width="25%">{{ trans('label.for.-L') }}</th>
                <th width="30%">{{ trans('label.for.-T') }}</th>
                <th >{{ trans('label.for.by') }}</th>
                <th width="1%"></th>
            </thead>

            <tbody>
                @empty ($datas->toArray())
                    <tr id="dlRow">
                        <td colspan="6" align="center"><b>{{ trans('label.empty') }}</b></td>
                    </tr>
                @else
                    @foreach ($datas as $material)
                    <tr id="dlRow">
                        <td align="center">{{ $loop->iteration }}</td>
                        <td>{{ $material->lessons->nama_matapelajaran }}</td>
                        <td align="center">{{ $material->title }}</td>
                        <td align="center">{{ $material->teachers->employees['nama_pegawai'] }}</td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary btn-sm" type="button" data-toggle="dropdown">{{ trans('label.for.-O') }}</button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item dtl" href="{{ route('student.material.detail', $material->id) }}">{{ trans('label.act.detail') }}</a>
                                    @if (explode('.', $material->file_path)[1] === "pdf")
                                    <a class="dropdown-item" href="{{ route('student.material.read', $material->id) }}" target="_blank">{{ trans('label.act.reading') }}</a>
                                    @endif
                                    <a class="dropdown-item" href="{{ asset($material->file_path) }}" download="{{ $material->title . '.' .explode('.', $material->file_path)[1] }}">{{ trans('label.act.download') }}</a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                @endempty
            </tbody>
		</table>
	</div>
</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/DataTable.js') }}" ></script>
<script>
    $(document).ready(function () {

        @if (!empty($datas->toArray()))
            new TableController("#material");
        @endif
    })
</script>
@endpush