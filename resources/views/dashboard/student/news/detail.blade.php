@extends("dashboard.student.home")

@push("css")
@endpush

@section("content")
<div class="content-header">
    <div>
        <a href="{{ route('student.news') }}"> 
            <div class="fa fa-angle-double-left mr-2"></div>
            {{ trans('label.index_page_of', ['what' => "Berita"]) }}
        </a>
    </div>

    <h2 class="title">
		Detail Berita {{ $news->title }}
    </h2>
	@if (session("update"))
	    <div class="right-content" id="session">
	    	<span>
	    		<div class="alert alert-success alert-dismissible fade show" role="alert">
	    			<span class="lead">Success!</span>
	    			{{ session('update') }}
	                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
	                	<span aria-hidden="true">×</span>
	                </button>
	            </div>
	    	</span>
	    </div>
	@endif
</div>
{{-- {{ dd($news->description) }} --}}
<div class="content-body">
	<div class="container">
		<div class="section-mb-5">
            <dl class="row">
            	<dd class="col-sm-12 mb-5">
            		@php
            			echo $news->description;
            		@endphp
            	</dd>
            </dl>
        </div>
	</div>
</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/SessionMessages.js') }}"></script>
<script>
	$(document).ready(function () {
		
		$("#editor").summernote('code');
		$("#editor").summernote('destroy');
	});
</script>
@endpush