<div class="wrap">
    <div class="section">
        <div class="content-wrap">
        	@if (Route::is("student.home"))
        		@include(dashboard("dashboard", "student"))
        	@else
				@yield("content")
        	@endif
        </div>
    </div>
    <div class="mask"></div>
</div>

@push('javascript')
    @if (Route::is('student.home'))
        <script src="{{ asset('web/customs/js/Clock.js') }}"></script>
        <script>
            $(document).ready(function () {
                clock("p#clock"); 

                var quizToday = {{ count($quizs['today']) }};
                 for (let i = 1; i <= quizToday; i++) {
            
                    let liStatus = $(`li#status${i}`).find(`span#${i}`);
                    let spanStart = $(`span#start${i}`),
                        spanEnd = $(`span#end${i}`);

                    let quiz = {
                        start: new Date(spanStart.text()),
                        end: new Date(spanEnd.text())
                    };
                        
                    let begin = setInterval(function() {
                        
                        let date = new Date();

                        if (date.getTime() >= quiz.start.getTime() && date.getTime() <= quiz.end.getTime()) {

                            liStatus.text("sedang dilaksanakan");

                            if (date.getSeconds() % 2 === 0) {
                                liStatus.css("background-color", "#ff3f34");
                            }
                            else
                                liStatus.css("background-color", "#ffa801");
                        }
                        else if (date.getTime() > quiz.end.getTime()) {
                            
                            liStatus.text("sudah dilaksanakan").css("background-color", "#18a566");

                            setTimeout(function() {
                                
                                clearInterval(begin)
                            }, 0)
                        }
                    }, 1000);
                }
            });
        </script>
    @endif
@endpush