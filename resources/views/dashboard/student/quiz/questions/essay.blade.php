<div id="esy" class="mt-3">
	<nav aria-label="breadcrumb">
        <ol class="breadcrumb">Essay.</ol>
    </nav>
	@foreach ($questions['essay'] as $key => $question)
		@continue(null($question['no']))
		<div class="form-group">
			<div class="row">
				<div class="col-md-7">
					<label for="question">
						{{ $loop->iteration }}. {{ $question['exercise'] }}
					</label>
				
					<textarea name="essay[exam_{{ $question['no'] }}]" class="form-control" id="" rows="5">@isset ($stdQuiz){{ $stdQuiz->quizs[0]->answers['esy'][$key]['answers'] }}@endisset</textarea>
					@isset ($stdQuiz)
					    <input name="essay_scores[no{{ $key }}]" type="text" class="form-control" placeholder="Masukan Nilai">
					@endisset
				</div>

				<div class="col-md-5">
					<img src="data:image/jpeg;base64,{{ $question['img'] }}" alt="" style="width: 100%; height: auto;">
				</div>
			</div>
		</div>
		<hr />
	@endforeach
</div>