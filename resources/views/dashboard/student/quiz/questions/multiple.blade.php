<div id="mtp" class="mt-3">
	<nav aria-label="breadcrumb">
        <ol class="breadcrumb">Pilihan Ganda.</ol>
    </nav>
@foreach ($questions['multiple'] as $key => $question)
	@php
		$option = array_keys($question['option']);
		$opt = [
			'A' => $option[0],
			'B' => $option[1],
			'C' => $option[2],
			'D' => $option[3] 
		];
	@endphp
	<div class="form-group">
		<div class="row">
			<div class="col-md-7">
				<label for="question">
					<b>[{{ $loop->iteration }}].</b> {{ $question['exercise'] }}
				</label>
				<div class="row">
					<div class="col-md-6">
						<div class="radio">
							<input type="radio"
								   id="option_a_{{ $question['no'] }}"
								   name="multiple[exam_{{ $question['no'] }}]"
								   value="{{ $opt['A'] }}"
								   @isset ($stdQuiz)
								       @if (array_key_exists($loop->iteration, $stdQuiz->quizs[0]->answers['mtp']))
								       		@if ($stdQuiz->quizs[0]->answers['mtp'][$loop->iteration]['answers'] === $opt['A'])
												checked
								       		@endif
								       @endif
								       disabled
								   @endisset
								  />
							<label style="width: 200px;" for="option_a_{{ $question['no'] }}">A. {{ $question['option']['A'] }}</label>
						</div>

						<div class="radio">
							<input type="radio"
								   id="option_b_{{ $question['no'] }}"
								   name="multiple[exam_{{ $question['no'] }}]"
								   value="{{ $opt['B'] }}" 
								   @isset ($stdQuiz)
								       @if (array_key_exists($loop->iteration, $stdQuiz->quizs[0]->answers['mtp']))
								       		@if ($stdQuiz->quizs[0]->answers['mtp'][$loop->iteration]['answers'] === $opt['B'])
												checked
								       		@endif
								       @endif
								       disabled
								   @endisset/>
							<label style="width: 200px;" for="option_b_{{ $question['no'] }}">B. {{ $question['option']['B'] }}</label>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="radio">
							<input type="radio"
								   id="option_c_{{ $question['no'] }}"
								   name="multiple[exam_{{ $question['no'] }}]"
								   value="{{ $opt['C'] }}" 
								   @isset ($stdQuiz)
								       @if (array_key_exists($loop->iteration, $stdQuiz->quizs[0]->answers['mtp']))
								       		@if ($stdQuiz->quizs[0]->answers['mtp'][$loop->iteration]['answers'] === $opt['C'])
												checked
								       		@endif
								       @endif
								       disabled
								   @endisset/>
							<label style="width: 200px;" for="option_c_{{ $question['no'] }}">C. {{ $question['option']['C'] }}</label>
						</div>

						<div class="radio">
							<input type="radio"
								   id="option_d_{{ $question['no'] }}"
								   name="multiple[exam_{{ $question['no'] }}]"
								   value="{{ $opt['D'] }}" 
								   @isset ($stdQuiz)
								       @if (array_key_exists($loop->iteration, $stdQuiz->quizs[0]->answers['mtp']))
								       		@if ($stdQuiz->quizs[0]->answers['mtp'][$loop->iteration]['answers'] === $opt['D'])
												checked
								       		@endif
								       @endif
								       disabled
								   @endisset/>
							<label style="width: 200px;" for="option_d_{{ $question['no'] }}">D. {{ $question['option']['D'] }}</label>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-5">
				<img src="data:image/jpeg;base64,{{ $question['img'] }}" alt="" style="width: 100%; height: auto;">
			</div>
		</div>
		@isset ($stdQuiz)
		    <span>Kunci Jawaban : </span><span class="badge badge-success">{{ $question['key'] }}</span>
		@endisset
	</div>
	<hr />
@endforeach
</div>