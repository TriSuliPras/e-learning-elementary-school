@foreach ($questions['multiple'] as $key => $question)
	<div id="mtp{{ $loop->iteration }}" class="form-group">
		<label  for="question">
			{{ $loop->iteration }}. {{ $question['exercise'] }}
		</label>
		<div class="row col-md-8">
			<div class="col-md-4">
				<div class="radio">
					<input type="radio"
						   id="option_a_{{ $question['no'] }}"
						   name="multiple[exam_{{ $question['no'] }}]"
						   value="{{ array_keys($questions['multiple'][$key])[3] }}">
					<label style="width: 200px;" for="option_a_{{ $question['no'] }}">A. {{ $question['option_a'] }}</label>
				</div>

				<div class="radio">
					<input type="radio"
						   id="option_b_{{ $question['no'] }}"
						   name="multiple[exam_{{ $question['no'] }}]"
						   value="{{ array_keys($questions['multiple'][$key])[4] }}">
					<label style="width: 200px;" for="option_b_{{ $question['no'] }}">B. {{ $question['option_b'] }}</label>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="radio">
					<input type="radio"
						   id="option_c_{{ $question['no'] }}"
						   name="multiple[exam_{{ $question['no'] }}]"
						   value="{{ array_keys($questions['multiple'][$key])[5] }}">
					<label style="width: 200px;" for="option_c_{{ $question['no'] }}">C. {{ $question['option_c'] }}</label>
				</div>

				<div class="radio">
					<input type="radio"
						   id="option_d_{{ $question['no'] }}"
						   name="multiple[exam_{{ $question['no'] }}]"
						   value="{{ array_keys($questions['multiple'][$key])[6] }}">
					<label style="width: 200px;" for="option_d_{{ $question['no'] }}">D. {{ $question['option_d'] }}</label>
				</div>
			</div>
		</div>
	</div>
	<hr />
@endforeach

@if (! empty($questions['essay']))
	@foreach ($questions['essay'] as $key => $question)
		<div class="form-group">
			<label for="question">
				{{ $loop->iteration }}. {{ $question['exercise'] }}
			</label>
			<div class="row col-md-12">
				<textarea name="essay[exam_{{ $question['no'] }}]" class="form-control" id="" rows="5"></textarea>
			</div>
		</div>
	@endforeach
@endif