@extends("dashboard.student.home")

@push("css")
@endpush

@section("content")
<div class="content-header">
    <div>
        <a href="{{ route('student.quiz') }}"> 
            <div class="fa fa-angle-double-left mr-2"></div>
            {{ trans('label.index_page_of', ['what' => "Kuis"]) }}
        </a>
    </div>

    <h2 class="title">
    	{!! trans('label.detail', ['for' => "Kuis", 'what' => $quiz->lessons->nama_matapelajaran]) !!}
    </h2>
	@if (session("success"))
	    <div class="right-content" id="session">
	    	<span>
	    		<div class="alert alert-success alert-dismissible fade show" role="alert">
	    			<span class="lead">{{ trans('confirmation.success') }}</span>
	    			{{ session('success') }}
	                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
	                	<span aria-hidden="true">×</span>
	                </button>
	            </div>
	    	</span>
	    </div>
	@endif
</div>

<div class="content-body">
	<div class="row">
		<div class="col-md-12">
			<dl id="dlRow" class="row">
				<dt class="col-4"><h4>{{ trans('label.for.-T') }}</h4></dt>
				<dd class="col-8">{{ $quiz->title }}</dd>
				@if (!empty($quiz->students->toArray()))
				<dt class="col-4"><h4>{{ trans('label.quiz_count') }}</h4></dt>
				<dd class="col-8">
                    @if (not_null($quiz->weight_multiple))
                        <span title="Bobot Soal {{ $quiz->weight_multiple }} %" data-toggle="tooltip">Pilihan Ganda {{ $questions['multiple'] }}</span>
                    @endif

                    @if (not_null($quiz->weight_multiple) && not_null($quiz->weight_essay))
                        &nbsp;
                        <b>||</b>
                        &nbsp;
                    @endif
                    @if (not_null($quiz->weight_essay))
                        <span title="Bobot Soal {{ $quiz->weight_essay }} %" data-toggle="tooltip">Essay {{ $questions['essay'] }}</span>
                    @endif
                </dd>
				@endif
				<dt class="col-4"><h4>{{ trans('label.for.-C') }}</h4></dt>
				<dd class="col-8">{{ "{$quiz->class_room} {$quiz->pararel}" }}</dd>
				<dt class="col-4"><h4>{{ trans('label.for.-D') }}</h4></dt>
				<dd class="col-8">{{ $quiz->description }}</dd>
				<dt class="col-4"><h4>{{ trans('label.for.-s') }}</h4></dt>
				<dd id="status" class="col-8">
					@if ($quiz->status === "true")
                        <span class="badge badge-success">{{ trans('label.quiz_status', ['is' => "Sudah"]) }}</span>
                    @else
                        <span class="badge badge-info">{{ trans('label.quiz_status', ['is' => "Belum"]) }}</span>
                    @endif

                    <span id="start" hidden>{{ \Carbon\Carbon::parse($quiz->starting_time)->toDateTimeString() }}</span>
                    <span id="end" hidden>{{ \Carbon\Carbon::parse($quiz->starting_time)->addMinutes($quiz->count_down)->toDateTimeString() }}</span>
				</dd>

				<dt class="col-4"><h4>{{ trans('label.quiz_start') }}</h4></dt>
				<dd class="col-8" id="datecounter">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $quiz->starting_time, "Asia/Jakarta")->diffForHumans() }}</dd>

				<dt class="col-4"><h4>{{ trans('label.for.-c') }}</h4></dt>
				<dd class="col-8">{{ $quiz->count_down }} Menit</dd>

				<dt class="col-4 timer" style="display: none;"><h4>{{ trans('label.time_run') }}</h4></dt>
				<dd id="timer" class="col-8 timer" style="display: none;"></dd>

				<dt class="col-4"><h4>{{ trans('label.for.-O') }}</h4></dt>
				<dd class="col-8">
					<a class="badge badge-success doQuiz" data-toggle="tooltip" title="Belum dilaksanakan" style="display: none;">{{ trans('label.do') }} </a>
					<a data-toggle="tooltip" title="Belum menyelesaikan kuis" class="score" ><span class="badge badge-info">{{ trans('label.score') }}</span></a>
				</dd>
			</dl>
		
			<hr />
		</div>
	</div>
</div>
@endsection

@section("modal")
	<div class="modal fade" id="{{ 'score'.$quiz->id }}" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">{{ trans('label.score') }} {{ $quiz->title }}</h5>
	                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
	                    <img src="{{ asset('web/images/icon-close.png') }}"/>
	                </button>
	            </div>
	            <div class="modal-body">
	                <dl id="dlRow" class="row">
	                	@forelse ($quiz->students as $student)
	                		@if (not_null($quiz->weight_multiple))
	                			<dt class="col-8"><h4>{{ trans('label.multi') }}</h4></dt>
								<dd class="col-4">{{ $student->multiple_scores }}</dd>
							@endif

							@if (not_null($quiz->weight_essay))
								@php
									$essay = !empty($student->essay_scores) ? $student->essay_scores : (not_null($student->essay_scores) ? $student->essay_scores : trans("label.!correction"));
								@endphp
								<dt class="col-8"><h4>{{ trans('label.essay') }}</h4></dt>
								<dd class="col-4">{{ $essay }}</dd>
							@endif

							<dt class="col-8"><h4>{{ trans('label.score').' '.trans('label.end') }}</h4></dt>
							<dd class="col-4">
								@if (null($quiz->weight_multiple))
									{{ $essay }}
								@elseif (null($quiz->weight_essay))
									 {{ $student->multiple_scores }}
								@else
									{{ $student->final_scores }}
								@endif
							</dd>
						@empty
							<dt class="col-8"><h4>{{ trans('label.multi') }}</h4></dt>
							<dd class="col-4"><b>{{ trans('label.empty') }}</b></dd>

							<dt class="col-8"><h4>{{ trans('label.essay') }}</h4></dt>
							<dd class="col-4"><b>{{ trans('label.empty') }}</b></dd>

							<dt class="col-8"><h4>{{ trans('label.score').' '.trans('label.end') }}</h4></dt>
							<dd class="col-4"><b>{{ trans('label.empty') }}</b></dd>
	                	@endforelse
	                </dl>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="modal fade" id="{{ 'show'.$quiz->id }}" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">{{ $quiz->title }}</h5>
	                <button class="close" type="button" data-dismiss="modal" aria-label="Close" title="Batal">
	                    <img src="{{ asset('web/images/icon-close.png') }}"/>
	                </button>
	            </div>
	            <div class="modal-body">
	                <ul>
	                	@if (not_null($quiz->weight_multiple))
	                		<li title="Bobot Soal {{ $quiz->weight_multiple }} %" data-toggle="tooltip">
	                			{{ trans('label.multi') }} 
	                			{{ $questions['multiple'] }} 
	                			{{ trans('label.act.exercise') }}
	                		</li>
	                	@endif

	                	@if (not_null($quiz->weight_essay))
	                		<li title="Bobot Soal {{ $quiz->weight_essay }} %" data-toggle="tooltip">
	                			{{ trans('label.essay') }}
	                			{{ $questions['essay'] }}
	                			{{ trans('label.act.exercise') }}
	                		</li>
	                	@endif
	                	<li>{{ trans('label.quiz_starting', ['in' => $quiz->count_down]) }}</li>
	                </ul>
	            </div>
	            <div class="modal-footer">
	                <a class="btn mb-3 btn-success btn-round" href="{{ route('student.quiz.create', $quiz->id) }}">
	                	{{ trans('label.btn.st') }}
	                </a>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@push("javascript")
<script>
	$(document).ready(function() {
		var quizDate = new Date("{{ \Carbon\Carbon::parse($quiz->starting_time) }}").getTime();
		var timer = "{{ \Carbon\Carbon::parse($quiz->starting_time)->addMinutes($quiz->count_down)->format('d M Y H:i') }}";
        var dlStatus = $("dl#dlRow").find("dd#status");
        var spanStart = dlStatus.find("#start"),
            spanEnd = dlStatus.find("#end");
        var onStart = false;

        var quiz = {
            start: new Date(spanStart.text()),
            end: new Date(spanEnd.text())
        };

        $(function () {
			var quizInterval = setInterval(() => {
				var oneDay = 24 * 60 * 60 * 1000;
				var firstDate = new Date(timer);
				var secondDate = new Date();
				var days = (firstDate.getTime() - secondDate.getTime()) / (oneDay);
				var hrs = (days - Math.floor(days)) * 24;
				var min = (hrs - Math.floor(hrs)) * 60;

				var sec = Math.floor((min - Math.floor(min))* 60);
				var time = `${Math.floor(hrs)} Jam` + ' ' + `${(Math.floor(min))} Menit` + ' ' + `${sec} Detik`;
				
				console.log(time);
				
				
				if (onStart) {
					$(".timer").show(500);
					$("#timer").text(time);
				}
				if (!onStart) {
					
					clearInterval(quizInterval);
					$(".timer").hide(500);
				}
				// $(".count-down").text("Waktu Habis");

				//clearInterval(quizInterval);
			}, 1000)	
		});
        
        var countDown = setInterval(function() {
            
            var thisDay = new Date();

            var distance = quizDate - thisDay.getTime();
            
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            
            if (days > 0) {
                
                getId("datecounter").innerHTML = days + " hari dari sekarang";
            }
            else if (hours > 0) {
                
                getId("datecounter").innerHTML = hours + " jam dari sekarang";
            }
            else if (minutes > 0) {
                
                getId("datecounter").innerHTML = minutes + " menit dari sekarang";
            }
            else if (seconds > 0) {
                
                getId("datecounter").innerHTML = seconds + " detik dari sekarang";
            }
            else {

                if (days * -1 <= 1) {
                    
                    minutes *= -1;
                    hours *= -1;

                    if (hours > 1) {
                        
                        getId("datecounter").innerHTML = hours + " jam yang lalu";
                    }
                    else
                        getId("datecounter").innerHTML = minutes - 1 + " menit yang lalu";
                }
            }

			
            if (thisDay.getTime() >= quiz.start.getTime() && thisDay.getTime() <= quiz.end.getTime()) {
				
				onStart = true;
				
				@if (empty($quiz->students->toArray()))
					$("a.doQuiz").attr({
						href: "{{ '#show'.$quiz->id }}",
						'data-toggle': "modal",
					});

					$("a.doQuiz").removeAttr("data-original-title");
					$("a.doQuiz").show(500);
				@else
					$("a.score").removeAttr("data-original-title");
	                $("a.score").attr({
	                	href: "{{ '#score'.$quiz->id }}",
						'data-toggle': "modal",
	                });
				@endif

                dlStatus.find("span.badge").text('{{ trans('label.quiz_status', ['is' => "Sedang"]) }}');

                if (thisDay.getSeconds() % 2 === 0) {
                    dlStatus.find("span.badge").css("background-color", "#ff3f34");
                }
                else
                    dlStatus.find("span.badge").css("background-color", "#ffa801");
            }
            else if (thisDay.getTime() > quiz.end.getTime()) {
            	onStart = false;
                $("a.doQuiz").hide(500);
                $("a.score").removeAttr("data-original-title");
                $("a.score").attr({
                	href: "{{ '#score'.$quiz->id }}",
					'data-toggle': "modal",
                });

                dlStatus.find("span.badge").text('{{ trans('label.quiz_status', ['is' => "Sudah"]) }}').css("background-color", "#18a566");
            }
        }, 1000);
	});
</script>
@endpush