<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="UTF-8"/>
	    {{-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	    <meta name="viewport" content="width=device-width, initial-scale=1"/> --}}
	    <link rel="icon" type="image/png" href="{{ asset('web/images/logo.png') }}">
	    <title>{{ trans('label.app_title') }}</title>
	    <meta name="description" content=""/>
	    <link rel="stylesheet" href="{{ asset('web/styles/plugins.css') }}"/>
	    <link rel="stylesheet" href="{{ asset('web/styles/main.css') }}"/>
	    @stack('css')
	</head>
<body class="projects">
	<div class="wrap">
		<header>
			@include(dashboard("includes", "header"))
		</header>

		<nav class="main-menu">
			@include(dashboard("includes", "navigation"))
		</nav>

		<main>
			@include(dashboard("includes", "main"))
		</main>

		<footer>
        	<div class="content-wrap">
        		{{ trans('label.copyright') }}
        	</div>
      </footer>
	</div>
</body>

@yield("modal")

<script src="{{ asset('web/plugins/jquery-3.3.1/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('web/plugins/data-tables/DataTables-1.10.18/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('web/plugins/data-tables/DataTables-1.10.18/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('web/plugins/bootstrap-4/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('web/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('web/plugins/summer_note/dist/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('web/scripts/main.js') }}"></script>
@stack('javascript')
</html>