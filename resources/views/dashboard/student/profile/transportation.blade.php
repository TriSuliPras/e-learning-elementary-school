<div id="transportation" class="tab-pane fade show" role="tabpanel" aria-labelledby="transportation-tab">
<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.how_far_to_from_school', ['-n' => null]) }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="jarak_rumah_kesekolah"
			   type="text"
			   class="form-control" 
			   value="{{ old('jarak_rumah_kesekolah') ? old('jarak_rumah_kesekolah') : $student->jarak_rumah_kesekolah }}"/>
		@else
			{{ $student->jarak_rumah_kesekolah }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.how_far_to_from_school', ['-n' => "(Km)"]) }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input id="jarak_rumah_kesekolah_km" 
			   name="jarak_rumah_kesekolah_km {{ $errors->has('jarak_rumah_kesekolah_km') ? 'is-invalid' : '' }}"
			   type="number"
			   class="form-control" 
			   title="{{ trans('label.must_number') }}"
			   data-toggle="tooltip"
			   value="{{ old('jarak_rumah_kesekolah_km') ? old('jarak_rumah_kesekolah_km') : $student->jarak_rumah_kesekolah_km }}"/>
		@if ($errors->has('jarak_rumah_kesekolah_km'))<div class="invalid-feedback">{{ trans('validation.must_a_number') }}</div>@endif
		@else
			{{ $student->jarak_rumah_kesekolah_km }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.trans_to_use') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="alat_transportasi"
			   type="text"
			   class="form-control" 
			   value="{{ old('alat_transportasi') ? old('alat_transportasi') : $student->alat_transportasi }}"/>
		@else
			{{ $student->alat_transportasi }}
		@endif
	</dd>
</dl>
</div>