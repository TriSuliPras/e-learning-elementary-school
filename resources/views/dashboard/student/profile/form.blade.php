<form action="{{ route('student.update', $student->id) }}" method="POST" enctype="multipart/form-data" autocomplete="off">
	{{ method_field("PATCH") }} {{ csrf_field() }}
	<div class="row">
		<div class="col-md-8">
			<dl class="row">
				<dt class="col-4"><h4>NISN</h4></dt>
				<dd class="col-8">
					<input name="nisn"
						   type="text"
						   class="form-control"
						   value="{{ old('nisn') ? old('nisn') : $student->nisn }}" 
						   readonly />
				</dd>

				<dt class="col-4"><h4>NIS</h4></dt>
				<dd class="col-8">
					<input name="nis"
						   type="text"
						   class="form-control"
						   value="{{ old('nis') ? old('nis') : $student->nis }}" 
						   readonly 
					>
				</dd>

				<dt class="col-4"><h4>NIK</h4></dt>
				<dd class="col-8">
					<input name="nik"
						   type="text"
						   class="form-control"
						   value="{{ old('nik') ? old('nik') : $student->nik }}" 
						   readonly 
					/>
				</dd>

				<dt class="col-4"><h4>Nama</h4></dt>
				<dd class="col-8">
					<input name="namsiswa"
						   type="text"
						   class="form-control"
						   value="{{ old('nama_siswa') ? old('nama_siswa') : $student->nama_siswa }}" 
						   readonly 
					/>
				</dd>

				<dt class="col-4"><h4>Kelas</h4></dt>
				<dd class="col-8">
					<select namesiswa" class="select-control" title="Pilih Kelas" disabled>
						<option value="1" {{ $student->kelas == 1 ? 'selected' : '' }}>1</option>
						<option value="2" {{ $student->kelas == 2 ? 'selected' : '' }}>2</option>
						<option value="3" {{ $student->kelas == 3 ? 'selected' : '' }}>3</option>
						<option value="4" {{ $student->kelas == 4 ? 'selected' : '' }}>4</option>
						<option value="5" {{ $student->kelas == 5 ? 'selected' : '' }}>5</option>
						<option value="6" {{ $student->kelas == 6 ? 'selected' : '' }}>6</option>
					</select>
				</dd>

				<dt class="col-4"><h4>Status</h4></dt>
				<dd class="col-8">
					<input name="status"
						   type="text"
						   class="form-control"
						   value="{{ old('status') ? old('status') : $student->status }}" 
						   disabled />
				</dd>
				<dt class="col-4"><h4>Foto</h4></dt>
				<dd class="col-8">
				    <div class="custom-file">
				      <input name="foto_siswa" 
				           type="file" 
				           class="custom-file-input {{ !$errors->first('foto_siswa') ?: 'is-invalid' }}" 
				           id="foto_siswa" 
				           value="{{ old('foto_siswa') ? old('foto_siswa') : '' }}" />
				          <div class="invalid-feedback" style="display: inline;">{{ $errors->first('foto_siswa') }}</div> 
				        <label class="custom-file-label" for="foto_siswa" >{{ isset($student) ? $student->foto_siswa : '' }}</label>
				    </div>
				</dd>
			</dl>

			<div class="mb-3">
				<ul class="nav nav-tabs" id="employee-tab" role="tablist">
					<li class="nav-item">
						<a id="biodata-tab" href="#biodata" role="tab" aria-controls="biodata" aria-selected="true" class="nav-link active" data-toggle="tab">Biodata</a>
					</li>

					<li class="nav-item">
						<a id="transportation-tab" href="#transportation" role="tab" aria-controls="transportation" aria-selected="false" class="nav-link" data-toggle="tab">Transportasi</a>
					</li>

					<li class="nav-item">
						<a id="education-tab" href="#education" role="tab" aria-controls="education" aria-selected="false" class="nav-link" data-toggle="tab">Pendidikan</a>
					</li>

					<li class="nav-item">
						<a id="family-tab" href="#family" role="tab" aria-controls="family" aria-selected="false" class="nav-link" data-toggle="tab">Keluarga</a>
					</li>
				</ul>
			</div>

			<div id="employee-tab-content" class="tab-content">
				@include("dashboard.student.profile.biodata")

				@include("dashboard.student.profile.transportation")	

				@include("dashboard.student.profile.education")

				@include("dashboard.student.profile.family")		

				@include("dashboard.student.profile.address")				
			</div>
			
			<br />
			<div class="row">
				<div class="form-action">
					<a class="btn btn-outline-secondary btn-round" href="{{ route('student.profile', $student->id) }}" title="{{ trans('label.back_to', ['what' => "Profil"]) }}" data-toggle="tooltip">{{ trans('label.btn.cn') }}</a>
                	<button class="btn btn-success btn-round" type="submit">{{ !isset($student) ? trans('label.btn.sv') : trans('label.btn.ud') }}</button>
              	</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="row">
				<div>
					<div id="img-profil"></div>
				</div>
			</div>
		</div>
	</div>
</form>