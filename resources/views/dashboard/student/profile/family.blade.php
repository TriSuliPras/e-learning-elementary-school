<div id="family" class="tab-pane fade show" role="tabpanel" aria-labelledby="family-tab">
<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents', ['parent' => "Ayah"]) }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="nama_ayah"
			   type="text"
			   class="form-control {{ $errors->has('nama_ayah') ? 'is-invalid' : '' }}"
			   value="{{ old('nama_ayah') ? old('nama_ayah') : $student->nama_ayah }}"/>
		@if ($errors->has('nama_ayah'))<div class="invalid-feedback">{{ $errors->first('nama_ayah') }}</div>@endif
		@else
			{{ $student->nama_ayah }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents_born') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="tahunlahir_ayah"
			   type="number"
			   title="{{ trans('label.must_number') }}"
			   data-toggle="tooltip" 
			   class="form-control {{ $errors->has('tahunlahir_ayah') ? 'is-invalid' : '' }}" 
			   value="{{ old('tahunlahir_ayah') ? old('tahunlahir_ayah') : $student->tahunlahir_ayah }}"/>
		@if ($errors->has('tahunlahir_ayah'))<div class="invalid-feedback">{{ $errors->first('tahunlahir_ayah') }}</div>@endif
		@else
			{{ $student->tahunlahir_ayah }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents_job', ['parent' => "Ayah"]) }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="pekerjaan_ayah"
			   type="text"
			   class="form-control {{ $errors->has('pekerjaan_ayah') ? 'is-invalid' : '' }}" 
			   value="{{ old('pekerjaan_ayah') ? old('pekerjaan_ayah') : $student->pekerjaan_ayah }}"/>
		@if ($errors->has('pekerjaan_ayah'))<div class="invalid-feedback">{{ $errors->first('pekerjaan_ayah') }}</div>@endif
		@else
			{{ $student->pekerjaan_ayah }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents_education', ['parent' => "Ayah"]) }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="pendidikan_ayah"
			   type="text"
			   class="form-control {{ $errors->has('pendidikan_ayah') ? 'is-invalid' : '' }}" 
			   value="{{ old('pendidikan_ayah') ? old('pendidikan_ayah') : $student->pendidikan_ayah }}"/>
		@if ($errors->has('pendidikan_ayah'))<div class="invalid-feedback">{{ $errors->first('pendidikan_ayah') }}</div>@endif
		@else
			{{ $student->pendidikan_ayah }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents_income', ['parent' => "Ayah"]) }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="penghasilan_ayah"
			   type="text"
			   class="form-control {{ $errors->has('penghasilan_ayah') ? 'is-invalid' : '' }}" 
			   value="{{ old('penghasilan_ayah') ? old('penghasilan_ayah') : $student->penghasilan_ayah }}"/>
		@if ($errors->has('penghasilan_ayah'))<div class="invalid-feedback">{{ $errors->first('penghasilan_ayah') }}</div>@endif
		@else
			{{ $student->penghasilan_ayah }}
		@endif
	</dd>
</dl>

<hr />

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents', ['parent' => "Ibu"]) }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="nama_ibu"
			   type="text"
			   class="form-control {{ $errors->has('nama_ibu') ? 'is-invalid' : '' }}" 
			   value="{{ old('nama_ibu') ? old('nama_ibu') : $student->nama_ibu }}"/>
		@if ($errors->has('nama_ibu'))<div class="invalid-feedback">{{ $errors->first('nama_ibu') }}</div>@endif
		@else
			{{ $student->nama_ibu }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents_born') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="tahunlahir_ibu" 
			   type="number" 
			   title="{{ trans('label.must_number') }}"
			   data-toggle="tooltip"
			   class="form-control {{ $errors->has('tahunlahir_ibu') ? 'is-invalid' : '' }}" 
			   value="{{ old('tahunlahir_ibu') ? old('tahunlahir_ibu') : $student->tahunlahir_ibu }}"/>
		@if ($errors->has('tahunlahir_ibu'))<div class="invalid-feedback">{{ $errors->first('tahunlahir_ibu') }}</div>@endif
		@else
			{{ $student->tahunlahir_ibu }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents_job', ['parent' => "Ibu"]) }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="pekerjaan_ibu"
			   type="text"
			   class="form-control {{ $errors->has('pekerjaan_ibu') ? 'is-invalid' : '' }}" 
			   value="{{ old('pekerjaan_ibu') ? old('pekerjaan_ibu') : $student->pekerjaan_ibu }}"/>
		@if ($errors->has('pekerjaan_ibu'))<div class="invalid-feedback">{{ $errors->first('pekerjaan_ibu') }}</div>@endif
		@else
			{{ $student->pekerjaan_ibu }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents_education', ['parent' => "Ibu"]) }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="pendidikan_ibu"
			   type="text"
			   class="form-control {{ $errors->has('pendidikan_ibu') ? 'is-invalid' : '' }}"
			   value="{{ old('pendidikan_ibu') ? old('pendidikan_ibu') : $student->pendidikan_ibu }}">
		@if ($errors->has('pendidikan_ibu'))<div class="invalid-feedback">{{ $errors->first('pendidikan_ibu') }}</div>@endif
		@else
			{{ $student->pendidikan_ibu }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents_income', ['parent' => "ibu"]) }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="penghasilan_ibu"
			   type="text"
			   class="form-control {{ $errors->has('penghasilan_ibu') ? 'is-invalid' : '' }}" 
			   value="{{ old('penghasilan_ibu') ? old('penghasilan_ibu') : $student->penghasilan_ibu }}"/>
		@if ($errors->has('penghasilan_ibu'))<div class="invalid-feedback">{{ $errors->first('penghasilan_ibu') }}</div>@endif
		@else
			{{ $student->penghasilan_ibu }}
		@endif
	</dd>
</dl>

<hr />

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents', ['parent' => "Wali"]) }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input id="nama_wali" 
			   name="nama_wali"
			   type="text"
			   class="form-control {{ $errors->has('nama_wali') ? 'is-invalid' : '' }}" 
			   value="{{ old('nama_wali') ? old('nama_wali') : ucfirst($student->nama_wali) }}"/>
		@if ($errors->has('nama_wali'))<div class="invalid-feedback">{{ $errors->first('nama_wali') }}</div>@endif
		@else
			{{ $student->nama_wali }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents_born') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input id="tahunlahir_wali" 
			   name="tahunlahir_wali"
			   type="text"
			   title="{{ trans('label.must_number') }}"
			   data-toggle="tooltip"
			   class="form-control {{ $errors->has('tahunlahir_wali') ? 'is-invalid' : '' }}" 
			   value="{{ old('tahunlahir_wali') ? old('tahunlahir_wali') : $student->tahunlahir_wali }}"/>
		@if ($errors->has('tahunlahir_wali'))<div class="invalid-feedback">{{ $errors->first('tahunlahir_wali') }}</div>@endif
		@else
			{{ $student->tahunlahir_wali }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents_job', ['parent' => "Wali"]) }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input id="pekerjaan_wali"
			   name="pekerjaan_wali"
			   type="text"
			   class="form-control {{ $errors->has('pekerjaan_wali') ? 'is-invalid' : '' }}" 
			   value="{{ old('pekerjaan_wali') ? old('pekerjaan_wali') : $student->pekerjaan_wali }}"/>
		@if ($errors->has('pekerjaan_wali'))<div class="invalid-feedback">{{ $errors->first('pekerjaan_wali') }}</div>@endif
		@else
			{{ $student->pekerjaan_wali }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents_education', ['parent' => "Wali"]) }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input id="pendidikan_wali"
		       name="pendidikan_wali"
			   type="text"
			   class="form-control {{ $errors->has('pendidikan_wali') ? 'is-invalid' : '' }}" 
			   value="{{ old('pendidikan_wali') ? old('pendidikan_wali') : $student->pendidikan_wali }}"/>
		@if ($errors->has('pendidikan_wali'))<div class="invalid-feedback">{{ $errors->first('pendidikan_wali') }}</div>@endif
		@else
			{{ $student->pendidikan_wali }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.parents_income', ['parent' => "Wali"]) }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input id="penghasilan_wali"
		       name="penghasilan_wali"
			   type="text"
			   class="form-control {{ $errors->has('penghasilan_wali') ? 'is-invalid' : '' }}" 
			   value="{{ old('penghasilan_wali') ? old('penghasilan_wali') : $student->penghasilan_wali }}"/>
		@if ($errors->has('penghasilan_wali'))<div class="invalid-feedback">{{ $errors->first('penghasilan_wali') }}</div>@endif
		@else
			{{ $student->penghasilan_wali }}
		@endif
	</dd>
</dl>
</div>