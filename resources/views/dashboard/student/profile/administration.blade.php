<div class="mb-3">
    <div class="table-responsive-md">
        <table class="table table-custom">
            <thead>
                <tr>
                    <th rowspan="2" width="1%">{{ trans('label.for.numb') }}</th>
                    <th rowspan="2" width="10%">{{ trans('label.for.-m') }}</th>
                    <th colspan="3">{{ trans('label.for.payed') }}</th>
                    <th rowspan="2" width="1%">{{ trans('label.for.-s') }}</th>
                    <th rowspan="2" width="150px">{{ trans('label.for.currency') }}</th>
                    <th rowspan="2">{{ trans('label.for.info') }}</th>
                </tr>
                <tr>
                    <th width="130px">{{ trans('label.for.-d') }}</th>
                    <th width="130px">{{ trans('label.for.-m') }}</th>
                    <th width="130px">{{ trans('label.for.-y') }}</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($months as $key => $month)
                <tr>
                    <td align="center">{{ $loop->iteration }}</td>
                    @if ($key < count($student->administrations->toArray()))
                        @if ($student->administrations[$key]->bulan_id === $month->id)
                            <td align="center">{{ $month->bulan }}</td>
                            <td align="center">{{ explode(' / ', $student->administrations[$key]->tgl_bayar)[0] }}</td>
                            <td align="center">{{ explode(' / ', $student->administrations[$key]->tgl_bayar)[1] }}</td>
                            <td align="center">{{ explode(' / ', $student->administrations[$key]->tgl_bayar)[2] }}</td>
                            <td align="center">
                                <span class="badge badge-success">{{ $student->administrations[$key]->status }}</span>
                            </td>
                            <td align="center">Rp {{ number_format($student->administrations[$key]->biaya, 2, ',', '.') }}</td>
                            <td>{{ $student->administrations[$key]->keterangan }}</td>
                        @endif
                    @else
                        <td align="center">{{ $month->bulan }}</td>
                            <td align="center">-</td>
                            <td align="center">-</td>
                            <td align="center">-</td>
                            <td align="center">
                                <span class="badge badge-danger">{{ trans('label.for.!payed_off') }}</span>
                            </td>
                            <td align="center">Rp -,00</td>
                            <td align="center">-</td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>