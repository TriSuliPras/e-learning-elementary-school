<div id="address" class="tab-pane fade show" role="tabpanel" aria-labelledby="address-tab">
	<dl class="row">
		<dt class="col-2"><h4>Tinggal Dengan</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="jenis_tinggal" type="text" class="form-control">
			@else
				{{ $student->jenis_tinggal }}
			@endif
		</dd>
	</dl>
	
	<div class="mb-3">Alamat Surabaya</div>
	<dl class="row">
		<dt class="col-2"><h4>Alamat</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="surabaya_alamat" type="text" class="form-control">
			@else
				{{ $student->surabaya_alamat }}
			@endif
		</dd>
	</dl>

	<dl class="row">
		<dt class="col-2"><h4>RT</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="surabaya_rt" type="text" class="form-control">
			@else
				{{ $student->surabaya_rt }}
			@endif
		</dd>
	</dl>

	<dl class="row">
		<dt class="col-2"><h4>RW</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="surabaya_rw" type="text" class="form-control">
			@else
				{{ $student->surabaya_rw }}
			@endif
		</dd>
	</dl>

	<dl class="row">
		<dt class="col-2"><h4>Kelurahan / Desa</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="surabaya_kel_desa" type="text" class="form-control">
			@else
				{{ $student->surabaya_kel_desa }}
			@endif
		</dd>
	</dl>

	<dl class="row">
		<dt class="col-2"><h4>Kecamatan</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="surabaya_kecamatan" type="text" class="form-control">
			@else
				{{ $student->surabaya_kecamatan }}
			@endif
		</dd>
	</dl>

	<dl class="row">
		<dt class="col-2"><h4>Kota / Kabupaten</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="surabaya_kab_kota" type="text" class="form-control">
			@else
				{{ $student->surabaya_kab_kota }}
			@endif
		</dd>
	</dl>

	<dl class="row">
		<dt class="col-2"><h4>Kode POS</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="surabaya_kodepos" type="text" class="form-control">
			@else
				{{ $student->surabaya_kodepos }}
			@endif
		</dd>
	</dl>

	<div class="mb-3">Luar Kota</div>
	<dl class="row">
		<dt class="col-2"><h4>Alamat</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="luar_alamat" type="email" class="form-control">
			@else
				{{ $student->luar_alamat }}
			@endif
		</dd>
	</dl>

	<dl class="row">
		<dt class="col-2"><h4>RT</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="luar_rt" type="text" class="form-control">
			@else
				{{ $student->luar_rt }}
			@endif
		</dd>
	</dl>

	<dl class="row">
		<dt class="col-2"><h4>RW</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="luar_rw" type="text" class="form-control">
			@else
				{{ $student->luar_rw }}
			@endif
		</dd>
	</dl>

	<dl class="row">
		<dt class="col-2"><h4>Kelurahan / Desa</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="luar_kel_desa" type="text" class="form-control">
			@else
				{{ $student->luar_kel_desa }}
			@endif
		</dd>
	</dl>

	<dl class="row">
		<dt class="col-2"><h4>Kecamatan</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="luar_kecamatan" type="text" class="form-control">
			@else
				{{ $student->luar_kecamatan }}
			@endif
		</dd>
	</dl>

	<dl class="row">
		<dt class="col-2"><h4>Kota / Kabupaten</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="luar_kab_kota" type="text" class="form-control">
			@else
				{{ $student->luar_kab_kota }}
			@endif
		</dd>
	</dl>

	<dl class="row">
		<dt class="col-2"><h4>Kode POS</h4></dt>
		<dd class="col-6">
			@if (! isset($student))
			<input name="luar_kodepos" type="text" class="form-control">
			@else
				{{ $student->luar_kodepos }}
			@endif
		</dd>
	</dl>
</div>