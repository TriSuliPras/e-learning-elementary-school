@extends("dashboard.student.home")

@push("css")
<style>
    #img-profil {
        width: 250px;
        height: 250px;
        border-radius: 10px;
        border: 1px solid black;
        margin: 0 49px;
    }

    #img-input {
        display: block;
        position: absolute;
        top: 288px;
        left: 51px;
    }
</style>
@endpush

@section("content")
<div class="content-header">
    <div>
        <a href="{{ route('student.home') }}"> 
            <div class="fa fa-angle-double-left mr-2"></div>
            {{ trans('label.dashboard') }}
        </a>
    </div>

    <h2 class="title">{{ trans('label.profile.-S') }}</h2>
    @if (session("update"))
        <div class="right-content" id="session">
            <span>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <span class="lead">{{ trans('confirmation.success') }}</span>
                    {{ session('update') }}
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </span>
        </div>
    @endif
    <div class="right-content">
        <a id="edit" class="btn btn-primary pull-right btn-add" href="{{ route('student.edit', $student->id) }}"> 
            <span>{{ trans('label.profile.-e') }}</span>
        </a>
    </div>
</div>

<div class="content-body">
    <div class="row">
        <div class="col-md-8">
            <dl class="row">
                <dt class="col-4"><h4>{{ trans('label.for.nisn') }}</h4></dt>
                <dd class="col-8">{{ $student->nisn }}</dd>
                <dt class="col-4"><h4>{{ trans('label.for.nis') }}</h4></dt>
                <dd class="col-8">{{ $student->nis }}</dd>
                <dt class="col-4"><h4>{{ trans('label.for.nik') }}</h4></dt>
                <dd class="col-8">{{ $student->nik }}</dd>
                <dt class="col-4"><h4>{{ trans('label.for.name') }}</h4></dt>
                <dd class="col-8">{{ $student->nama_siswa }}</dd>
                <dt class="col-4"><h4>{{ trans('label.for.-C') }}</h4></dt>
                <dd class="col-8">{{ "{$student->kelas} {$student->pararel}" }}</dd>
                <dt class="col-4"><h4>{{ trans('label.for.-s') }}</h4></dt>
                <dd class="col-8">{{ $student->status }}</dd>
            </dl>

            <h3 class="mb-3">
                <div class="accordion">
                    <div class="card">
                        <div id="identity">
                            <button id="collapse1" class="btn mb-3 btn-primary btn-wicon" data-toggle="collapse" data-target="#identity-data" aria-expanded="true" aria-controls="identity">
                                <span class="fa fa-vcard-o"></span>
                                {{ trans('label.for.academica') }}
                            </button>

                            <button id="collapse2" class="btn mb-3 btn-primary btn-wicon" data-toggle="collapse" data-target="#administration-data" aria-expanded="true" aria-controls="identity">
                                <span class="fa fa-money"></span>
                                {{ trans('label.for.administration') }} {{ date('Y') }}
                            </button>
                        </div>
                    </div>
                </div>
            </h3>
        </div>

        <div class="col-md-4">
			<div class="row">
				<div>
					<div id="img-profil"></div>
				</div>
			</div>
		</div>

        <div class="col-12" id="Identitas">
            <div id="identity-data" class="collapse" aria-labelledby="identity" data-parent="#accordion">
                <div class="card-body">
                    <div class="mb-3">
                <ul class="nav nav-tabs" id="employee-tab" role="tablist">
                    <li class="nav-item">
                        <a id="biodata-tab" href="#biodata" role="tab" aria-controls="biodata" aria-selected="true" class="nav-link active" data-toggle="tab">{{ trans('label.for.bio') }}</a>
                    </li>

                    <li class="nav-item">
                        <a id="transportation-tab" href="#transportation" role="tab" aria-controls="transportation" aria-selected="false" class="nav-link" data-toggle="tab">{{ trans('label.for.trans') }}</a>
                    </li>

                    <li class="nav-item">
                        <a id="education-tab" href="#education" role="tab" aria-controls="education" aria-selected="false" class="nav-link" data-toggle="tab">{{ trans('label.for.education') }}</a>
                    </li>

                    <li class="nav-item">
                        <a id="family-tab" href="#family" role="tab" aria-controls="family" aria-selected="false" class="nav-link" data-toggle="tab">{{ trans('label.for.family') }}</a>
                    </li>
                </ul>
            </div>

            <div id="employee-tab-content" class="tab-content">
                @include(dashboard("profile", "biodata"))

                @include(dashboard("profile", "transportation")) 

                @include(dashboard("profile", "education"))

                @include(dashboard("profile", "family"))     

                @include(dashboard("profile", "address"))                
            </div>
                </div>
            </div>
        </div>

        <div class="col-12" id="Administrasi">
            <div id="administration-data" class="collapse" aria-labelledby="identity" data-parent="#accordion">
                <div class="card-body">
                    @include(dashboard("profile", "administration"))
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push("javascript")
<script>
    @if (session('update'))

        $("#edit").hide();

        (function () {
            setTimeout(function () {
                $("#session").slideUp(500, function() {
                    
                    $("#edit").show(500);
                });
            }, 5000);
        })();
    @endif
    (function () {
        $("#collapse1").click(function () {
            let adminData = $("#administration-data");

            if (adminData.hasClass("show")) {
                adminData.removeClass("show");
            }
        });

        $("#collapse2").click(function () {
            let identityData = $("#identity-data");

            if (identityData.hasClass("show")) {
                identityData.removeClass("show");
            }
        });
    })();
</script>
@endpush