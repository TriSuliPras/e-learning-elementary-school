<div class="content-header">
	<h2 class="title">{{ trans('label.dashboard') }}</h2>
	<p id="clock" class="mb-2"><i class="fa fa-calendar mr-1"></i></p>
</div>

<div class="content-body">
    <div class="row">
      <div class="col-3">
        <div class="panel">
          <div class="panel-body">
            <a href="{{ route('student.material') }}" class="panel-image fa fa-book panel-success"></a>
            <div class="panel-content">
              <h3 class="panel-header">{{ trans('label.total', ['what' => "Materi"]) }}</h3>
              <p class="big">{{ $materials->count() }}</p>
            </div>
          </div>
        </div>
        <div class="panel">
          <div class="panel-body">
            <a href="{{ route('student.task') }}" class="panel-image fa fa-tasks panel-warning"></a>
            <div class="panel-content">
              <h3 class="panel-header">{{ trans('label.total', ['what' => "Tugas"]) }}</h3>
              <p class="big">
                <span title="Jumlah Tugas" data-toggle="tooltip">{{ $tasks['total'] }}</span>
              </p>
            </div>
          </div>
        </div>
        <div class="panel">
          <div class="panel-body">
            <a href="{{ route('student.task') }}" class="panel-image fa fa-edit panel-info"></a>
            <div class="panel-content">
              <h3 class="panel-header">{{ trans('label.total', ['what' => "Kuis"]) }}</h3>
              <p class="big">{{ $quizs['total'] }}</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-6">
        <h3 class="mb-2">{{ trans('label.news') }}</h3>
        <div class="news-box panel-scrollable">
          @empty ($news->toArray())
              <div class="news">
                <h4>{{ trans('label.news_default_header') }}</h4>
              </div>
          @else
            @foreach ($news as $CommonNews)
              <div id="common-news" class="news" style="overflow: hidden; height: 210px;">
                <h4>
                  {{ $CommonNews->title }}
                  <br /><small><a href="{{ route('student.news.detail', $CommonNews->id) }}">{{ trans('label.read_more') }}</a></small>
                </h4>
                {!! $CommonNews->description !!}
              </div>
            @endforeach
          @endempty
        </div>
      </div>
      <div class="col-3">      
        <h3 class="mb-2">{{ trans('label.event', ['what' => "Tugas"]) }}</h3>
        <div class="panel">
          <div class="panel-body">
            <ul class="list-group list-group-flush">
              @empty ($tasks['today']->toArray())
                    <li class="list-group-item text-center">
                        <span><b>{{ trans('label.empty') }}</b></span>
                    </li>
                @else
                    @foreach ($tasks['today'] as $task)
                        <li class="list-group-item">
                            <span>{{ trans('label.for.-T') }}</span>
                            <a href="{{ route('task.detail', $task->id) }}" title="Detail" data-toggle="tooltip">{{ $task->title }}</a>
                        </li>
                    @endforeach
                @endempty
            </ul>
          </div>
        </div>
        <h3 class="mb-2">{{ trans('label.event', ['what' => "Kuis"]) }}</h3>
        @empty ($quizs['today']->toArray())
            <div class="panel">
                <div class="panel-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item text-center">
                            <b>{{ trans('label.empty') }}</b>
                        </li>
                    </ul>
                </div>
            </div>
        @else
            @foreach ($quizs['today'] as $quiz)
                <div class="panel">
                    <div class="panel-body">
                        <ul id="quiz" class="list-group list-group-flush text-center">
                            <li class="list-group-item {{ $loop->iteration }}"><a href="{{ route('student.quiz.detail', $quiz->id) }}" title="Detail" data-toggle="tooltip">{{ $quiz->title }}</a>
                                <div class="small">
                                  <span>
                                    {{ "{$quiz->class_room}{$quiz->pararel} - " . \Carbon\Carbon::parse($quiz->starting_time)->format('H:i') }}
                                  </span>
                                </div>
                                <span id="start{{ $loop->iteration }}" hidden>{{ \Carbon\Carbon::parse($quiz->starting_time)->toDateTimeString() }}</span>
                                <span id="end{{ $loop->iteration }}" hidden>{{ \Carbon\Carbon::parse($quiz->starting_time)->addMinutes($quiz->count_down)->toDateTimeString() }}</span>
                            </li>
                            <li id="status{{ $loop->iteration }}" class="list-group-item">
                                @if (is_equal($quiz->status,"true"))
                                    <span class="badge badge-success">{{ trans('label.quiz_status', ['is' => "Sudah"]) }}</span>
                                @else
                                    <span id="{{ $loop->iteration }}" class="badge badge-info">{{ trans('label.quiz_status', ['is' => "Belum"]) }}</span>
                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
            @endforeach
        @endempty
      </div>
	</div>
</div>