@extends("dashboard.management.home")

@push("css")
<style>
    .custom-file-input:lang(en)~.custom-file-label::after {
        content: "Pilih File";
        background-color: #18a566;
        color: #fff;
    }
</style>
@endpush

@section("content")
<div class="row justify-content-center">
	<div class="col-md-6">
		<div class="content-header">
	        <div>
	        	<a href="{{ route('task') }}"> 
	            	<div class="fa fa-angle-double-left mr-2"></div>
	            	{{ trans('label.index_page_of', ['what' => "Tugas"]) }}
	            </a>

	            @isset ($task)
	                &nbsp;<b>||</b>&nbsp;
		            <a href="{{ route('task.detail', $task->id) }} }}"> 
		            	{{ trans('label.detail_is', ['what' => "Tugas"]) }}
		            	<div class="fa fa-angle-double-right mr-2"></div>
		            </a>
	            @endisset
	        </div>
	        <h2 class="title">
	        	{{ !isset($task) ? trans('label.act.add', ['what' => "Tugas"]) : trans('label.act.edit_is', ['what' => "Tugas"]).' '.$task->title  }}
	        </h2>
	    </div>

	    <div class="content-body">
	    	@include("forms.task")
	    </div>
	</div>
</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/InputValidation.js') }}"></script>
<script src="{{ asset('web/customs/js/SelectOptionValidation.js') }}"></script>
<script>
	var lesson = new SelectOptionValidation('lesson_id'),
		classroom = new SelectOptionValidation('class_room'),
		input = new InputValidation('title', 'description', 'datetime', 'file_path');

		lesson.validated();
		classroom.validated();
		input.validated();


	$("#datetimepicker").datetimepicker({ format: "LLL" });
</script>
@endpush