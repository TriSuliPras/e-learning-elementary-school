@extends("dashboard.management.home")

@push("css")
<link rel="stylesheet" href="{{ asset('web/customs/css/scrollbar-list.css') }}">
@endpush

@section("content")
<div class="content-header">
    <div>
        <a href="{{ route('task') }}"> 
            <div class="fa fa-angle-double-left mr-2"></div>
            Halaman Utama Tugas
        </a>
    </div>

    <h2 class="title">
    	{{ "Tugas Mata Pelajaran ".$task->lessons->nama_matapelajaran}}
    	
    </h2>
</div>
<div class="content-body">
	<div class="row">
		<div class="col-md-8">
			<dl id="dlRow" class="row">
				<dt class="col-4"><h4>Judul</h4></dt>
				<dd class="col-8">{{ $task->title }}</dd>

				<dt class="col-4"><h4>Kelas</h4></dt>
				<dd class="col-8">{{ $task->classes->nama_kelas }}</dd>

				<dt class="col-4"><h4>Deskripsi</h4></dt>
				<dd class="col-8">{{ $task->description }}</dd>

				<dt class="col-4"><h4>Deadline</h4></dt>
				<dd class="col-8">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $task->deadline, "Asia/Jakarta")->format('d F Y H:i') }}
                </dd>

				<dt class="col-4"><h4>Penjelasan Tugas</h4></dt>
				<dd class="col-8">
					<a href="{{ asset('storage/tasks/uid_'.$task->user_id.'/cid_'.$task->class_id.'/lid_'.$task->lesson_id.'/'.$task->file_path) }}" download="{{ $task->title }}">
						{{ $task->file_path }}
					</a>
				</dd>
				
				<dt class="col-4"><h4>Waktu Upload</h4></dt>
				<dd class="col-8">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $task->created_at, "Asia/Jakarta")->diffForHumans() }}</dd>

				<dt class="col-4"><h4>Opsi</h4></dt>
				<dd class="col-8">
                    @if ($task->user_id == 1)
                    <a class="badge badge-primary" href="{{ route('task.edit', $task->id) }}">Edit</a>
                    <a class="badge badge-danger" href="{{ '#delete'.$task->id }}" data-toggle="modal">Hapus</a>
                    ||
					<a class="badge badge-info" href="{{ '#scores'.$task->id }}" data-toggle="modal">Nilai</a>

                    <a class="badge badge-warning" href="{{ '#lock'.$task->id }}" data-toggle="modal">Kunci</a>
                    @else
                    <a class="badge badge-info" href="{{ '#scores'.$task->id }}" data-toggle="modal">Nilai</a>
                    @endif
				</dd>
			</dl>
		
			<hr />
		</div>
	</div>
</div>
@endsection

@section("modal")
        <div class="modal fade" id="{{ 'scores'.$task->id }}" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nilai Tugas {{ $task->title }}</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <img src="{{ asset('web/images/icon-close.png') }}"/>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive-md">
                        <table class="table table-custom">
                            <thead>
                                <tr>
                                    <th width="1%">#</th>
                                    <th width="40%">Nama Siswa</th>
                                    <th >Nisn</th>
                                    <th width="13%">Nilai</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($task->students as $student)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $student->nama_siswa }}</td>
                                        <td align="center">{{ $student->nisn }}</td>
                                        <td width="1%"><span>{{ $student->scores }}</span></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">
                        Tutup
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="{{ 'lock'.$task->id }}" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Kunci Tugas {{ $task->title }}</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <img src="{{ asset('web/images/icon-close.png') }}"/>
                    </button>
                </div>
                <div class="modal-body">
                    <p style="margin-bottom: 0;">Jika tugas dikunci: 
                        <ul>
                            <li>semua siswa 
                                sudah tidak bisa mengumpulkan tugas!</li>
                            <li>nilai siswa tidak bisa lagi dimodifikasi</li>
                        </ul>
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">
                        Batal
                    </button>
                    <form id="lock" action="{{ route('task.update', $task->id) }}" method="POST">
                        {{ csrf_field() }} {{ method_field("PATCH") }}
                        <input name="status" type="text" value="lock" hidden>
                        <button class="btn btn-success" type="submit">OK</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="{{ 'delete'.$task->id }}" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Hapus Tugas {{ $task->title }}</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <img src="{{ asset('web/images/icon-close.png') }}"/>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Anda Yakin Ingin Mengapus data Ini?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">
                        Batal
                    </button>
                    <form action="{{ route('task.delete', $task->id) }}" method="POST">
                        {{ csrf_field() }} {{ method_field("DELETE") }}
                        <button class="btn btn-danger" type="submit">Hapus</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@push("javascript")
@endpush