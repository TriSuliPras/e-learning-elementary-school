@extends("dashboard.management.home")

@push("css")
<link rel="stylesheet" href="{{ asset('web/customs/css/option-hover.css') }}">
@endpush

@section("content")
<div class="content-header">
	<h2 class="title">{{ trans('label.index_page_of', ['what' => "Materi"]) }}</h2>
    <div class="right-content">
    	<a class="btn btn-primary pull-right btn-add" href="{{ route('material.add') }}">
    		<span>{{ trans('label.act.add', ['what' => "Materi"]) }}</span>
    	</a>
    </div>
    @component('components.search_periode')@endcomponent
</div>
@if (session("store") || session("delete") || session("update"))
    <div id="session" class="alert alert-success alert-dismissible fade show" role="alert">
        <span class="lead">
            {{ trans('confirmation.success') }} {{ session("store") ?  session("store") : (session("delete") ? session("delete") : session("update")) }}
        </span>
        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
@endif  
<div class="content-body">
	<div class="table-responsive-md">
		<table id="material" class="table table-custom">
            <thead>
                <th width="1%">{{ trans('label.for.#') }}</th>
                <th width="20%">{{ trans('label.for.-L') }}</th>
                <th width="25%">{{ trans('label.for.-T') }}</th>
                <th width="10%">{{ trans('label.for.-C') }}</th>
                @role('admin')<th>{{ trans('label.for.by') }}</th>@endrole
                <th width="1%"></th>
            </thead>

            <tbody>
                @empty ($datas->toArray())
                    <tr id="dlRow">
                        <td colspan="7" align="center"><b>{{ trans('label.empty') }}</b></td>
                    </tr>
                @else
                    @foreach ($datas as $material)
                    <tr id="dlrow">
                        <td align="center">{{ $loop->iteration }}</td>
                        <td>{{ $material->lessons->nama_matapelajaran }}</td>
                        <td align="center">{{ $material->title }}</td>
                        <td align="center">{{ "{$material->class_room} {$material->pararel}" }}</td>
                        @role('admin')
                        <td align="center">{{ is_null($material->teachers->employees['nama_pegawai']) ? "Admin" : $material->teachers->employees['nama_pegawai'] }}</td>
                        @endrole
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary btn-sm" type="button" data-toggle="dropdown">{{ trans('label.for.-O') }}</button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item dtl" href="{{ route('material.detail', $material->id) }}">{{ trans('label.act.detail') }}</a>
                                    @if ($material->user_id === auth()->user()->id)
                                    <a class="dropdown-item edit" href="{{ route('material.edit', $material->id) }}">{{ trans('label.act.edit') }}</a>
                                    <a class="dropdown-item del" href="{{ '#delete'.$material->id }}" data-toggle="modal">{{ trans('label.act.delete') }}</a>
                                    @endif

                                    @if (explode('.', $material->file_path)[1] === "pdf")
                                        <a class="dropdown-item" href="{{ route('material.show', $material->id) }}" target="_blank">{{ trans('label.act.reading') }}</a>
                                    @endif
                                    <a class="dropdown-item" href="{{ asset($material->file_path) }}" download="{{ $material->title . '.' .explode('.', $material->file_path)[1] }}">{{ trans('label.act.download') }}</a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                @endempty
            </tbody>
		</table>
	</div>
</div>
@endsection

@section("modal")
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/SessionMessages.js') }}"></script>
<script src="{{ asset('web/customs/js/DataTable.js') }}"></script>
<script>
    @if (!empty($datas->toArray()))
        new TableController("#material");
    @endif
</script>
@endpush