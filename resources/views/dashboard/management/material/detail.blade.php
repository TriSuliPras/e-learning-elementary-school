@extends("dashboard.management.home")

@push("css")
@endpush

@section("content")
<div class="content-header">
    <div>
        <a href="{{ route('material') }}"> 
            <div class="fa fa-angle-double-left mr-2"></div>
            {{ trans('label.index_page_of', ['what' => "Materi"]) }}
        </a>
		&nbsp;<b>||</b>&nbsp;
        <a href="{{ route('material.add') }}"> 
            {{ trans('label.act.add', ['what' => "Materi"]) }}
            <div class="fa fa-angle-double-right mr-2"></div>
        </a>
    </div>

    <h2 class="title">
		{!! trans('label.detail', ['for'=> "Materi",'what' => $material->lessons->nama_matapelajaran]) !!}
    </h2>
	@if (session("update"))
	    <div class="right-content" id="session">
	    	<span>
	    		<div class="alert alert-success alert-dismissible fade show" role="alert">
	    			<span class="lead">{{ trans('confirmation.success') }}</span>
	    			{{ session('delete') ? session('delete') : session('update') }}
	                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
	                	<span aria-hidden="true">×</span>
	                </button>
	            </div>
	    	</span>
	    </div>
	@endif
</div>
<div class="content-body">
	<div class="row">
		<div class="col-md-8">
			<dl id="dlRow" class="row">
				<dt class="col-4"><h4>{{ trans('label.for.-T') }}</h4></dt>
				<dd class="col-8">{{ $material->title }}</dd>

				<dt class="col-4"><h4>{{ trans('label.for.-C') }}</h4></dt>
				<dd class="col-8">{{ "{$material->class_room} {$material->pararel}" }}</dd>

				<dt class="col-4"><h4>{{ trans('label.for.-D') }}</h4></dt>
				<dd class="col-8">{{ null($material->description) ? trans('label.empty') : $material->description }}</dd>
				
				<dt class="col-4"><h4>{{ trans('label.for.-H') }}</h4></dt>
				<dd class="col-8">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $material->created_at, "Asia/Jakarta")->diffForHumans() }}</dd>

				<dt class="col-4"><h4>{{ trans('label.for.-O') }}</h4></dt>
				<dd class="col-8">
					@if ($material->user_id === auth()->user()->id)
						<a class="badge badge-success" href="{{ route('material.edit', $material->id) }}">{{ trans('label.act.edit') }}</a>
						<a class="badge badge-danger" href="#delete{{ $material->id }}" data-toggle="modal">{{ trans('label.act.delete') }}</a>
					@endif

					@if (explode('.', $material->file_path)[1] === "pdf")
						<a class="badge badge-info" href="{{ route('material.show', $material->id) }}" target="_blank">{{ trans('label.act.reading') }}</a>
					@endif
					<a class="badge badge-success" href="{{ asset($material->file_path) }}" download="{{ $material->title . '.' .explode('.', $material->file_path)[1] }}">{{ trans('label.act.download') }}</a>
				</dd>
			</dl>
		
			<hr />
		</div>

		<div class="col-md-4">
			@if (explode('.', $material->file_path)[1] === "pdf")
				<object id="pdf" data="{{ asset($material->file_path) }}" width="250px" height="250px" type="application/pdf" frameborder="5"></object>
			@endif
			<hr/>
		</div>
	</div>
</div>
@endsection


@section("modal")
	<div class="modal fade" id="{{ 'delete'.$material->id }}" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">{{ trans('label.act.delete') }}</h5>
	                <button class="close" type="button" data-dismiss="modal" aria-label="Close" title="Batal" data-toggle="tooltip">
	                    <img src="{{ asset('web/images/icon-close.png') }}"/>
	                </button>
	            </div>
	            <div class="modal-body">
	                <p>{{ trans('confirmation.sure_to_delete', ['this' => "Materi"]) }}</p>
	            </div>
	            <div class="modal-footer">
	                <form action="{{ route('material.delete', $material->id) }}" method="POST">
	                	{{ csrf_field() }} {{ method_field("DELETE") }}
	                	<button class="btn btn-danger btn-round" type="submit">{{ trans('label.btn.rm') }}</button>
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/SessionMessages.js') }}"></script>
@endpush