@extends("dashboard.management.home")

@push("css")
<style>
	.custom-file-input:lang(en)~.custom-file-label::after {
        content: "Pilih File";
        background-color: #18a566;
        color: #fff;
    }
</style>
@endpush

@section("content")
<div class="row justify-content-center">
	<div class="col-md-6">
		<div class="content-header">
	        <div>
	        	<a href="{{ route('material') }}"> 
	            	<div class="fa fa-angle-double-left mr-2"></div>
	            	{{ trans('label.index_page_of', ['what' => "Materi"]) }}
	            </a>

	            @isset ($material)
	                &nbsp;||&nbsp;
		            <a href="{{ route('material.detail', $material->id) }} }}"> 
		            	{{ trans('label.detail_is', ['what' => "Materi"]) }}
		            	<div class="fa fa-angle-double-right mr-2"></div>
		            </a>
	            @endisset
	        </div>
	        <h2 class="title">
	        	{{ !isset($material) ? trans('label.act.add', ['what' => "Materi"]) : trans('label.act.edit_is', ['what' => "Materi"]).' '.$material->title  }}
	        </h2>
	    </div>

	    <div class="content-body">
	    	@include("forms.material")
	    </div>
	</div>
</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/SelectOptionValidation.js') }}"></script>
<script src="{{ asset('web/customs/js/InputValidation.js') }}"></script>
<script>
	$(document).ready(function () {
		
		var selectLesson = new SelectOptionValidation("lesson_id"),
			selectClassRoom = new SelectOptionValidation('class_room'),
			input = new InputValidation('title', 'description', 'file_path');

			selectLesson.validated();
			selectClassRoom.validated();
			input.validated();
	});
</script>
@endpush