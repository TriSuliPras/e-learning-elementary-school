@extends("dashboard.management.home")

@push("css")
<style>
    .custom-file-input:lang(en)~.custom-file-label::after {
        content: "Pilih File";
        background-color: #18a566;
        color: #fff;
    }
</style>
@endpush

@section("content")

<div class="row justify-content-center">
	<div class="col-md-6">
		<div class="content-header">
	        <div>
	        	<a href="{{ route('quiz') }}"> 
	            	<div class="fa fa-angle-double-left mr-2"></div>
	            	Daftar Kuis
	            </a>

	            @isset ($quiz)
	                &nbsp;||&nbsp;
		            <a href="{{ route('quiz.detail', $quiz->id) }} }}"> 
		            	Detail Kuis {{ $quiz->title }}
		            	<div class="fa fa-angle-double-right mr-2"></div>
		            </a>
	            @endisset
	        </div>
	        <h2 class="title">
	        	{{ !isset($quiz) ? "Tambah Kuis" : "Edit Kuis $quiz->title" }}
	        </h2>
	    </div>

	    <div class="content-body">
	    	@include("forms.quiz")
	    </div>
	</div>
</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/InputValidation.js') }}"></script>
<script src="{{ asset('web/customs/js/SelectOptionValidation.js') }}"></script>
<script>
	 var lesson = new SelectOptionValidation('lesson_id'),
        classroom = new SelectOptionValidation('class_room'),
        inputs = new InputValidation(
            'title', 
            'datetime', 
            'count_down',
            'weight_multiple',
            'weight_essay',
            'file_path' 
        );

        lesson.validated();
        classroom.validated();
        inputs.validated();
    
	$("#datetimepicker").datetimepicker({ format: "LLL" });
	
	
    /*$("input[name=weight_multiple]").keyup(function() {
    	var multipleValue = $(this).val(),
    		essayValue = $("input[name=weight_essay]");

    	if (
    		parseInt(multipleValue) + parseInt(essayValue.val()) > 100 ||
    		parseInt(multipleValue) + parseInt(essayValue.val()) < 100 
    	) {
			$(this).addClass('is-invalid');
			essayValue.addClass('is-invalid');
			$('.weight').show();
    	}
    	else {

    		$(this).removeClass('is-invalid');
			essayValue.removeClass('is-invalid');
			$('.weight').hide();
    	}
    })

    $("input[name=weight_essay]").keyup(function() {
    	var essayValue = $(this).val(),
    		multipleValue = $("input[name=weight_multiple]");

    	if (
    		parseInt(multipleValue.val()) + parseInt(essayValue) > 100 ||
    		parseInt(multipleValue.val()) + parseInt(essayValue) < 100
    	) {
			$(this).addClass('is-invalid');
			multipleValue.addClass('is-invalid');
			$('.weight').show();
    	}
    	else {

    		$(this).removeClass('is-invalid');
			multipleValue.removeClass('is-invalid');
			$('.weight').hide();
    	}
    })*/
</script>
@endpush