@extends("dashboard.management.home")

@push("css")
<style>
	.count-down {
	    position: fixed;
	    top: 100px;
	    right: 30px;
	    background: #FFF;
	    padding: 15px;
	    border: 1px solid #e2e2e2;
	    z-index: 99;
	}
</style>
@endpush

@section("content")
<div class="row justify-content-center">
	<div class="col-md-12">
		<div class="content-header mb-0">
			<div>
		        <a href="{{ route('quiz.detail', $quiz->id) }}"> 
		        	<div class="fa fa-angle-double-left mr-2"></div>
		        	Halaman Detail Kuis {{ $quiz->title }}
		        </a>
		        @if (Route::is("quiz.essay"))
		        	<div class="right-content">
				    	<a id="send" class="btn btn-primary pull-right btn-add" href="#send">
				    		<span>Kirim</span>
				    	</a>
				    </div>
		        @endif
		    </div>

			<div class="table-responsive-md">
				<table class="table table-custom">
					<tbody>
						<table class="col-md-6" width="75%">
							@if (Route::is('quiz.show'))
							<tr>
								<td><b>Judul</b></td>
								<td><b>:</b></td>
								<td>{{ $quiz->title }}</td>
							</tr>
							<tr>
								<td><b>Mata Pelajaran</b></td>
								<td><b>:</b></td>
								<td>{{ $quiz->lessons->nama_matapelajaran }}</td>
							</tr>
							<tr>
								<td><b>Waktu</b></td>
								<td><b>:</b></td>
								<td>{{ $quiz->count_down }} Menit</td>
							</tr>
							@if (count($questions['multiple']) > 0)
							<tr>
								<td><b>Soal Pilihan Ganda</b></td>
								<td><b>:</b></td>
								<td>{{ count($questions['multiple']) }} Soal</td>
							</tr>
							@endif
							@if (count($questions['essay']) > 0)
							<tr>
								<td><b>Soal Essay</b></td>
								<td><b>:</b></td>
								<td>{{ count($questions['essay']) }} Soal</td>
							</tr>
							@endif
							@elseif(Route::is("quiz.essay"))
							<tr>
								<td><b>Kuis</b></td>
								<td><b>:</b></td>
								<td>{{ $quiz->title }}</td>
							</tr>
							<tr>
								<td><b>Mata Pelajaran</b></td>
								<td><b>:</b></td>
								<td>{{ $quiz->lessons->nama_matapelajaran }}</td>
							</tr>
							@if (count($questions['multiple']) > 0)
							<tr>
								<td><b>Soal Pilihan Ganda</b></td>
								<td><b>:</b></td>
								<td>
									<a data-toggle="collapse" href="#mtp" role="button" aria-expanded="false" aria-controls="mtp" title="klik untuk lihat / sembunyikan soal">{{ count($questions['multiple']) }} Soal</a></td>
							</tr>
							@endif
							@if (count($questions['essay']) > 0)
							<tr>
								<td><b>Soal Essay</b></td>
								<td><b>:</b></td>
								<td>
									<a data-toggle="collapse" href="#esy" role="button" aria-expanded="false" aria-controls="esy" title="klik untuk lihat / sembunyikan soal">{{ count($questions['essay']) }} Soal</a>
								</td>
							</tr>
							@endif
							<tr>
								<td><b>Nama</b></td>
								<td><b>:</b></td>
								<td>{{ $quiz->students[0]->nama_siswa }}</td>
							</tr>

							<tr>
								<td><b>NISN</b></td>
								<td><b>:</b></td>
								<td>{{ $quiz->students[0]->nisn }}</td>
							</tr>
							<tr>
								<td><b>Kelas</b></td>
								<td><b>:</b></td>
								<td>{{ "{$quiz->class_room} {$quiz->pararel}" }}</td>
							</tr>
							@endif
						</table>
					</tbody>
				</table>
			</div>
		</div>

		<div class="content-body">
			<div class="row">
				<div class="col-md-12">
					@if (Route::is("quiz.essay"))
					    <form action="{{ route('quiz.essay.update', $quiz->id) }}" method="POST">
					    	{{ csrf_field() }} @method('PATCH')
					    	<input name="student_id" type="text" value="{{ $studentAnswers['id'] }}" hidden>
					    	<input name="questions_mtp" type="number" value="{{ count($questions['multiple']) }}" hidden />
					    	<input name="questions_esy" type="number" value="{{ count($questions['essay']) }}" hidden />
					    	<input name="mtp_score" type="number" value="{{ $studentAnswers['mtp_score'] }}" hidden>
					@endif
						@if (count($questions['multiple']) > 0)
							@include(dashboard("quiz.questions", "multiple"))
						@endif
						@if (count($questions['essay']) > 0)
							@include(dashboard("quiz.questions", "essay"))
						@endif
					@if (Route::is("quiz.essay"))
						</form>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section("modal")
	<div class="modal fade" id="send" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Kirim Nilai</h5>
	                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
	                    <img src="{{ asset('web/images/icon-close.png') }}"/>
	                </button>
	            </div>
	            <div class="modal-body">
	                <p>Anda Yakin Ingin Dengan Data Yang akan Disimpan?</p>
	            </div>
	            <div class="modal-footer">
	                <button class="btn btn-danger" type="button" data-dismiss="modal">
	                    Batal
	                </button>
	                <button id="submit" class="btn btn-success" type="button">OK</button>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/SessionMessages.js') }}"></script>
<script>
	$(document).ready(function () {
		
		$('a#send').click(function () {
			$('form').submit();
		});
	});
</script>
@endpush