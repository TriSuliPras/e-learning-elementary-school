@extends("dashboard.management.home")

@push("css")
<link rel="stylesheet" href="{{ asset('web/customs/css/option-hover.css') }}">
@endpush

@section("content")
<div class="content-header">
    <div>
        <a href="{{ route('quiz') }}"> 
            <div class="fa fa-angle-double-left mr-2"></div>
            {{ trans('label.index_page_of', ['what' => "Kuis"]) }}
        </a>
        &nbsp;<b>||</b>&nbsp;
        <a href="{{ route('quiz.create') }}"> 
            {{ trans('label.act.add', ['what' => "Kuis"]) }}
            <div class="fa fa-angle-double-right mr-2"></div>
        </a>
    </div>

    <h2 class="title">
        {!! trans('label.detail', ['for' => "Kuis", 'what' => $quiz->lessons->nama_matapelajaran]) !!}
    </h2>
	@if (session("update"))
	    <div class="right-content" id="session">
	    	<span>
	    		<div class="alert alert-success alert-dismissible fade show" role="alert">
	    			<span class="lead">{{ trans('confirmation.success') }} {{ session('update') }}</span>
	                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
	                	<span aria-hidden="true">×</span>
	                </button>
	            </div>
	    	</span>
	    </div>
	@endif
</div>

<div class="content-body">
	<div class="row">
		<div class="col-md-8">
            <dl id="dlRow" class="row">
                <dt class="col-4"><h4>{{ trans('label.for.-T') }}</h4></dt>
                <dd class="col-8">{{ $quiz->title }}</dd>

                <dt class="col-4"><h4>{{ trans('label.quiz_count') }}</h4></dt>
                <dd class="col-8">
                    @if (not_null($quiz->weight_multiple))
                        <span title="Bobot Soal {{ $quiz->weight_multiple }} %" data-toggle="tooltip">Pilihan Ganda {{ $questions['multiple'] }}</span>
                    @endif

                    @if (not_null($quiz->weight_multiple) && not_null($quiz->weight_essay))
                        &nbsp;
                        <b>||</b>
                        &nbsp;
                    @endif
                    @if (not_null($quiz->weight_essay))
                        <span title="Bobot Soal {{ $quiz->weight_essay }} %" data-toggle="tooltip">Essay {{ $questions['essay'] }}</span>
                    @endif
                </dd>

                <dt class="col-4"><h4>{{ trans('label.for.-C') }}</h4></dt>
                <dd class="col-8">{{ "$quiz->class_room $quiz->pararel" }}</dd>

                <dt class="col-4"><h4>{{ trans('label.for.-D') }}</h4></dt>
                <dd class="col-8">{{ $quiz->description }}</dd>

                <dt class="col-4"><h4>{{ trans('label.for.-s') }}</h4></dt>
                <dd id="status" class="col-8">
                    @if ($quiz->status === "true")
                        <span class="badge badge-success">{{ trans('label.quiz_status', ['is' => "Sudah"]) }}</span>
                    @else
                        <span class="badge badge-info">{{ trans('label.quiz_status', ['is' => "Belum"]) }}</span>
                    @endif

                    <span id="start" hidden>{{ \Carbon\Carbon::parse($quiz->starting_time)->toDateTimeString() }}</span>
                    <span id="end" hidden>{{ \Carbon\Carbon::parse($quiz->starting_time)->addMinutes($quiz->count_down)->toDateTimeString() }}</span>
                </dd>
               
                <dt class="col-4"><h4>{{ trans('label.quiz_start') }}</h4></dt>
                <dd class="col-8" id="datecounter">{{ \Carbon\Carbon::parse($quiz->starting_time)->diffForHumans() }}</dd>
                
                <dt class="col-4"><h4>{{ trans('label.for.-c') }}</h4></dt>
                <dd class="col-8">{{ $quiz->count_down }} {{ trans('label.for.minute') }}</dd>

                <dt class="col-4 timer" style="display: none;"><h4>{{ trans('label.time_run') }}</h4></dt>
                <dd id="timer" class="col-8 timer" style="display: none;"></dd>

                <dt class="col-4"><h4>{{ trans('label.for.-O') }}</h4></dt>
                <dd class="col-8">
                    <a class="badge badge-info" href="{{ route('quiz.show', $quiz->id) }}">{{ trans('label.act.exercise') }}</a>
                    @if ($quiz->status === "true")
                        <a class="badge badge-success result" href="{{ '#quiz'.$quiz->id }}" data-toggle="modal">{{ trans('label.score') }}</a>
                    @endif

                    @if ($quiz->status === "false" && management()->getId() === $quiz->teachers->id)
                        <a class="badge badge-primary ed" href="{{ route('quiz.edit', $quiz->id) }}">{{ trans('label.btn.ed') }}</a>
                        <a class="badge badge-danger de" href="{{ '#delete'.$quiz->id }}" data-toggle="modal">{{ trans('label.act.delete') }}</a>
                    @endif
                </dd>
            </dl>

            <hr />
        </div>
	</div>
</div>
@endsection

@section("modal")
<div class="modal fade score" id="{{ 'quiz'.$quiz->id }}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    {{ trans('label.score').' '.$quiz->title }}
                </h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" title="Tutup">
                    <img src="{{ asset('web/images/icon-close.png') }}"/>
                </button>
            </div>
            <div class="modal-body">
                @include("forms.modal.quiz_detail_student_scores")
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="{{ 'delete'.$quiz->id }}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ trans('label.act.delete') }}</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close" title="Batal" data-toggle="tooltip">
                    <img src="{{ asset('web/images/icon-close.png') }}"/>
                </button>
            </div>
            <div class="modal-body">
                {{ trans('confirmation.sure_to_delete', ['this' => "Kuis"]) }}
            </div>
            <div class="modal-footer">
                <form action="{{ route('quiz.delete', $quiz->id) }}" method="POST">
                    {{ csrf_field() }} {{ method_field("DELETE") }}
                    <button class="btn btn-danger btn-round" type="submit">{{ trans('label.btn.rm') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/SessionMessages.js') }}"></script>
<script>
	$(document).ready(function() {
        @if (!empty($quiz->students->toArray()))
            $("#student_scores").DataTable({
                "language": {
                    "lengthMenu": "Tampil _MENU_",
                    "zeroRecords": "Tidak dapat menemukan - maaf",
                    "info": "Menampilkan _START_ sampai _END_ dari total _TOTAL_ data",
                    "infoEmpty": "Data tidak ditemukan",
                    "infoFiltered": "(filter dari _MAX_ total data)",
                    "search": "Cari"
                }  ,
                "aLengthMenu": [
                    [5, 10], 
                    [5, 10]
                ]
            });
        @endif

        $("a.lock").each(function( ) {
            $(this).click(function ( event ) {
                event.preventDefault();
                var isConfirmed = confirm("{{ trans('confirmation.sure_to_lock', ['score' => "Kuis"]) }}");

                if (isConfirmed) {
                    var uri = $(this).attr('href');
                    $.ajax({
                        url: uri,
                        type: "PATCH",
                        data: {'_token': "{{ csrf_token() }}"},
                        success: function ( response ) {
                            alert(response);
                            window.location.reload();
                        },
                        error: function ( data ) {
                            alert(response);
                        }
                    });
                }
            })
        })
	});

    var quizDate = new Date("{{ \Carbon\Carbon::parse($quiz->starting_time) }}").getTime();
    var timer = "{{ \Carbon\Carbon::parse($quiz->starting_time)->addMinutes($quiz->count_down)->format('d M Y H:i') }}";
    var dlStatus = $("dl#dlRow").find("dd#status");
    var spanStart = dlStatus.find("#start"),
        spanEnd = dlStatus.find("#end");
    var onStart = false;

    var quiz = {
        start: new Date(spanStart.text()),
        end: new Date(spanEnd.text())
    };

    $(function () {
        var quizInterval = setInterval(() => {
            var oneDay = 24 * 60 * 60 * 1000;
            var firstDate = new Date(timer);
            var secondDate = new Date();
            var days = (firstDate.getTime() - secondDate.getTime()) / (oneDay);
            var hrs = (days - Math.floor(days)) * 24;
            var min = (hrs - Math.floor(hrs)) * 60;

            var sec = Math.floor((min - Math.floor(min))* 60);
            var time = `${Math.floor(hrs)} Jam` + ' ' + `${(Math.floor(min))} Menit` + ' ' + `${sec} Detik`;
            
            console.log(time);
            
            
            if (onStart) {
                $(".timer").show(500);
                $("#timer").text(time);
            }
            if (!onStart) {
                
                clearInterval(quizInterval);
                $(".timer").hide(500);
            }
            // $(".count-down").text("Waktu Habis");

            //clearInterval(quizInterval);
        }, 1000)    
    });
    
    var countDown = setInterval(function() {
        
        var thisDay = new Date();

        var distance = quizDate - thisDay.getTime();
        
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        if (days > 0) {
            
            getId("datecounter").innerHTML = days + " hari dari sekarang";
        }
        else if (hours > 0) {
            
            getId("datecounter").innerHTML = hours + " jam dari sekarang";
        }
        else if (minutes > 0) {
            
            getId("datecounter").innerHTML = minutes + " menit dari sekarang";
        }
        else if (seconds > 0) {
            
            getId("datecounter").innerHTML = seconds + " detik dari sekarang";
        }
        else {

            minutes *= -1;
            hours *= -1;

            if (hours > 1) {
                
                getId("datecounter").innerHTML = hours + " jam yang lalu";
            }
            else
                getId("datecounter").innerHTML = minutes - 1 + " menit yang lalu";
        }



        if (thisDay.getTime() >= quiz.start.getTime() && thisDay.getTime() <= quiz.end.getTime()) {
            onStart = true;
            dlStatus.find("span.badge").text('{{ trans('label.quiz_status', ['is' => "Sedang"]) }}');
            $("a.ed").hide();
            $("a.de").hide();

            if (thisDay.getSeconds() % 2 === 0) {
                dlStatus.find("span.badge").css("background-color", "#ff3f34");
            }
            else
                dlStatus.find("span.badge").css("background-color", "#ffa801");
        }
        else if (thisDay.getTime() > quiz.end.getTime()) {
            onStart = false;
            dlStatus.find("span.badge").text('{{ trans('label.quiz_status', ['is' => "Sudah"]) }}').css("background-color", "#18a566");

            $("a.ed").hide();
            $("a.de").hide();
        }
    }, 1000);
</script>
@endpush