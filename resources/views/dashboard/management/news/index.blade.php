@extends("dashboard.management.home")

@push('css')
<link rel="stylesheet" href="{{ asset('web/customs/css/option-hover.css') }}">
@endpush

@section("content")
<div class="content-header">
	<h2 class="title">{{ trans('label.index_page_of', ['what' => "Berita"]) }}</h2>
    <div class="right-content">
    	<a id="form" class="btn btn-primary pull-right btn-add" href="{{ route('news.add.category') }}">
    		<span>{{ trans('label.act.add', ['what' => "Berita"]) }}</span>
    	</a>
    </div>
</div>

@if (session("store") || session("delete") || session("update") )
    <div id="session" class="alert alert-success alert-dismissible fade show" role="alert">
        <span class="lead">
            {{ trans('confirmation.success') }} {{ session("store") ?  session("store") : (session("delete") ? session("delete") : session("update")) }}
        </span>
        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
@else
    <div id="updateStatus" class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
        <span class="lead"></span>
        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
@endif  
<div class="content-body">
	<div class="table-responsive-md">
		<table id="news" class="table table-custom">
            <thead>
                <th width="1%">{{ trans('label.for.#') }}</th>
                <th width="20%">{{ trans('label.for.-T') }}</th>
                <th>{{ trans('label.category') }}</th>
                <th>{{ trans('label.for.by') }}</th>
                <th>{{ trans('label.for.-H') }}</th>
                <th>{{ trans('label.for.-s') }}</th>
                <th width="1%"></th>
            </thead>

            <tbody>
                @empty ($datas->toArray())
                    <tr id="dlRow">
                        <td colspan="7" align="center"><b>{{ trans('label.empty') }}</b></td>
                    </tr>
                @else
                    @foreach ($datas as $news)
                    <tr id="dlrow">
                        <td align="center">{{ $loop->iteration }}</td>
                        <td>{{ $news->title }}</td>
                        <td align="center">
                            @if ($news->receiver === "Management")
                                {{ trans('label.for.-M') }}
                            @elseif ($news->receiver === "Student")
                                {{ trans('label.for.-S') }}
                            @elseif ($news->receiver === "Class")
                                 {{ trans('label.for.-C') }}
                            @else
                                 {{ trans('label.common') }}
                            @endif
                        </td>
                        <td align="center">{{ $news->teachers->employees['nama_pegawai'] }}</td>
                        <td align="center">{{ $news->created_at->diffForHumans() }}</td>
                        <td align="center">
                            @if ($news->status === "active")
                                <span class="badge badge-success">{{ trans('label.active') }}</span>
                            @else
                                <span class="badge badge-warning">{{ trans('label.!active') }}</span>
                            @endif
                        </td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary btn-sm" type="button" data-toggle="dropdown">{{ trans('label.for.-O') }}</button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    @if ($news->status === "active")
                                        <a class="dropdown-item dtl" href="{{ route('news.detail', $news->id) }}">{{ trans('label.act.detail') }}</a>
                                    @endif
                                    @if ($news->user_id === auth()->user()->id)
                                        @if ($news->status === "active")
                                            <a class="dropdown-item edit" href="{{ route('news.edit.category', $news->id) }}">{{ trans('label.act.edit') }}</a>
                                        @endif
                                    <a class="dropdown-item del" href="{{ '#delete'.$news->id }}" data-toggle="modal">{{ trans('label.act.delete') }}</a>

                                    <a id="{{ "$news->title id_$news->id" }}" class="dropdown-item {{ $news->status === "active" ? 'del' : 'dtl' }} banned" href="#banned">{{ $news->status === "active" ? trans('label.!active') : trans('label.active') }}</a>
                                    @endif
                            </div>
                        </td>
                    </tr>
                    @endforeach
                @endempty
            </tbody>
		</table>
	</div>
</div>
@endsection

@section("modal")
<div class="modal fade" id="banned" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" title="{{ trans('label.tooltip.-c') }}">
                    <img src="{{ asset('web/images/icon-close.png') }}"/>
                </button>
            </div>

            <div class="modal-body"></div>

            <div class="modal-footer">
                <button id="submit" class="btn btn-primary" type="submit">{{ trans('label.btn.-o') }}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/SessionMessages.js') }}" ></script>
<script src="{{ asset('web/customs/js/DataTable.js') }}" ></script>
<script>
    $(document).ready(function () {
        @if (! empty($datas->toArray()))
            new TableController("#news");
        @endif

        $("a.banned").click(function(e) {
            
            e.preventDefault();

            let a = $(this);
            let modal = $("#banned");
            let title = $(this).attr("id");
                title = title.split('id_');
            
            modal.find('button[data-dismiss = modal]')
                .click(function() { modal.find("button[type=submit]").attr("id", "submit"); });
            modal.find('h5.modal-title').text("Judul: " + title[0]);
            modal.find('button#submit').attr("id",  title[0] + "banned" + title[1]);

            if (a.text() == "Tidak Aktif") {
                modal.find('.modal-body').html("<p>Jika Berita Dinonaktifkan, Berita tidak dapat dibaca!</p>");
            }
            else 
                modal.find('.modal-body').html("<p>Klik OK untuk melanjutkan</p>");

            modal.modal('show');

            modal.find("button[type=submit]")
                 .click(function(e) {
                    e.preventDefault();
                    var news = $(this).attr('id'),
                        id = news.split('banned')[1],
                        by_title = news.split('banned')[0];
                    
                    $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                    });
                    $.ajax({
                        type        : "PATCH",
                        url         : "{{ route('news.update.status') }}",
                        data        : {by_id: id, title: by_title, status: a.text()},
                        dataType    : "JSON",
                        beforeSend  : function() { modal.find("button[type=submit]").attr("id", "submit"); },
                        success     : function(data) {

                                        $('#banned').modal("hide");
                                        var timeout = setTimeout(function() { 

                                            $("#updateStatus").slideDown(500, function() {
                                                $(this).find('span.lead').text("SUCCESS: " + data.msg);
                                            });

                                            setTimeout(function() {
                                                $("#updateStatus").slideUp(500);
                                                clearTimeout(timeout);
                                                location.reload(true);
                                            }, 3000); 
                                        }, 1000);
                                      },
                        error       : function(data) {
                                        var div = $("<div>" , {
                                            id: "session",
                                            class: "alert alert-danger alert-dismissible fade show",
                                            role: "alert",
                                            html: [
                                                $("<span>", {class: "lead", text: `Oops!: ${data.msg}`}),
                                                $("<button>", {
                                                    class: "close", 
                                                    type: "button", 
                                                    'data-dismiss': "alert",
                                                    'aria-label': "Close",
                                                    html: $("<span>", {'aria-hidden': true, 'text': "×"})
                                                })
                                            ]
                                        });
                                        
                                        $('#banned').modal("hide");
                                        setTimeout(function() { $('.content-header').after(div); }, 1000);
                                      }
                    });
                 })
        });

    });
</script>
@endpush