@extends("dashboard.management.home")

@push("css")
<style>
	#img-profil {
        width: 250px;
        height: 250px;
        border-radius: 10px;
        border: 1px solid black;
        margin: 0 49px;
    }
</style>
@endpush

@section("content")
<div class="content-header">
    <div>
    	<a href="{{ route('management.home') }}"> 
        	<div class="fa fa-angle-double-left mr-2"></div>
        	{{ trans('label.dashboard') }}
        </a>
    </div>
    <h2 class="title">{{ trans('label.profile.-M') }}</h2>
    @if (session("update"))
	    <div class="right-content" id="session">
	    	<span>
	    		<div class="alert alert-success alert-dismissible fade show" role="alert">
	    			<span class="lead">{{ trans('confirmation.success') }}</span>
	    			{{ session('update') }}
	                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
	                	<span aria-hidden="true">×</span>
	                </button>
	            </div>
	    	</span>
	    </div>
	@endif

	@if (auth()->user()->pegawai_id === $employee->id)
    <div class="right-content">
    	<a id="edit" class="btn btn-primary pull-right btn-add" href="{{ route('management.employee.edit', auth()->user()->pegawai_id) }}"> 
    		<span>{{ trans('label.profile.-e') }}</span>
    	</a>
    </div>
    @endif
</div>

<div class="content-body">
	<div class="row">
		<div class="col-md-8">
			<dl class="row">
				<dt class="col-4"><h4>{{ trans('label.for.nip') }}</h4></dt>
				<dd class="col-8">{{ $employee->nip }}</dd>
				<dt class="col-4"><h4>{{ trans('label.for.name') }}</h4></dt>
				<dd class="col-8">{{ $employee->nama_pegawai }}</dd>
				<dt class="col-4"><h4>{{ trans('label.for.sex') }}</h4></dt>
				<dd class="col-8">{{ $employee->jenis_kelamin === "L" ? "Laki-Laki" : "Perempuan" }}</dd>
				<dt class="col-4"><h4>{{ trans('label.for.POB') }}</h4></dt>
				<dd class="col-8">{{ $employee->ttl }}</dd>
				<dt class="col-4"><h4>{{ trans('label.for.address') }}</h4></dt>
				<dd class="col-8">{{ $employee->alamat }}</dd>
				<dt class="col-4"><h4>{{ trans('label.for.group') }}</h4></dt>
				<dd class="col-8">{{ $employee->gol }}</dd>
				<dt class="col-4"><h4>{{ trans('label.for.phone') }}</h4></dt>
				<dd class="col-8">{{ $employee->no_telp }}</dd>
				<dt class="col-4"><h4>{{ trans('label.for.as') }}</h4></dt>
				<dd class="col-8">{{ $employee->jabatan }}</dd>
			</dl>

			<div class="mb-3">
				<ul class="nav nav-tabs" id="employee-tab" role="tablist">
					<li class="nav-item">
						<a id="education-tab" href="#education" role="tab" aria-controls="education" aria-selected="true" class="nav-link active" data-toggle="tab">{{ trans('label.for.education') }}</a>
					</li>
					<li class="nav-item">
						<a id="appointment-tab" href="#appointment" role="tab" aria-controls="appointment" aria-selected="false" class="nav-link" data-toggle="tab">{{ trans('label.for.appointed') }}</a>
					</li>
					<li class="nav-item">
						<a id="graduation-tab" href="#graduation" role="tab" aria-controls="graduation" aria-selected="false" class="nav-link" data-toggle="tab">{{ trans('label.for.tmt') }}</a>
					</li>
					<li class="nav-item">
						<a id="status-tab" href="#status" role="tab" aria-controls="status" aria-selected="false" class="nav-link" data-toggle="tab">{{ trans('label.for.-s') }}</a>
					</li>
				</ul>
			</div>
			
			<div id="employee-tab-content" class="tab-content">
				<div id="education" class="tab-pane fade show active" role="tabpanel" aria-labelledby="education-tab">
					<dl class="row">
						<dt class="col-4"><h4>{{ trans('label.for.last_edu') }}</h4></dt>
						<dd class="col-8">{{ $employee->pendidikan_terakhir }}</dd>
						<dt class="col-4"><h4>{{ trans('label.for.place') }}</h4></dt>
						<dd class="col-8">{{ $employee->pendidikan_terakhir_tempat }}</dd>
					</dl>
				</div>

				<div id="appointment" class="tab-pane fade show" role="tabpanel" aria-labelledby="appointment-tab">
					<dl class="row">
						<dt class="col-4"><h4>{{ trans('label.for.cpns') }}</h4></dt>
						<dd class="col-8">{{ $employee->CPNS_TMT }}</dd>

						<dt class="col-4"><h4>{{ trans('label.for.pns') }}</h4></dt>
						<dd class="col-8">{{ $employee->PNS_TMT }}</dd>
					</dl>
				</div>

				<div id="graduation" class="tab-pane fade show" role="tabpanel" aria-labelledby="graduation-tab">
					<dl class="row">
						<dt class="col-4"><h4>{{ trans('label.for.-d') }}</h4></dt>
						<dd class="col-8">{{ $employee->tmt_guru_tanggal }}</dd>
						<dt class="col-4"><h4>{{ trans('label.for.-m') }}</h4></dt>
						<dd class="col-8">{{ $employee->tmt_guru_bulan }}</dd>
						<dt class="col-4"><h4><h4>{{ trans('label.for.-y') }}</h4></dt>
						<dd class="col-8">{{ $employee->tmt_guru_tahun }}</dd>
					</dl>
				</div>

				<div id="status" class="tab-pane fade show" role="tabpanel" aria-labelledby="status-tab">
					<dl class="row">
						<dt class="col-4"><h4>{{ trans('label.for.home_teach') }}</h4></dt>
						<dd class="col-8">{{ is_null($employee->homeroom) ? trans('label.for.!home_teach') : $employee->homeroom->nama_kelas }}</dd>
						<dt class="col-4"><h4>{{ trans('label.for.-s -w', ['is' => "NUPTK"]) }}</h4></dt>
						<dd class="col-8">{{ $employee->NUPTK }}</dd>

						<dt class="col-4"><h4>{{ trans('label.for.-s -w', ['is' => "GTT"]) }}</h4></dt>
						<dd class="col-8">{{ $employee->GTT }}</dd>

						<dt class="col-4"><h4>{{ trans('label.for.-s -w', ['is' => "GTY"]) }}</h4></dt>
						<dd class="col-8">{{ $employee->GTY }}</dd>
						
						<dt class="col-4"><h4>{{ trans('label.for.-s -w', ['is' => "Sertifikasi"]) }}</h4></dt>
						<dd class="col-8">{{ $employee->sertifikasi_tahun }}</dd>
					</dl>
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="row">
				<div>
					<div id="img-profil"></div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push("javascript")
<script>
	@if (session('update'))

		$("#edit").hide();

		(function () {
			setTimeout(function () {
				$("#session").slideUp(500, function() {
					
					$("#edit").show(500);
				});
			}, 5000);
		})();
	@endif
</script>
@endpush