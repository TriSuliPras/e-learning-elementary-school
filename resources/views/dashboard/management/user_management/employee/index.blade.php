@extends("dashboard.management.home")

@push("css")
<link rel="stylesheet" href="{{ asset('web/customs/css/option-hover.css') }}">
@endpush

@section("content")
<div class="content-header">
    <h2 class="title">Daftar Management</h2>
    <div class="right-content">
    	<a class="btn btn-primary pull-right btn-add" href="{{ route('management.employee.create') }}"> 
    		<span>Tambah Management</span>
    	</a>
    </div>
</div>
@if (session("saved") || session("delete"))
<div id="session" class="alert alert-success alert-dismissible fade show" role="alert">
    <span class="lead">
        Success!: {{ session("saved") ? session("saved") : (session("delete") ? session("delete") : '') }}
    </span>
    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>  
@endif

<div class="content-body">
<div class="row justify-content-between mb-4">
    <div class="col-md-2 col-sm-4">
        <div class="filter">
            <select class="select-control" name="search" title="Filter">
                <option value="name">Nama</option>
                <option value="as">Jabatan</option>
            </select>
        </div>
    </div>
    <div class="col-md-4 col-sm-5">
        <div class="search">
            <form action="{{ route('management.employee.index') }}" method="GET">
                {{ csrf_field() }}
                <input name="option" type="text" id="option" hidden value="" />
                <input name="search" id="input" class="form-control" type="text" placeholder="Cari..," title="Berdasarkan Nama / Jabatan" data-toggle="tooltip" />
                <div id="select_teacher" style="display: none;">
                    <select name="search_teacher" class="select-control" title="Pilih Pengajar" data-live-search="true">
                        @foreach ($employees as $teacher)
                            <option value="{{ $teacher->id }}">{{ $teacher->nama_pegawai }}</option>
                        @endforeach
                    </select>
                </div>

                <div id="select_as" style="display: none;">
                    <select name="search_as" class="select-control" title="Pilih Jabatan" data-live-search="true">
                        @foreach ($employees as $teacher)
                            <option value="{{ $teacher->jabatan }}">{{ $teacher->jabatan }}</option>
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-link fa fa-search" type="submit"></button>
            </form>
        </div>
    </div>
</div>

<div class="table-responsive-md">
    <table class="table table-custom">
         <thead>
          <th width="1%">#</th>
          <th width="25%">Nama</th>
          <th width="15%">Jabatan</th>
          <th>ALamat</th>
          <th>No Telpon</th>
          <th width="1%"> </th>
         </thead>

         <tbody id="tbody-employees">
              @if (is_null($employees))
                <tr>
                  <td align="center" colspan="6"><h4>Data Tidak Ditemukan!</h4></td>
                </tr>
              @elseif (array_key_exists('data', $employees->toArray()) && empty($employees->toArray()['data']))
                <tr>
                  <td align="center" colspan="6"><h4>Data Tidak Ditemukan!</h4></td>
                </tr>
              @elseif (empty($employees->toArray()))
                <tr>
                  <td align="center" colspan="6"><h4>Data Tidak Ditemukan!</h4></td>
                </tr>
              @else
                @foreach($employees as $employee)
                <tr>
                    <td align="center">{{ $loop->iteration }}</td>
                    <td>{{ $employee->nama_pegawai }}</td>
                    <td align="center">{{ $employee->jabatan }}</td>
                    <td align="right">{{ $employee->alamat }}</td>
                    <td align="center">{{ $employee->no_telp }}</td>
                    <td>
                         <div class="dropdown">
                             <button class="btn btn-outline-secondary btn-sm" type="button" data-toggle="dropdown">Opsi</button>
                             <div class="dropdown-menu dropdown-menu-right">
                              <a class="dropdown-item dtl" href="{{ route('management.employee.show', $employee->id) }}">Detail</a>
                              @if ($employee->id === auth()->user()->pegawai_id)
                                  <a class="dropdown-item edit" href="{{ route('management.employee.edit', $employee->id) }}">Edit</a>
                              @endif
                              <a class="dropdown-item del" href="{{ '#delete'.$employee->id }}" data-toggle="modal">Hapus</a></div>
                         </div>
                    </td>
                </tr>
                @endforeach
              @endif
         </tbody>
    </table>
</div>

<div class="row justify-content-between mb-4">
    <div class="col-md-2 col-sm-3"></div>
    <div class="col-md-10 col-sm-6 text-right">
        {{ $employees->links('pagination.paginate') }}
    </div>
</div>
</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/SessionMessages.js') }}"></script>
<script>
	$(document).ready(function () {
		$("select[name=search]").change(function () {
			
			let option = $(this).val();

			$("#option").attr("value", option);

			switch (option) {
				case 'as':
					$("#input").hide();
	                $("#select_teacher").hide();
	                $("#select_as").removeAttr('style');
	                $("#select_as").find('select').change(() => $('form[method = GET]').submit());
					break;
				default:
	                $("#input").hide();
	                $("#select_as").hide();
	                $("#select_teacher").removeAttr('style');
	                $("#select_teacher").find('select').change(() => $('form[method = GET]').submit());
					break;
			}
		});
	})
</script>
@endpush