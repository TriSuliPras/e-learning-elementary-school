@extends("dashboard.management.home")

@push("css")
<style>
    #img-profil {
        width: 250px;
        height: 250px;
        border-radius: 10px;
        border: 1px solid black;
        margin: 0 49px;
    }

    #img-input {
        display: block;
        position: absolute;
        top: 288px;
        left: 51px;
    }
</style>
@endpush

@section("content")
<div class="content-header">
    <div>
        <a href="{{ route('management.student.index') }}"> 
            <div class="fa fa-angle-double-left mr-2"></div>
            Halaman Daftar Siswa
        </a>
    </div>
    <h2 class="title">{{ isset($student) ? $student->nama_siswa : "Tambah Data Siswa" }}</h2>
</div>

<div class="content-body">
    <div class="row">
        <div class="col-md-12">
            @include("forms.student")
        </div>
    </div>
</div>
@endsection

@push("javascript")
<script></script>
@endpush