<div class="mb-3">
    <div class="table-responsive-md">
        <table class="table table-custom">
            <thead>
                <tr>
                    <th rowspan="2" width="1%">No.</th>
                    <th rowspan="2" width="25%">Keterangan</th>
                    <th colspan="3">Pembayaran</th>
                    <th rowspan="2" width="1%">Status</th>
                    <th rowspan="2" width="150px">Nominal</th>
                </tr>
                <tr>
                    <th width="130px">Tanggal</th>
                    <th width="130px">Bulan</th>
                    <th width="130px">Tahun</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($months as $key => $month)
                <tr>
                    <td align="center">{{ $loop->iteration }}</td>
                    @if ($key < count($student->administrations->toArray()))
                        @if ($student->administrations[$key]->bulan_id === $month->id)
                            <td align="center">{{ $month->bulan }}</td>
                            <td align="center">{{ explode(' / ', $student->administrations[$key]->tgl_bayar)[0] }}</td>
                            <td align="center">{{ explode(' / ', $student->administrations[$key]->tgl_bayar)[1] }}</td>
                            <td align="center">{{ explode(' / ', $student->administrations[$key]->tgl_bayar)[2] }}</td>
                            <td align="center">
                                <span class="badge badge-success">{{ $student->administrations[$key]->status }}</span>
                            </td>
                            <td align="center">Rp {{ number_format($student->administrations[$key]->biaya, 2, ',', '.') }}</td>
                        @endif
                    @else
                        <td align="center">{{ $month->bulan }}</td>
                            <td align="center">-</td>
                            <td align="center">-</td>
                            <td align="center">-</td>
                            <td align="center">
                                <span class="badge badge-danger">Belum Lunas</span>
                            </td>
                            <td align="center">Rp -,00</td>
                    @endif
                    {{-- @foreach ($student->administrations as $administration)
                        @break($administration->bulan_id !== $month->id)
                        @if ($administration->bulan_id === $month->id)
                        <td align="center">{{ $month->bulan }}</td>
                        <td align="center">{{ explode(' / ', $administration->tgl_bayar)[0] }}</td>
                        <td align="center">{{ explode(' / ', $administration->tgl_bayar)[1] }}</td>
                        <td align="center">{{ explode(' / ', $administration->tgl_bayar)[2] }}</td>
                        <td align="center">
                            <span class="badge badge-success">{{ $administration->status }}</span>
                        </td>
                        <td align="center">Rp {{ number_format($administration->biaya, 2, ',', '.') }}</td>
                        @else
                            <td align="center">{{ $month->bulan }}</td>
                            <td align="center">-</td>
                            <td align="center">-</td>
                            <td align="center">-</td>
                            <td align="center">
                                <span class="badge badge-success">-</span>
                            </td>
                            <td align="center">Rp -,00</td>
                        @endif
                    @endforeach --}}
                    
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>