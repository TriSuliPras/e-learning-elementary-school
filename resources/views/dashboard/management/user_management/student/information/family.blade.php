<div id="family" class="tab-pane fade show" role="tabpanel" aria-labelledby="family-tab">
<dl class="row">
	<dt class="col-2"><h4>Nama Ayah</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="nama_ayah" type="text" class="form-control">
		@else
			{{ $student->nama_ayah }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Tahun Lahir</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="tahunlahir_ayah" type="text" class="form-control">
		@else
			{{ $student->tahunlahir_ayah }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Pekerjaan Ayah</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="pekerjaan_ayah" type="text" class="form-control">
		@else
			{{ $student->pekerjaan_ayah }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Pendidikan Ayah</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="pendidikan_ayah" type="text" class="form-control">
		@else
			{{ $student->pendidikan_ayah }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Pengahasilan Ayah</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="penghasilan_ayah" type="text" class="form-control">
		@else
			{{ $student->penghasilan_ayah }}
		@endif
	</dd>
</dl>

<hr />

<dl class="row">
	<dt class="col-2"><h4>Nama Ibu</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="nama_ibu" type="text" class="form-control">
		@else
			{{ $student->nama_ibu }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Tahun Lahir</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="tahunlahir_ibu" type="text" class="form-control">
		@else
			{{ $student->tahunlahir_ibu }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Pekerjaan Ibu</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="pekerjaan_ibu" type="text" class="form-control">
		@else
			{{ $student->pekerjaan_ibu }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Pendidikan Ibu</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="pendidikan_ibu" type="text" class="form-control">
		@else
			{{ $student->pendidikan_ibu }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Pengahasilan Ibu</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="penghasilan_ibu" type="text" class="form-control">
		@else
			{{ $student->penghasilan_ibu }}
		@endif
	</dd>
</dl>

<hr />

<dl class="row">
	<dt class="col-2"><h4>Nama Wali</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="nama_wali" type="text" class="form-control">
		@else
			{{ $student->nama_wali }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Tahun Lahir</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="tahunlahir_wali" type="text" class="form-control">
		@else
			{{ $student->tahunlahir_wali }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Pekerjaan Wali</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="pekerjaan_wali" type="text" class="form-control">
		@else
			{{ $student->pekerjaan_wali }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Pendidikan Wali</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="pendidikan_wali" type="text" class="form-control">
		@else
			{{ $student->pendidikan_wali }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Pengahasilan Wali</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="penghasilan_wali" type="text" class="form-control">
		@else
			{{ $student->penghasilan_wali }}
		@endif
	</dd>
</dl>
</div>