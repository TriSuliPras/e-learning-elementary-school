<div id="transportation" class="tab-pane fade show" role="tabpanel" aria-labelledby="transportation-tab">
<dl class="row">
	<dt class="col-2"><h4>Jarak Rumah Ke Sekolah</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="jarak_rumah_kesekolah" type="text" class="form-control">
		@else
			{{ $student->jarak_rumah_kesekolah }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Jarak Rumah Ke Sekolah (Km)</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="jarak_rumah_kesekolah_km" type="text" class="form-control">
		@else
			{{ $student->jarak_rumah_kesekolah_km }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Alat Transportasi</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="alat_transportasi" type="text" class="form-control">
		@else
			{{ $student->alat_transportasi }}
		@endif
	</dd>
</dl>
</div>