<div id="education" class="tab-pane fade show" role="tabpanel" aria-labelledby="education-tab">
<dl class="row">
	<dt class="col-2"><h4>Asal Sekolah</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="ijazahterakhir_asal" type="text" class="form-control">
		@else
			{{ $student->ijazahterakhir_asal }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>No Ijazah</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="ijazahterakhir_no" type="text" class="form-control">
		@else
			{{ $student->ijazahterakhir_no }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Tahun Ijazah</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="ijazahterakhir_tahun" type="text" class="form-control">
		@else
			{{ $student->ijazahterakhir_tahun }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Program Pengajaran</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="program_pengajaran" type="text" class="form-control">
		@else
			{{ $student->program_pengajaran }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>NPSN</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="npsn" type="text" class="form-control">
		@else
			{{ $student->npsn }}
		@endif
	</dd>
</dl>
</div>