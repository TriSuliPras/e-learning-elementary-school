<div id="biodata" class="tab-pane fade show active" role="tabpanel" aria-labelledby="biodata-tab">
<dl class="row">
	<dt class="col-2"><h4>Jenis Kelamin</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
			<select name="jenis_kelamin" class="select-control" title="Pilih Jenis Kelamin">
				<option value="L">Laki-Laki</option>
				<option value="P">Perempuan</option>
			</select>
		@else
			{{ $student->jenis_kelamin === "L" ? "Laki-Laki" : "Perempuan" }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Tempat Lahir</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="tempat_lahir" type="text" class="form-control">
		@else
			{{ $student->tempat_lahir }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Tanggal Lahir</h4></dt>
	<div class="col-6">
		@if (! isset($student))
		<div class="date-control">
			<input name="tanggal_lahir" type="text" class="form-control" value="" data-date-format="yyyy-mm-dd" >
			<label class="fa fa-calendar" for="startDate"></label>
		</div>
		@else
			{{ $student->tanggal_lahir }}
		@endif
	</div>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Agama</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="agama" type="text" class="form-control" value="ISLAM">
		@else
			{{ $student->agama }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Tinggi Badan</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="tinggi_badan" type="text" class="form-control">
		@else
			{{ $student->tinggi_badan }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Berat Badan</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="berat_badan" type="text" class="form-control">
		@else
			{{ $student->berat_badan }}			
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Anak Ke</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="" type="text" class="form-control">
		@else
			{{ "" }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>Berkebutuhan Khusus</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="berkebutuhan_khusus" type="text" class="form-control">
		@else
			{{ $student->berkebutuhan_khusus }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>No.Telpon</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="no_tlp" type="text" class="form-control">
		@else
			{{ $student->no_tlp }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>No.Hp</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="no_hp" type="text" class="form-control">
		@else
			{{ $student->no_hp }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>E-Mail</h4></dt>
	<dd class="col-6">
		@if (! isset($student))
		<input name="email" type="text" class="form-control">
		@else
			{{ $student->email }}
		@endif
	</dd>
</dl>
</div>