@extends("dashboard.management.home")

@push("css")
@endpush

@section("content")
<div class="content-header">
    <div>
        <a href="{{ route('management.student.index') }}"> 
            <div class="fa fa-angle-double-left mr-2"></div>
            Halaman Daftar Siswa
        </a>
    </div>

    <h2 class="title">Data Personal Siswa</h2>
</div>

<div class="content-body">
    <div class="row">
        <div class="col-md-8">
            <dl class="row">
                <dt class="col-4"><h4>NISN</h4></dt>
                <dd class="col-8">{{ $student->nisn }}</dd>

                <dt class="col-4"><h4>NIS</h4></dt>
                <dd class="col-8">{{ $student->nis }}</dd>

                <dt class="col-4"><h4>NIK</h4></dt>
                <dd class="col-8">{{ $student->nik }}</dd>

                <dt class="col-4"><h4>Nama</h4></dt>
                <dd class="col-8">{{ $student->nama_siswa }}</dd>

                <dt class="col-4"><h4>Kelas</h4></dt>
                <dd class="col-8">{{ $student->kelas }}</dd>

                <dt class="col-4"><h4>Status</h4></dt>
                <dd class="col-8">{{ $student->status }}</dd>
            </dl>

            <h3 class="mb-3">
                <div class="accordion">
                    <div class="card">
                        <div id="identity">
                            <button id="collapse1" class="btn mb-3 btn-primary btn-wicon" data-toggle="collapse" data-target="#identity-data" aria-expanded="true" aria-controls="identity">
                                <span class="fa fa-vcard-o"></span>
                                Data Akademik
                            </button>

                            <button id="collapse2" class="btn mb-3 btn-primary btn-wicon" data-toggle="collapse" data-target="#administration-data" aria-expanded="true" aria-controls="identity">
                                <span class="fa fa-money"></span>
                                Data Administrasi {{ date('Y') }}
                            </button>
                        </div>
                    </div>
                </div>
            </h3>
        </div>

        <div class="col-12" id="Identitas">
            <div id="identity-data" class="collapse" aria-labelledby="identity" data-parent="#accordion">
                <div class="card-body">
                    <div class="mb-3">
                        <ul class="nav nav-tabs" id="employee-tab" role="tablist">
                            <li class="nav-item">
                                <a id="biodata-tab" href="#biodata" role="tab" aria-controls="biodata" aria-selected="true" class="nav-link active" data-toggle="tab">Biodata</a>
                            </li>

                            <li class="nav-item">
                                <a id="transportation-tab" href="#transportation" role="tab" aria-controls="transportation" aria-selected="false" class="nav-link" data-toggle="tab">Transportasi</a>
                            </li>

                            <li class="nav-item">
                                <a id="education-tab" href="#education" role="tab" aria-controls="education" aria-selected="false" class="nav-link" data-toggle="tab">Pendidikan</a>
                            </li>

                            <li class="nav-item">
                                <a id="family-tab" href="#family" role="tab" aria-controls="family" aria-selected="false" class="nav-link" data-toggle="tab">Keluarga</a>
                            </li>

                            <li class="nav-item">
                                <a id="address-tab" href="#address" role="tab" aria-controls="address" aria-selected="false" class="nav-link" data-toggle="tab">Akun</a>
                            </li>
                        </ul>
                    </div>

                    <div id="employee-tab-content" class="tab-content">
                        @include("dashboard.management.user_management.student.information.biodata")

                        @include("dashboard.management.user_management.student.information.transportation") 

                        @include("dashboard.management.user_management.student.information.education")

                        @include("dashboard.management.user_management.student.information.family")     

                        @include("dashboard.management.user_management.student.information.address")                
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12" id="Administrasi">
            <div id="administration-data" class="collapse" aria-labelledby="identity" data-parent="#accordion">
                <div class="card-body">
                    @include("dashboard.management.user_management.student.information.administration")
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push("javascript")
<script>
    (function () {
        $("#collapse1").click(function () {
            let adminData = $("#administration-data");

            if (adminData.hasClass("show")) {
                adminData.removeClass("show");
            }
        });

        $("#collapse2").click(function () {
            let identityData = $("#identity-data");

            if (identityData.hasClass("show")) {
                identityData.removeClass("show");
            }
        });
    })();
</script>
@endpush