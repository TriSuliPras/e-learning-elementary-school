@extends("dashboard.management.home")

@push("css")
<link rel="stylesheet" href="{{ asset('web/customs/css/option-hover.css') }}">
@endpush

@section("content")
<div class="content-header">
    <h2 class="title">Daftar Siswa</h2>
    <div class="right-content">
    	<a class="btn btn-primary pull-right btn-add" href="{{ route('management.student.create') }}"> 
    		<span>Tambah Data Siswa</span>
    	</a>
    </div>
</div>
@if (session("saved") || session("delete"))
<div id="session" class="alert alert-success alert-dismissible fade show" role="alert">
    <span class="lead">
        Success!: {{ session("saved") ? session("saved") : (session("delete") ? session("delete") : '') }}
    </span>
    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>  
@endif
<div class="content-body">
	<div class="row justify-content-between mb-4">
		<div class="col-md-2 col-sm-4">
            <div class="filter">
                <select class="select-control" name="search" title="Filter">
                    <option value="class">Kelas</option>
                    <option value="all">Semua</option>
                </select>
            </div>     
        </div>
          	<div class="col-md-4 col-sm-5">
            <div class="search">
              	<form action="{{ route('management.student.index') }}" method="GET">
                    {{ csrf_field() }}
                    <input name="option" type="text" id="option" hidden value="nisn">
	                <input id="input" name="search_nisn" class="form-control" type="text" placeholder="Cari..," title="Berdasarkan NISN / Kelas" data-toggle="tooltip" />
                    <div id="select_class" style="display: none;">
                        <select name="search_class" class="select-control" title="Pilih Kelas" data-live-search="true">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                        </select>
                    </div>
	                <button class="btn btn-link fa fa-search" type="submit"></button>
            	</form>
            </div>
        </div>
	</div>

	<div class="table-responsive-md">
		<table class="table table-custom">
			<thead>
				<th width="1%">#</th>
                <th width="25%">Nama</th>
                <th width="15%">N I S N</th>
                <th>Kelas</th>
                <th>No Telpon</th>
                <th width="1%"> </th>
			</thead>

			<tbody>
                @if (is_null($students))
                <tr>
                  <td align="center" colspan="6"><h4>Data Tidak Ditemukan!</h4></td>
                </tr>
                  @elseif (array_key_exists('data', $students->toArray()) && empty($students->toArray()['data']))
                    <tr>
                      <td align="center" colspan="6"><h4>Data Tidak Ditemukan!</h4></td>
                    </tr>
                  @elseif (empty($students->toArray()))
                    <tr>
                      <td align="center" colspan="6"><h4>Data Tidak Ditemukan!</h4></td>
                    </tr>
                  @else
    				@foreach ($students as $student)
    				<tr>
                        <td align="center">{{ $loop->iteration }}</td>
                        <td>{{ $student->nama_siswa }}</td>
                        <td align="center">{{ $student->nisn }}</td>
                        <td align="right">{{ "$student->kelas" }}</td>
                        <td align="center">{{ $student->no_tlp }}</td>
                        <td>
                        	<div class="dropdown">
    	                        <button class="btn btn-outline-secondary btn-sm" type="button" data-toggle="dropdown">Action</button>
    	                        <div class="dropdown-menu dropdown-menu-right">
    	                        	<a class="dropdown-item dtl" href="{{ route('management.student.show', $student->id) }}">Detail</a>
    	                        	<a class="dropdown-item del" href="#modalDelete" data-toggle="modal">Hapus</a></div>
                        	</div>
                        </td>
                    </tr>
    				@endforeach
                @endif
			</tbody>
		</table>
	</div>
	
	<div class="row justify-content-between mb-4">
      	<div class="col-md-12 col-sm-6 text-right">
            {{ $students->links('pagination.paginate') }}
      	</div>
    </div>

</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/SessionMessages.js') }}"></script>
<script>
    $(document).ready(function () {
        $("select[name=search]").change(function () {
            
            let option = $(this).val();

            $("#option").attr("value", option);
            
            if (option == "class") {
                $("#input").hide();
                $("#select_class").removeAttr('style');
                $("#select_class").find('select').change(() => $('form[method = GET]').submit());
            }
            else {
                $("#select_class").hide();
                $("#input").removeAttr('style');
            }
        });
    })
</script>
@endpush