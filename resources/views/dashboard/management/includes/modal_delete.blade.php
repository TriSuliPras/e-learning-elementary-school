@if (Route::is("material"))
    @foreach ($datas as $material)
        <div class="modal fade" id="{{ 'delete'.$material->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ trans('label.act.delete') }}</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close" title="Batal" data-toggle="tooltip">
                            <img src="{{ asset('web/images/icon-close.png') }}"/>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{ trans('confirmation.sure_to_delete', ['this' => "Materi"]) }} <br> <b>{{ trans('label.for.-T').": {$material->title}" }}</b></p>
                    </div>
                    <div class="modal-footer">
                        <form action="{{ route('material.delete', $material->id) }}" method="POST">
                            {{ csrf_field() }} {{ method_field("DELETE") }}
                            <button class="btn btn-danger btn-round" type="submit">{{ trans('label.btn.rm') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@elseif (Route::is("task"))
    @foreach ($datas as $task)
        <div class="modal fade" id="{{ 'delete'.$task->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ trans('label.act.delete') }}</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close" title="Batal" data-toggle="tooltip">
                            <img src="{{ asset('web/images/icon-close.png') }}"/>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{ trans('confirmation.sure_to_delete', ['this' => "Tugas"]) }} <br> <b>{{ trans('label.for.-T').": {$task->title}" }}</b></p>
                    </div>
                    <div class="modal-footer">
                        <form action="{{ route('task.delete', $task->id) }}" method="POST">
                            {{ csrf_field() }} {{ method_field("DELETE") }}
                            <button class="btn btn-danger btn-round" type="submit">{{ trans('label.btn.rm') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@elseif(Route::is("quiz"))
    @foreach ($datas as $quiz)
        <div class="modal fade" id="{{ 'delete'.$quiz->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            {{ trans('label.act.delete') }}
                        </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close" title="Batal" data-toggle="tooltip">
                            <img src="{{ asset('web/images/icon-close.png') }}"/>
                        </button>
                    </div>
                    <form action="{{ route('quiz.delete', $quiz->id) }}" method="POST">
                        {{ csrf_field() }} {{ method_field("DELETE") }}
                        <div class="modal-body">
                            <p>{{ trans('confirmation.sure_to_delete', ['this' => "Kuis"]) }} <br> <b>{{ trans('label.for.-T').": {$quiz->title}" }}</b></p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-danger btn-round" type="submit">{{ trans('label.btn.rm') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@elseif (Route::is("news"))
    @foreach ($datas as $news)
        <div class="modal fade" id="{{ 'delete'.$news->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            {{ trans('label.act.delete') }}
                        </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close" title="Batal" data-toggle="tooltip">
                            <img src="{{ asset('web/images/icon-close.png') }}"/>
                        </button>
                    </div>
                    <form action="{{ route('news.delete', $news->id) }}" method="POST">
                        {{ csrf_field() }} {{ method_field("DELETE") }}
                        <div class="modal-body">
                            <p>{{ trans('confirmation.sure_to_delete', ['this' => "Kuis"]) }} <br> <b>{{ trans('label.for.-T').": {$news->title}" }}</b></p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-danger btn-round" type="submit">{{ trans('label.btn.rm') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endif