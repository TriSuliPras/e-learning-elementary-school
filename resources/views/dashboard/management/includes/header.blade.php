<h1 class="logo" style="margin-left: 16px;">
	<a href="{{ route('management.home') }}">
		MI Baiturrahman 
	</a>
</h1>
<div class="dropdown">
	<div class="account" data-toggle="dropdown">
		<div class="image"><img src="" alt="image" /></div>
		<div class="content">
			<span class="name">
				{{ auth()->user()->name }}
			</span>
			<span class="role">
				{{ auth()->user()->hasRole('admin') ? "Admin" : "Guru" }}
			</span>
		</div>
	</div>

	<div class="dropdown-menu dropdown-menu-right">
		<a class="dropdown-item" href="{{ route('management.employee.show', auth()->user()->pegawai_id) }}">Profil</a>
		<div class="dropdown-divider"></div>
		<a class="dropdown-item" href="#" target="_blank">Sim Data</a>
		<a class="dropdown-item" href="{{ route('management.logout') }}">Logout</a>
	</div>
</div>