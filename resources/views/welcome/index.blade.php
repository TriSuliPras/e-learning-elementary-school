<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
       {{--  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/> --}}
        <link rel="icon" type="image/png" href="{{ asset('web/images/logo.png') }}">
        <title>{{ trans('label.app_title') }}</title>
        <meta name="description" content=""/>
        <link rel="stylesheet" href="{{ asset('web/styles/plugins.css') }}"/>
        <link rel="stylesheet" href="{{ asset('web/styles/main.css') }}"/>
    </head>
    <body class="login">
        <div id="wrap">
            <main>
                <div class="container-fluid auth-page">
                    <div class="row">
                        <div class="col-md-10 left-wrap">
                            <div class="carousel slide login-slide" id="loginSlider" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li class="active" data-target="#loginSlider" data-slide-to="0"></li>
                                    <li data-target="#loginSlider" data-slide-to="1"></li>
                                    <li data-target="#loginSlider" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="image" style="background-image: url({{ asset('web/images/carousel3.jpeg') }});"></div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="image" style="background-image: url({{ asset('web/images/carousel1.JPG') }});"></div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="image" style="background-image: url({{ asset('web/images/carousel2.JPG') }});">         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 right-wrap">
                            <div class="form-wrap">
                                <div class="box">
                                    <h1 class="logo"><img src="{{ asset('web/images/logo.png') }}"/>
                                        {{ trans('label.origin') }}
                                    </h1>
                                    <p>{{ trans('label.login') }}</p>
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a id="std" class="nav-link active" data-toggle="tab" href="#tab1">
                                                {{ trans('label.for.-S') }}
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a id="mng" class="nav-link" data-toggle="tab" href="#tab2">{{ trans('label.for.-M') }}</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active pt-2" id="tab1" role="tabpanel">
                                            @include("forms.login_student")
                                        </div>
                                        <div class="tab-pane pt-3" id="tab2" role="tabpanel">
                                            @include("forms.login_management")
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </body>
    <script src="{{ asset('web/plugins/jquery-3.3.1/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('web/plugins/bootstrap-4/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('web/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(function () {
            var loginBtn = $("button[type=submit]");

            loginBtn.each(function () {
                var btn = $(this);

                btn.click(function ( event ) {
                    event.preventDefault();

                    var form = $(this).parent().parent(),
                        name = form.find('input[type=text]'),
                        pwd =  form.find('input[type=password]'),
                        _csrf = form.find('input[name=_token]');
                    
                    if ( name.val() == "" && pwd.val() == "") {
                        name.attr('title', "{{ trans('validation.custom.nisn.required') }}");
                        pwd.attr('title', "{{ trans('validation.custom.password.required') }}");
                    }
                    else if ( pwd.val() == "" ) {
                        pwd.attr('title', "{{ trans('validation.custom.password.required') }}"); 
                    }
                    else if ( name.val() == "" ) {
                        name.attr('title', "{{ trans('validation.custom.nisn.required') }}");
                    }
                    else {
                        
                        $.ajax({
                            url: form.attr('action'),
                            type: "{{ trans('label.method.PS') }}",
                            data: {
                                _token: _csrf.val(),
                                name: name.val(),
                                nisn: name.val(),
                                password: pwd.val()
                            },
                            success: function ( res ) {
                                window.location.reload();
                            },
                            error: function ( res ) {
                                var msg = res.responseJSON.msg;

                                name.addClass('is-invalid');
                                pwd.addClass('is-invalid');
                                form.find('.invalid-feedback').html(msg);
                            }
                        })
                    }
                })
            })
        });
    </script>
</html>