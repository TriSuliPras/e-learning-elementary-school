@if (!Route::is("news"))
<div id="select_lesson" style="display: none;">
    <select name="search_lesson" class="select-control" title="Pilih Mata Pelajaran" data-live-search="true">
        @foreach ($lessons as $lesson)
           <option value="{{ $lesson->id }}">{{ $lesson->nama_matapelajaran }}</option>
        @endforeach
    </select>
</div>

<div id="select_class" style="display: none;">
    <select name="search_class" class="select-control" title="Pilih Kelas" data-live-search="true">
        @foreach ($classes as $classroom)
            <option value="{{ "$classroom->nama_kelas $classroom->palarel" }}">{{ $classroom->nama_kelas }}</option>
        @endforeach
    </select>
</div>
@endif

<div id="select_teacher" style="display: none;">
    <select name="search_teacher" class="select-control" title="Pilih Pengajar" data-live-search="true">
        @foreach ($datas as $teacher)
            <option value="{{ $teacher->teachers->id }}">{{ $teacher->teachers->employees['nama_pegawai'] }}</option>
        @endforeach
    </select>
</div>