<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>MI-Baiturrahman</title>
    <meta name="description" content=""/>
    <link rel="stylesheet" href="{{ asset('web/styles/plugins.css') }}"/>
    <link rel="stylesheet" href="{{ asset('web/styles/main.css') }}"/>
  </head>
  <body class="login">
    <div id="wrap">
      <main>
        <div class="page-not-found">
          <div class="content"><img class="img" src="{{ asset('web/images/404.jpg') }}"/>
            <div class="text">Oops! Maaf, kami tidak bisa memnemukan halaman yang diminta</div>
          </div>
        </div>
      </main>
    </div>
  </body>
</html>