<?php 

return [
	'sure_to_lock' => "Apa anda yakin akan mengunci nilai :score siswa ini ?",
	'sure_to_delete' => "Apakah anda yakin akan menghapus data :this ini ?",
	'unauthenticate' => "NamaPengguna atau Password tidak terdaftar!",
	'store' => ":data Baru berhasil disimpan",
	'store_student_quiz' => "Selamat, anda baru saja menyelesaikan kuis",
	'update' => ":data berhasil diperbarui",
	'delete' => ":data berhasil dihapus",
	'update_score' => "Nilai :data siswa berhasil diperbarui",
	'update_lock' => "Nilai :data siswa berhasil dikunci",
	'success' => "Success! ",
	'error' => "Oops! ",
	'failure' => "Kesalahan terjadi, Periksa kembali form anda."
];