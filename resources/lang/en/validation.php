<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field must have a value.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'greater_than_one_hundred' => "Pastikan field Bobot Nilai mencapai / tidak lebih dari 100 %",
    'score_more_than_one_hundred' => "Pastikan Nilai tidak melebihi max [100]",
    'invalid_date' => "Pastikan memasukan format :what yang benar",
    'must_a_number' => "Pastikan field Diisi Dengan Angka",
    'phone_number' => "Nomor Tidak Valid",
    'custom' => [
        'weight_multiple' => [
            'required_without' => "Pastikan field Bobot Nilai salah satu sudah terisi",
            'required_unless' => "Pastikan field Bobot Nilai mencapai / tidak lebih dari 100 %"
        ],
        'weight_essay' => [
            'required_without' => "Pastikan field Bobot Nilai salah satu sudah terisi",
            'required_unless' => "Pastikan field Bobot Nilai mencapai / tidak lebih dari 100 %"
        ],
        'foto_pegawai' => [
            'mimes' => 'Pastikan format file adalah [:values]'
        ],
        'foto_siswa' => [
            'mimes' => 'Pastikan format file adalah [:values]'
        ],
        'alamat' => [
            'required' => "Pastikan field Alamat sudah terisi"
        ],
        'nip' => [
            'required' => "Pastikan field NIP sudah terisi",
            'numeric' => "Pastikan field NIP terisi dengan angka",
        ],
        'no_telp' => [
            'required' => "Pastikan field Telepon sudah terisi",
            'numeric' => "Pastikan field Telepon terisi dengan angka",
        ],
        'nama_pegawai' => [
            'required' => "Pastikan Nama Pegawai sudah terisi"
        ],
        'description' => [
            'required' => 'Pastikan field deskripsi sudah terisi',
        ],
        'file_path' => [
            'required' => 'Pastikan anda sudah memilih File',
            'mimes'    => 'Pastikan format file adalah [:values]',
            'required_without' => 'File boleh kosong jika deskripsi telah diisi'
        ],
        'title' => [
            'required' => 'Pastikan field Judul sudah terisi',
            'max'      => 'Pastikan jumlah karakter field Judul tidak lebih dari 75'
        ],
        'datetime' => [
            'required' => 'Pastikan field Tanggal dan Waktu sudah terisi',
            'unique' => 'Tanggal dan Jam Sudah Terpakai'
        ],
        'lesson_id' => [
            'required' => 'Pastikan field Mata Pelajaran sudah terisi'
        ],
        'class_room' => [
            'required' => 'Pastikan field Kelas sudah terisi'
        ],
        'nisn' => [
            'required' => 'Pastikan field NISN sudah terisi',
            'unique'   => 'NISN sudah terdaftar',
            'numeric'  => 'Pastikan NISN terisi dengan angka'
        ],
        'nis' => [
            'required' => 'Pastikan field NIS sudah terisi',
            'unique'   => 'NIS sudah terdaftar',
            'numeric'  => 'Pastikan NIS terisi dengan angka'
        ],
        'nik' => [
            'required' => 'Pastikan field NIK sudah terisi',
            'unique'   => 'NIK sudah terdaftar',
            'numeric'  => 'Pastikan NIK terisi dengan angka'
        ],
        'CPNS_TMT' => [
            'min' => "Pastikan memasukan format tahun yang benar",
            'max' => "Pastikan memasukan format tahun yang benar",
        ],
        'PNS_TMT' => [
            'min' => "Pastikan memasukan format tahun yang benar",
            'max' => "Pastikan memasukan format tahun yang benar",
        ],
        'tmt_guru_tahun' => [
            'min' => "Pastikan memasukan format tahun yang benar",
            'max' => "Pastikan memasukan format tahun yang benar",
        ],
        'tmt_guru_bulan' => [
            'max' => "Pastikan memasukan format tahun yang benar",
        ],
        'tmt_guru_tanggal' => [
            'max' => "Pastikan memasukan format tahun yang benar",
        ],
        'count_down' => [
            'digits_between' => "Pastikan Waktu Soal tidak lebih dari 3 digit"
        ],
        'essay_scores.*' => [
            'digits_between' => "Pastikan Nilai tidak lebih dari 3 digit" 
        ],
        'password' => [
            'required' => "Pastikan field Password sudah terisi"
        ],
        'nama_ayah' => [
            'required' => "Pastikan field Nama Ayah sudah terisi"
        ],
        'tahunlahir_ayah' => [
            'required' => "Pastikan field Tahun Lahir Ayah sudah terisi",
            'min' => "Pastikan memasukan format tahun yang benar",
            'max' => "Pastikan memasukan format tahun yang benar"
        ],
        'pekerjaan_ayah' => [
            'required' => "Pastikan field Pekerjaan Ayah sudah terisi"
        ],
        'pendidikan_ayah' => [
            'required' => "Pastikan field Pendidikan Ayah sudah terisi"
        ],
        'penghasilan_ayah' => [
            'required' => "Pastikan field Penghasilan Ayah sudah terisi"
        ],
        'nama_ibu' => [
            'required' => "Pastikan field Nama Ibu sudah terisi"
        ],
        'tahunlahir_ibu' => [
            'required' => "Pastikan field Tahun Lahir Ibu sudah terisi",
            'min' => "Pastikan memasukan format tahun yang benar",
            'max' => "Pastikan memasukan format tahun yang benar"
        ],
        'pekerjaan_ibu' => [
            'required' => "Pastikan field Pekerjaan Ibu sudah terisi"
        ],
        'pendidikan_ibu' => [
            'required' => "Pastikan field Pendidikan Ibu sudah terisi"
        ],
        'penghasilan_ibu' => [
            'required' => "Pastikan field Penghasilan Ibu sudah terisi"
        ],
        'nama_wali' => [
            'required' => "Pastikan field Nama Wali sudah terisi"
        ],
        'tahunlahir_wali' => [
            'required' => "Pastikan field Tahun Lahir Wali sudah terisi",
            'min' => "Pastikan memasukan format tahun yang benar",
            'max' => "Pastikan memasukan format tahun yang benar"
        ],
        'pekerjaan_wali' => [
            'required' => "Pastikan field Pekerjaan Wali sudah terisi"
        ],
        'pendidikan_wali' => [
            'required' => "Pastikan field Pendidikan Wali sudah terisi"
        ],
        'penghasilan_wali' => [
            'required' => "Pastikan field Penghasilan Wali sudah terisi"
        ],
        'email' => "Penulisan Email Tidak Valid. ( ex: email@email.com )"
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'file_path',
        'title',
        'lesson_id',
        'class_room',
        'nisn',
        'nis',
        'nik',
        'description',
        'datetime',
        'count_down'
    ],

];
